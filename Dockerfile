FROM openjdk:8

RUN mkdir /home/app

WORKDIR /home/app

ADD . ./

RUN wget -q https://services.gradle.org/distributions/gradle-3.3-bin.zip \
    && unzip gradle-3.3-bin.zip -d /opt \
    && rm gradle-3.3-bin.zip

ENV GRADLE_HOME /opt/gradle-3.3
ENV PATH $PATH:/opt/gradle-3.3/bin

EXPOSE 3030
ENV PORT 3030

CMD ["gradle", "runApplication"]