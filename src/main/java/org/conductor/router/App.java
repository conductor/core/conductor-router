package org.conductor.router;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.awt.*;
import java.net.*;
import java.io.IOException;

public class App {

  private static Logger log = LogManager.getLogger(App.class.getName());

    public static void main(String[] args) {
      log.info("Starting application.");

      io.vertx.core.Starter.main(new String[0]);

      try {
        ApplicationContext context = new ClassPathXmlApplicationContext(new String[] { "Spring-AutoScan.xml" });

        Bootstrap bootstrap = (Bootstrap) context.getBean("bootstrap2");
        bootstrap.start();
      } catch (Exception e) {
        log.error("Failed to start application.", e);
      }
    }
}