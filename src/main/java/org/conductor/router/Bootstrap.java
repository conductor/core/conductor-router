package org.conductor.router;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.config.Config;
import org.conductor.router.core.mockups.ApplicationMockup;
import org.conductor.router.core.mockups.ControlUnitMockup;
import org.conductor.router.core.mockups.UserMockup;
import org.conductor.router.service.websocket.server.DDPWebSocket;
import org.conductor.router.service.websocket.server.WordgameClientEndpoint;
import org.glassfish.tyrus.client.ClientManager;
import org.glassfish.tyrus.server.Server;

import javax.inject.Inject;
import javax.inject.Named;
import javax.websocket.DeploymentException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.concurrent.CountDownLatch;

@Named(value = "bootstrap2")
public class Bootstrap {

    private Logger log = LogManager.getLogger(this.getClass().getName());

    @Inject
    private UserMockup userMockup;

    @Inject
    private ControlUnitMockup controlUnitMockup;

    @Inject
    private ApplicationMockup applicationMockup;

    @Inject
    private WordgameClientEndpoint gameClient;

    private CountDownLatch latch;

    public void start() throws IOException {
        try {
            userMockup.addUsers();
            controlUnitMockup.addControlUnits();
            applicationMockup.addApplications();
        } catch (IOException e) {
            log.error("Mockups failed.", e);
        }

        runSocketServer();
        runSocketClient();
    }

    public void runSocketServer() {
        Server server = new Server("localhost", 3030, "/", DDPWebSocket.class);
        try {
            server.start();
        } catch (Exception e) {
            throw new RuntimeException(e);
        } finally {
            //server.stop();
        }
    }

    public void runSocketClient() {
        latch = new CountDownLatch(1);

        ClientManager client = ClientManager.createClient();
        try {
            client.connectToServer(gameClient, new URI("ws://localhost:3030/websocket"));
            latch.await();

        } catch (DeploymentException | URISyntaxException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

}
