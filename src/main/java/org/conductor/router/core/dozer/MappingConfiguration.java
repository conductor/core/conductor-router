package org.conductor.router.core.dozer;

import org.conductor.router.core.dozer.converters.ComponentGroupToComponentList;
import org.conductor.router.core.dozer.converters.DeviceAuthorizationsToStringList;
import org.conductor.router.core.dozer.converters.ObjectConverter;
import org.conductor.router.core.dozer.converters.PublicationNameConverter;
import org.dozer.CustomConverter;
import org.dozer.spring.DozerBeanMapperFactoryBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henrik on 15/03/2016.
 */
@Configuration
public class MappingConfiguration {

    private static final List<CustomConverter> converters = new ArrayList<>();

    static {
        converters.add(new PublicationNameConverter());
        converters.add(new DeviceAuthorizationsToStringList());
        converters.add(new ComponentGroupToComponentList());
        converters.add(new ObjectConverter());
    }

    @Bean
    // REMEMBER THAT ALL XML MAPPING FILES MUST END WITH "Mapping.xml"!
    public DozerBeanMapperFactoryBean dozerBeanMapperFactoryBean(@Value("classpath*:mappings/*Mapping.xml") Resource[] resources) throws Exception {
        final DozerBeanMapperFactoryBean dozerBeanMapperFactoryBean = new DozerBeanMapperFactoryBean();
        // Other configurations
        dozerBeanMapperFactoryBean.setMappingFiles(resources);
        dozerBeanMapperFactoryBean.setCustomConverters(converters);
        return dozerBeanMapperFactoryBean;
    }
}