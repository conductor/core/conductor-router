package org.conductor.router.core.dozer.converters;

import org.conductor.router.core.entities.DeviceAuthorization;
import org.conductor.router.core.publisher.publications.PublicationName;
import org.dozer.CustomConverter;
import org.dozer.DozerConverter;
import org.dozer.MappingException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Henrik on 07/09/2016.
 */
public class DeviceAuthorizationsToStringList implements CustomConverter {

    @Override
    public Object convert(Object destination, Object source, Class destClass, Class sourceClass) {
        if (source == null) {
            return null;
        }

        List<String> result = new ArrayList();

        if (source instanceof Collection) {
            Collection<DeviceAuthorization> deviceAuthorizations = (Collection<DeviceAuthorization>) source;

            deviceAuthorizations.forEach(authorization -> {
                if (authorization != null && authorization.getAccessGivenTo() != null) {
                    result.add(authorization.getAccessGivenTo().getPublicKey());
                }
            });

            return result;
        } else {
            throw new MappingException("Converter " + this.getClass().getSimpleName()
                    + "used incorrectly. Arguments passed in were:"
                    + destination + " and " + source);
        }
    }

}
