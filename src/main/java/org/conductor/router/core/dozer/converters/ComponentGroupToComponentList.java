package org.conductor.router.core.dozer.converters;

import org.conductor.router.core.entities.Component;
import org.conductor.router.core.entities.ComponentGroup;
import org.conductor.router.core.entities.Property;
import org.conductor.router.core.entities.enums.DataType;
import org.dozer.CustomConverter;
import org.dozer.MappingException;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Henrik on 07/09/2016.
 */
public class ComponentGroupToComponentList implements CustomConverter {

    @Override
    public Object convert(Object destination, Object source, Class destClass, Class sourceClass) {
        if (source == null) {
            return null;
        }

        List<org.conductor.router.core.types.publications.Component> result = new ArrayList();

        if (source instanceof Collection) {
            Collection<ComponentGroup> componentGroups = (Collection<ComponentGroup>) source;

            componentGroups.forEach(componentGroup -> {
                if (componentGroup != null && componentGroup.getComponents() != null) {
                    result.addAll(convertComponents(componentGroup.getComponents()));
                }
            });

            return result;
        } else {
            throw new MappingException("Converter " + this.getClass().getSimpleName()
                    + "used incorrectly. Arguments passed in were:"
                    + destination + " and " + source);
        }
    }

    public List<org.conductor.router.core.types.publications.Component> convertComponents(Collection<Component> components) {
        List<org.conductor.router.core.types.publications.Component> result = new ArrayList();

        for (Component component : components) {
            result.add(convertComponent(component));
        }

        return result;
    }

    public org.conductor.router.core.types.publications.Component convertComponent(Component component) {
        org.conductor.router.core.types.publications.Component result = new org.conductor.router.core.types.publications.Component();
        result.setId(component.getId());
        result.setName(component.getName());
        result.setType(component.getType());
        result.setProperties(convertProperties(component.getProperties()));
        return result;
    }

    public List<org.conductor.router.core.types.publications.Property> convertProperties(Collection<Property> properties) {
        List<org.conductor.router.core.types.publications.Property> result = new ArrayList();

        for (Property property : properties) {
            result.add(convertProperty(property));
        }

        return result;
    }

    public org.conductor.router.core.types.publications.Property convertProperty(Property property) {
        org.conductor.router.core.types.publications.Property result = new org.conductor.router.core.types.publications.Property();
        result.setId(property.getId());
        result.setName(property.getName());
        result.setDataType(property.getDataType());
        result.setValue(convertToDataType(property.getValue(), property.getDataType()));
        result.setReadOnly(property.isReadOnly() == null || !property.isReadOnly() ? false : true);
        return result;
    }

    public Object convertToDataType(String value, DataType dataType) {
        switch(dataType) {
            case STRING:
                return value;
            case DOUBLE:
                return Double.parseDouble(value);
            case INTEGER:
                return Integer.parseInt(value);
            case BOOLEAN:
                return Boolean.parseBoolean(value);
            default:
                throw new RuntimeException("Unsupported data type: " + dataType);
        }

    }

}
