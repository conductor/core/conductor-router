package org.conductor.router.core.dozer.converters;

import org.dozer.CustomConverter;
import org.dozer.MappingException;

/**
 * Created by Henrik on 10/09/2016.
 */
public class ObjectConverter implements CustomConverter {

    @Override
    public Object convert(Object destination, Object source, Class destClass, Class sourceClass) {
        if (source == null) {
            return null;
        }

        if (source instanceof Object) {
            return source;
        } else {
            throw new MappingException("Converter " + this.getClass().getSimpleName()
                    + "used incorrectly. Arguments passed in were:"
                    + destination + " and " + source);
        }
    }

}
