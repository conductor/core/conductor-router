package org.conductor.router.core.dozer.converters;

import org.conductor.router.core.publisher.publications.PublicationName;
import org.dozer.CustomConverter;
import org.dozer.MappingException;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Henrik on 25/03/2016.
 */
public class PublicationNameConverter implements CustomConverter {

    private static final Map<PublicationName, String> publicationNameMap = new HashMap<>();

    static {
        /*
        publicationNameMap.put(PublicationName.COMMAND_REQUEST, "requests");
        publicationNameMap.put(PublicationName.COMMAND_RESPONSE, "responses");
        */
    }

    public Object convert(Object destination, Object source,
                          Class destClass, Class sourceClass) {
        if (source == null) {
            return null;
        }

        if (source instanceof PublicationName) {
            if (!publicationNameMap.containsKey(source)) {
                throw new MappingException("Converter " + this.getClass().getSimpleName() + " failed to convert source " + source + ", no mapping exist for that source.");
            }

            return publicationNameMap.get(source);
        } else {
            throw new MappingException("Converter " + this.getClass().getSimpleName()
                    + "used incorrectly. Arguments passed in were:"
                    + destination + " and " + source);
        }
    }
}