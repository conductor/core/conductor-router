package org.conductor.router.core.exceptions;

/**
 * Created by Henrik on 24/07/2016.
 */
public class InvalidRequestException extends Exception {

    public InvalidRequestException(String message) {
        this(message, null);
    }

    public InvalidRequestException(String message, Throwable e) {
        super(message, e);
    }

}
