package org.conductor.router.core.repositories;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;
import com.j256.ormlite.support.DatabaseConnection;
import com.j256.ormlite.table.TableUtils;
import org.conductor.router.core.config.Config;
import org.conductor.router.core.entities.*;
import org.conductor.router.core.entities.ApplicationInvite;
import org.conductor.router.core.entities.SetPropertyValueRequest;
import org.conductor.router.core.entities.InstallComponentGroupResponse;
import org.conductor.router.core.entities.InstallDeviceResponse;
import org.conductor.router.core.entities.SetPropertyValueResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.inject.Inject;
import java.sql.SQLException;
import java.util.Collection;
import java.util.List;

/**
 * Created by Henrik on 26/07/2016.
 */
@Repository
public class DBRepository {
    private ConnectionSource connectionSource;
    private DatabaseConnection databaseConnection;

    private Dao<ControlUnit, String> controlUnitDAO;
    private Dao<Device, String> deviceDAO;
    private Dao<ComponentGroup, String> componentGroupDAO;
    private Dao<Component, String> componentDAO;
    private Dao<Property, String> propertyDAO;
    private Dao<Method, String> methodDAO;
    private Dao<MethodParameterDefinition, String> methodParameterDefinitionDAO;
    private Dao<SetPropertyValueResponse, String> setPropertyValueResponseDAO;
    private Dao<SetPropertyValueRequest, String> setPropertyValueRequestDAO;
    private Dao<InstallComponentGroupRequest, String> installComponentGroupRequestDAO;
    private Dao<InstallComponentGroupResponse, String> installComponentGroupResponseDAO;
    private Dao<User, String> userDAO;
    private Dao<Application, String> applicationDAO;
    private Dao<AuthenticationKey, String> authenticationKeyDAO;
    private Dao<InstallDeviceRequest, String> installDeviceRequestDAO;
    private Dao<InstallDeviceResponse, String> installDeviceResponseDAO;
    private Dao<Option, String> optionDAO;
    private Dao<DeviceAuthorization, String> deviceAuthorizationDAO;
    private Dao<ApplicationInvite, String> applicationInviteDAO;
    private Dao<SetPropertyValueRequestSummary, String> setPropertyValueRequestSummaryDAO;

    @Inject
    public DBRepository(Config config) throws SQLException {
        String databasePath = config.getProperty(Config.Properties.DATABASE_PATH);

        if (databasePath == null) {
            databasePath = "conductor.db";
        }

        this.connectionSource = new JdbcConnectionSource("jdbc:sqlite:" + databasePath);
        this.databaseConnection = this.connectionSource.getReadWriteConnection();
        this.databaseConnection.setAutoCommit(false);

        controlUnitDAO = DaoManager.createDao(this.connectionSource, ControlUnit.class);
        deviceDAO = DaoManager.createDao(this.connectionSource, Device.class);
        componentGroupDAO = DaoManager.createDao(this.connectionSource, ComponentGroup.class);
        componentDAO = DaoManager.createDao(this.connectionSource, Component.class);
        propertyDAO = DaoManager.createDao(this.connectionSource, Property.class);
        methodDAO = DaoManager.createDao(this.connectionSource, Method.class);
        methodParameterDefinitionDAO = DaoManager.createDao(this.connectionSource, MethodParameterDefinition.class);
        setPropertyValueResponseDAO = DaoManager.createDao(this.connectionSource, SetPropertyValueResponse.class);
        setPropertyValueRequestDAO = DaoManager.createDao(this.connectionSource, SetPropertyValueRequest.class);
        installComponentGroupRequestDAO = DaoManager.createDao(this.connectionSource, InstallComponentGroupRequest.class);
        installComponentGroupResponseDAO = DaoManager.createDao(this.connectionSource, InstallComponentGroupResponse.class);
        userDAO = DaoManager.createDao(this.connectionSource, User.class);
        applicationDAO = DaoManager.createDao(this.connectionSource, Application.class);
        authenticationKeyDAO = DaoManager.createDao(this.connectionSource, AuthenticationKey.class);
        installDeviceRequestDAO = DaoManager.createDao(this.connectionSource, InstallDeviceRequest.class);
        installDeviceResponseDAO = DaoManager.createDao(this.connectionSource, InstallDeviceResponse.class);
        optionDAO = DaoManager.createDao(this.connectionSource, Option.class);
        deviceAuthorizationDAO = DaoManager.createDao(this.connectionSource, DeviceAuthorization.class);
        applicationInviteDAO = DaoManager.createDao(this.connectionSource, ApplicationInvite.class);
        setPropertyValueRequestSummaryDAO = DaoManager.createDao(this.connectionSource, SetPropertyValueRequestSummary.class);

        TableUtils.createTableIfNotExists(this.connectionSource, ControlUnit.class);
        TableUtils.createTableIfNotExists(this.connectionSource, Device.class);
        TableUtils.createTableIfNotExists(this.connectionSource, ComponentGroup.class);
        TableUtils.createTableIfNotExists(this.connectionSource, Component.class);
        TableUtils.createTableIfNotExists(this.connectionSource, Property.class);
        TableUtils.createTableIfNotExists(this.connectionSource, Method.class);
        TableUtils.createTableIfNotExists(this.connectionSource, MethodParameterDefinition.class);
        TableUtils.createTableIfNotExists(this.connectionSource, SetPropertyValueResponse.class);
        TableUtils.createTableIfNotExists(this.connectionSource, SetPropertyValueRequest.class);
        TableUtils.createTableIfNotExists(this.connectionSource, InstallComponentGroupRequest.class);
        TableUtils.createTableIfNotExists(this.connectionSource, InstallComponentGroupResponse.class);
        TableUtils.createTableIfNotExists(this.connectionSource, User.class);
        TableUtils.createTableIfNotExists(this.connectionSource, Application.class);
        TableUtils.createTableIfNotExists(this.connectionSource, AuthenticationKey.class);
        TableUtils.createTableIfNotExists(this.connectionSource, InstallDeviceRequest.class);
        TableUtils.createTableIfNotExists(this.connectionSource, InstallDeviceResponse.class);
        TableUtils.createTableIfNotExists(this.connectionSource, Option.class);
        TableUtils.createTableIfNotExists(this.connectionSource, DeviceAuthorization.class);
        TableUtils.createTableIfNotExists(this.connectionSource, ApplicationInvite.class);
        TableUtils.createTableIfNotExists(this.connectionSource, SetPropertyValueRequestSummary.class);
    }

    public void add(ControlUnit controlUnit) throws SQLException {
        controlUnitDAO.createIfNotExists(controlUnit);
        controlUnitDAO.commit(this.databaseConnection);
        add(controlUnit.getAuthenticationKey());
        addDevices(controlUnit.getDevices());
    }

    public void add(AuthenticationKey authenticationKey) throws SQLException {
        authenticationKeyDAO.createIfNotExists(authenticationKey);
        authenticationKeyDAO.commit(this.databaseConnection);
    }

    public void addDevices(Collection<Device> devices) throws SQLException {
        for (Device device : devices) {
            add(device);
        }
    }

    public void add(Device device) throws SQLException {
        deviceDAO.createIfNotExists(device);
        deviceDAO.commit(this.databaseConnection);
        add(device.getComponentGroups());
        addDeviceAuthorizations(device.getDeviceAuthorizations());
    }

    private void addDeviceAuthorizations(Collection<DeviceAuthorization> deviceAuthorizations) throws SQLException {
        for (DeviceAuthorization deviceAuthorization : deviceAuthorizations) {
            add(deviceAuthorization);
        }
    }

    public void add(Collection<ComponentGroup> componentGroups) throws SQLException {
        for (ComponentGroup componentGroup : componentGroups) {
            add(componentGroup);
        }
    }

    public void add(ComponentGroup componentGroup) throws SQLException {
        componentGroupDAO.createIfNotExists(componentGroup);
        componentGroupDAO.commit(this.databaseConnection);
        addComponents(componentGroup.getComponents());
    }

    public void addComponents(Collection<Component> components) throws SQLException {
        for (Component component : components) {
            add(component);
        }
    }

    public void add(Component component) throws SQLException {
        componentDAO.createIfNotExists(component);
        componentDAO.commit(this.databaseConnection);
        addProperties(component.getProperties());
        addMethods(component.getMethods());
    }

    public void addProperties(Collection<Property> properties) throws SQLException {
        for (Property property : properties) {
            add(property);
        }
    }

    public void add(Property property) throws SQLException {
        propertyDAO.createIfNotExists(property);
        propertyDAO.commit(this.databaseConnection);
    }

    public void addMethods(Collection<Method> methods) throws SQLException {
        for (Method method : methods) {
            add(method);
        }
    }

    public void add(Method method) throws SQLException {
        methodDAO.createIfNotExists(method);
        methodDAO.commit(this.databaseConnection);
        addMethodParameterDefinitions(method.getMethodParameterDefinitions());
    }

    public void addMethodParameterDefinitions(Collection<MethodParameterDefinition> methodParameterDefinitions) throws SQLException {
        for (MethodParameterDefinition methodParameterDefinition : methodParameterDefinitions) {
            add(methodParameterDefinition);
        }
    }

    public void add(MethodParameterDefinition methodParameterDefinition) throws SQLException {
        methodParameterDefinitionDAO.createIfNotExists(methodParameterDefinition);
        methodParameterDefinitionDAO.commit(this.databaseConnection);
    }

    public void remove(ControlUnit controlUnit) throws SQLException {
        controlUnitDAO.delete(controlUnit);
        controlUnitDAO.commit(this.databaseConnection);
    }

    public void remove(Device device) throws SQLException {
        deviceDAO.delete(device);
        deviceDAO.commit(this.databaseConnection);
    }

    public void remove(ComponentGroup componentGroup) throws SQLException {
        componentGroupDAO.delete(componentGroup);
        componentGroupDAO.commit(this.databaseConnection);
    }

    public void update(ControlUnit controlUnit) throws SQLException {
        controlUnitDAO.update(controlUnit);
        controlUnitDAO.commit(this.databaseConnection);
    }

    public void update(Device device) throws SQLException {
        deviceDAO.update(device);
        deviceDAO.commit(this.databaseConnection);
    }

    public void update(ComponentGroup componentGroup) throws SQLException {
        componentGroupDAO.update(componentGroup);
        componentGroupDAO.commit(this.databaseConnection);
    }

    public void update(Component component) throws SQLException {
        componentDAO.update(component);
        componentDAO.commit(this.databaseConnection);
    }

    public <T> T queryForId(Class clazz, String id) throws SQLException {
        if (clazz.equals(Device.class)) {
            return (T) deviceDAO.queryForId(id);
        } else if (clazz.equals(Application.class)) {
            return (T) applicationDAO.queryForId(id);
        } else if (clazz.equals(ControlUnit.class)) {
            return (T) controlUnitDAO.queryForId(id);
        } else if (clazz.equals(User.class)) {
            return (T) userDAO.queryForId(id);
        } else if (clazz.equals(InstallComponentGroupRequest.class)) {
            return (T) installComponentGroupRequestDAO.queryForId(id);
        } else if (clazz.equals(InstallComponentGroupResponse.class)) {
            return (T) installComponentGroupResponseDAO.queryForId(id);
        } else if (clazz.equals(DeviceAuthorization.class)) {
            return (T) deviceAuthorizationDAO.queryForId(id);
        } else if (clazz.equals(ApplicationInvite.class)) {
            return (T) applicationInviteDAO.queryForId(id);
        } else if (clazz.equals(SetPropertyValueRequest.class)) {
            return (T) setPropertyValueRequestDAO.queryForId(id);
        } else if (clazz.equals(SetPropertyValueRequestSummary.class)) {
            return (T) setPropertyValueRequestSummaryDAO.queryForId(id);
        } else if (clazz.equals(Component.class)) {
            return (T) componentDAO.queryForId(id);
        } else {
            throw new IllegalArgumentException("Unknown class (" + clazz.getClass().getName() + "), can't query the database for that class.");
        }
    }

    public <T> List<T> queryForMatching(T object) throws SQLException {
        if (object instanceof Device) {
            return (List<T>) deviceDAO.queryForMatchingArgs((Device) object);
        } else if (object instanceof ComponentGroup) {
            return (List<T>) componentGroupDAO.queryForMatchingArgs((ComponentGroup) object);
        } else if (object instanceof User) {
            return (List<T>) userDAO.queryForMatchingArgs((User) object);
        } else if (object instanceof AuthenticationKey) {
            return (List<T>) authenticationKeyDAO.queryForMatchingArgs((AuthenticationKey) object);
        } else if (object instanceof Property) {
            return (List<T>) propertyDAO.queryForMatchingArgs((Property) object);
        } else if (object instanceof InstallComponentGroupRequest) {
            return (List<T>) installComponentGroupRequestDAO.queryForMatchingArgs((InstallComponentGroupRequest) object);
        } else if (object instanceof InstallComponentGroupResponse) {
            return (List<T>) installComponentGroupResponseDAO.queryForMatchingArgs((InstallComponentGroupResponse) object);
        } else if (object instanceof DeviceAuthorization) {
            return (List<T>) deviceAuthorizationDAO.queryForMatchingArgs((DeviceAuthorization) object);
        } else if (object instanceof Application) {
            return (List<T>) applicationDAO.queryForMatchingArgs((Application) object);
        } else if (object instanceof ApplicationInvite) {
            return (List<T>) applicationInviteDAO.queryForMatchingArgs((ApplicationInvite) object);
        } else if (object instanceof SetPropertyValueRequest) {
            return (List<T>) setPropertyValueRequestDAO.queryForMatchingArgs((SetPropertyValueRequest) object);
        } else if (object instanceof SetPropertyValueRequestSummary) {
            return (List<T>) setPropertyValueRequestSummaryDAO.queryForMatchingArgs((SetPropertyValueRequestSummary) object);
        } else if (object instanceof ControlUnit) {
            return (List<T>) controlUnitDAO.queryForMatchingArgs((ControlUnit) object);
        } else if (object instanceof Component) {
            return (List<T>) componentDAO.queryForMatchingArgs((Component) object);
        } else {
            throw new IllegalArgumentException("Unknown object (" + object.getClass().getSimpleName() + "), can't query the database for that object.");
        }
    }

    public <T> T queryForFirstMatching(T object) throws SQLException {
        List<T> results = queryForMatching(object);

        if (results.size() > 0) {
            return results.get(0);
        }

        return null;
    }

    public void closeConnection() throws SQLException {
        this.connectionSource.close();
    }

    public void add(SetPropertyValueResponse setPropertyValueResponse) throws SQLException {
        setPropertyValueResponseDAO.createIfNotExists(setPropertyValueResponse);
        setPropertyValueResponseDAO.commit(this.databaseConnection);
    }

    public void update(SetPropertyValueResponse setPropertyValueResponse) throws SQLException {
        setPropertyValueResponseDAO.update(setPropertyValueResponse);
        setPropertyValueResponseDAO.commit(this.databaseConnection);
    }

    public void remove(SetPropertyValueResponse setPropertyValueResponse) throws SQLException {
        setPropertyValueResponseDAO.delete(setPropertyValueResponse);
        setPropertyValueResponseDAO.commit(this.databaseConnection);
    }

    public void add(SetPropertyValueRequest setPropertyValueRequest) throws SQLException {
        setPropertyValueRequestDAO.createIfNotExists(setPropertyValueRequest);
        setPropertyValueRequestDAO.commit(this.databaseConnection);
    }

    public void update(SetPropertyValueRequest setPropertyValueRequest) throws SQLException {
        setPropertyValueRequestDAO.update(setPropertyValueRequest);
        setPropertyValueRequestDAO.commit(this.databaseConnection);
    }

    public void remove(SetPropertyValueRequest setPropertyValueRequest) throws SQLException {
        setPropertyValueRequestDAO.delete(setPropertyValueRequest);
        setPropertyValueRequestDAO.commit(this.databaseConnection);
    }

    public void add(InstallComponentGroupRequest installComponentGroupRequest) throws SQLException {
        installComponentGroupRequestDAO.createIfNotExists(installComponentGroupRequest);
        installComponentGroupRequestDAO.commit(this.databaseConnection);
    }

    public void update(InstallComponentGroupRequest installComponentGroupRequest) throws SQLException {
        installComponentGroupRequestDAO.update(installComponentGroupRequest);
        installComponentGroupRequestDAO.commit(this.databaseConnection);
    }

    public void remove(InstallComponentGroupRequest installComponentGroupRequest) throws SQLException {
        installComponentGroupRequestDAO.delete(installComponentGroupRequest);
        installComponentGroupRequestDAO.commit(this.databaseConnection);
    }

    public void add(InstallComponentGroupResponse installComponentGroupResponse) throws SQLException {
        installComponentGroupResponseDAO.createIfNotExists(installComponentGroupResponse);
        installComponentGroupResponseDAO.commit(this.databaseConnection);
    }

    public void update(InstallComponentGroupResponse installComponentGroupResponse) throws SQLException {
        installComponentGroupResponseDAO.update(installComponentGroupResponse);
        installComponentGroupResponseDAO.commit(this.databaseConnection);
    }

    public void remove(InstallComponentGroupResponse installComponentGroupResponse) throws SQLException {
        installComponentGroupResponseDAO.delete(installComponentGroupResponse);
        installComponentGroupResponseDAO.commit(this.databaseConnection);
    }

    public void add(User user) throws SQLException {
        userDAO.createIfNotExists(user);
        userDAO.commit(this.databaseConnection);
        add(user.getAuthenticationKey());
    }

    public void update(User user) throws SQLException {
        userDAO.update(user);
        userDAO.commit(this.databaseConnection);
    }

    public void remove(User user) throws SQLException {
        userDAO.delete(user);
        userDAO.commit(this.databaseConnection);
    }

    public void add(Application application) throws SQLException {
        applicationDAO.createIfNotExists(application);
        applicationDAO.commit(this.databaseConnection);
        add(application.getAuthenticationKey());
    }

    public void update(Application application) throws SQLException {
        applicationDAO.update(application);
        applicationDAO.commit(this.databaseConnection);
    }

    public void remove(Application application) throws SQLException {
        applicationDAO.delete(application);
        applicationDAO.commit(this.databaseConnection);
    }

    public void add(InstallDeviceRequest installDeviceRequest) throws SQLException {
        installDeviceRequestDAO.createIfNotExists(installDeviceRequest);
        installDeviceRequestDAO.commit(this.databaseConnection);
    }

    public void update(InstallDeviceRequest installDeviceRequest) throws SQLException {
        installDeviceRequestDAO.update(installDeviceRequest);
        installDeviceRequestDAO.commit(this.databaseConnection);
    }

    public void remove(InstallDeviceRequest installDeviceRequest) throws SQLException {
        installDeviceRequestDAO.delete(installDeviceRequest);
        installDeviceRequestDAO.commit(this.databaseConnection);
    }

    public void add(InstallDeviceResponse installDeviceResponse) throws SQLException {
        installDeviceResponseDAO.createIfNotExists(installDeviceResponse);
        installDeviceResponseDAO.commit(this.databaseConnection);
    }

    public void update(InstallDeviceResponse installDeviceResponse) throws SQLException {
        installDeviceResponseDAO.update(installDeviceResponse);
        installDeviceResponseDAO.commit(this.databaseConnection);
    }

    public void remove(InstallDeviceResponse installDeviceResponse) throws SQLException {
        installDeviceResponseDAO.delete(installDeviceResponse);
        installDeviceResponseDAO.commit(this.databaseConnection);
    }

    public void update(Property property) throws SQLException {
        propertyDAO.update(property);
        propertyDAO.commit(this.databaseConnection);
    }

    public void remove(Property property) throws SQLException {
        propertyDAO.delete(property);
        propertyDAO.commit(this.databaseConnection);
    }

    public void add(DeviceAuthorization deviceAuthorization) throws SQLException {
        deviceAuthorizationDAO.createIfNotExists(deviceAuthorization);
        deviceAuthorizationDAO.commit(this.databaseConnection);
    }

    public void update(DeviceAuthorization deviceAuthorization) throws SQLException {
        deviceAuthorizationDAO.update(deviceAuthorization);
        deviceAuthorizationDAO.commit(this.databaseConnection);
    }

    public void remove(DeviceAuthorization deviceAuthorization) throws SQLException {
        deviceAuthorizationDAO.delete(deviceAuthorization);
        deviceAuthorizationDAO.commit(this.databaseConnection);
    }

    public void add(ApplicationInvite applicationInvite) throws SQLException {
        applicationInviteDAO.createIfNotExists(applicationInvite);
        applicationInviteDAO.commit(this.databaseConnection);
    }

    public void update(ApplicationInvite applicationInvite) throws SQLException {
        applicationInviteDAO.update(applicationInvite);
        applicationInviteDAO.commit(this.databaseConnection);
    }

    public void remove(ApplicationInvite applicationInvite) throws SQLException {
        applicationInviteDAO.delete(applicationInvite);
        applicationInviteDAO.commit(this.databaseConnection);
    }

    public void add(SetPropertyValueRequestSummary setPropertyValueRequestSummary) throws SQLException {
        setPropertyValueRequestSummaryDAO.createIfNotExists(setPropertyValueRequestSummary);
        setPropertyValueRequestSummaryDAO.commit(this.databaseConnection);

        for (SetPropertyValueRequest subRequest : setPropertyValueRequestSummary.getSetPropertyValueRequests()) {
            add(subRequest);
        }
    }

    public void update(SetPropertyValueRequestSummary setPropertyValueRequestSummary) throws SQLException {
        setPropertyValueRequestSummaryDAO.update(setPropertyValueRequestSummary);
        setPropertyValueRequestSummaryDAO.commit(this.databaseConnection);
    }

    public void remove(SetPropertyValueRequestSummary setPropertyValueRequestSummary) throws SQLException {
        setPropertyValueRequestSummaryDAO.delete(setPropertyValueRequestSummary);
        setPropertyValueRequestSummaryDAO.commit(this.databaseConnection);
    }

    public void remove(Component component) throws SQLException {
        componentDAO.delete(component);
        componentDAO.commit(this.databaseConnection);
    }
}