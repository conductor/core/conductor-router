package org.conductor.router.core.types.publications;

/**
 * Created by Henrik on 08/09/2016.
 */
public class DeviceAuthorization {

    private String accessGivenTo;
    private String accessGivenBy;
    private Device device;

    public String getAccessGivenTo() {
        return accessGivenTo;
    }

    public void setAccessGivenTo(String accessGivenTo) {
        this.accessGivenTo = accessGivenTo;
    }

    public String getAccessGivenBy() {
        return accessGivenBy;
    }

    public void setAccessGivenBy(String accessGivenBy) {
        this.accessGivenBy = accessGivenBy;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }
}
