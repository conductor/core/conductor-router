package org.conductor.router.core.types.response.controlunit;

/**
 * Created by Henrik on 13/03/2016.
 */
public enum ResponseStatus {
    SUCCESS,
    WAITING,
    ERROR
}
