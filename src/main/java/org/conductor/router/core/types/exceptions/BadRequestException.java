package org.conductor.router.core.types.exceptions;

/**
 * Created by Henrik on 31/08/2016.
 */
public class BadRequestException extends Exception {

    public BadRequestException(String message) {
        super(message);
    }

}
