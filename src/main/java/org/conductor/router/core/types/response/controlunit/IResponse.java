package org.conductor.router.core.types.response.controlunit;

import org.conductor.router.core.entities.AuthenticationKey;

import java.util.UUID;

/**
 * Created by Henrik on 13/03/2016.
 */
public interface IResponse {

    String getId();
    void setId(String id);

    ResponseStatus getStatus();
    void setStatus(ResponseStatus status);

    AuthenticationKey getRequester();
    void setRequester(AuthenticationKey authenticationKey);

    String getErrorMessage();
    void setErrorMessage(String errorMessage);

    String getRequestId();
    void setRequestId(String requestId);

}
