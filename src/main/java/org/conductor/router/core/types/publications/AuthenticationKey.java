package org.conductor.router.core.types.publications;

import org.conductor.router.core.entities.enums.KeyType;

/**
 * Created by Henrik on 08/09/2016.
 */
public class AuthenticationKey {

    private String publicKey;
    private String privateKey;
    private KeyType keyType;

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public KeyType getKeyType() {
        return keyType;
    }

    public void setKeyType(KeyType keyType) {
        this.keyType = keyType;
    }
}
