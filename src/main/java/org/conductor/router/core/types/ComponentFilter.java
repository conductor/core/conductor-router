package org.conductor.router.core.types;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Henrik on 13/03/2016.
 */
public class ComponentFilter implements IFilter {

    private String deviceId;
    private String deviceName;
    private String deviceType;
    private String componentId;
    private String componentName;
    private String componentType;
    private List<String> publicKeys = new ArrayList<>();

    @Override
    public String getDeviceId() {
        return deviceId;
    }

    @Override
    public void setDeviceID(String deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public String getDeviceName() {
        return deviceName;
    }

    @Override
    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @Override
    public String getDeviceType() {
        return deviceType;
    }

    @Override
    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    @Override
    public String getComponentId() {
        return componentId;
    }

    @Override
    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    @Override
    public String getComponentName() {
        return componentName;
    }

    @Override
    public void setComponentName(String componentName) {
        this.componentName = componentName;
    }

    @Override
    public String getComponentType() {
        return componentType;
    }

    @Override
    public void setComponentType(String componentType) {
        this.componentType = componentType;
    }

    @Override
    public List<String> getPublicKeys() {
        return publicKeys;
    }

    @Override
    public void setPublicKeys(List<String> publicKeys) {
        this.publicKeys = publicKeys;
    }
}
