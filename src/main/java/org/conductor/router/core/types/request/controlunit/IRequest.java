package org.conductor.router.core.types.request.controlunit;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.Component;
import org.conductor.router.core.entities.ControlUnit;

import java.util.UUID;

/**
 * Created by Henrik on 13/03/2016.
 */
public interface IRequest {

    String getId();
    AuthenticationKey getRequester();
    ControlUnit getControlUnit();
    Component getComponent();

}
