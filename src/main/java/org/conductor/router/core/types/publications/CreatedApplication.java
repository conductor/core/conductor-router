package org.conductor.router.core.types.publications;

/**
 * Created by Henrik on 08/09/2016.
 */
public class CreatedApplication {

    private String id;
    private String name;
    private AuthenticationKey authenticationKey;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AuthenticationKey getAuthenticationKey() {
        return authenticationKey;
    }

    public void setAuthenticationKey(AuthenticationKey authenticationKey) {
        this.authenticationKey = authenticationKey;
    }
}
