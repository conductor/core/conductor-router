package org.conductor.router.core.types;

import java.util.List;

/**
 * Created by Henrik on 13/03/2016.
 */
public interface IFilter {

    String getDeviceId();
    void setDeviceID(String deviceId);

    String getDeviceName();
    void setDeviceName(String deviceName);

    String getDeviceType();
    void setDeviceType(String deviceType);

    String getComponentId();
    void setComponentId(String componentId);

    String getComponentName();
    void setComponentName(String componentName);

    String getComponentType();
    void setComponentType(String componentType);

    List<String> getPublicKeys();
    void setPublicKeys(List<String> publicKeys);
}
