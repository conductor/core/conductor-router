package org.conductor.router.core.types.publications;

import org.conductor.router.core.publisher.converters.request.SetPropertyValueRequest;
import org.conductor.router.core.types.response.controlunit.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henrik on 19/09/2016.
 */
public class SetPropertyValueRequestSummary {

    private String id;
    private String requester;
    private ResponseStatus status;
    private String errorMessage;
    private List<SetPropertyValueRequest> subRequests = new ArrayList();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

    public List<SetPropertyValueRequest> getSubRequests() {
        return subRequests;
    }

    public void setSubRequests(List<SetPropertyValueRequest> subRequests) {
        this.subRequests = subRequests;
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
