package org.conductor.router.core.types.publications;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Henrik on 07/09/2016.
 */
public class Device {

    private String id;
    private String name;
    private String type;
    private Collection<Component> components = new ArrayList();
    private Collection<String> deviceAuthorizations = new ArrayList();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Collection<Component> getComponents() {
        return components;
    }

    public void setComponents(Collection<Component> components) {
        this.components = components;
    }

    public Collection<String> getDeviceAuthorizations() {
        return deviceAuthorizations;
    }

    public void setDeviceAuthorizations(Collection<String> deviceAuthorizations) {
        this.deviceAuthorizations = deviceAuthorizations;
    }
}
