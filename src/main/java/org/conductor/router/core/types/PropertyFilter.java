package org.conductor.router.core.types;

/**
 * Created by Henrik on 13/03/2016.
 */
public class PropertyFilter extends ComponentFilter {

    private String propertyId;
    private String propertyName;
    private String propertyValue;

    public String getPropertyId() {
        return propertyId;
    }

    public void setPropertyId(String propertyId) {
        this.propertyId = propertyId;
    }

    public String getPropertyName() {
        return propertyName;
    }

    public void setPropertyName(String propertyName) {
        this.propertyName = propertyName;
    }

    public String getPropertyValue() {
        return propertyValue;
    }

    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }

}
