package org.conductor.router.core.types.response.controlunit;

import com.j256.ormlite.field.DatabaseField;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.types.request.controlunit.Request;

import java.util.UUID;

/**
 * Created by Henrik on 14/03/2016.
 */
public class Response implements IResponse {

    @DatabaseField(canBeNull = false, id = true)
    private String id;

    @DatabaseField(foreign = true, foreignAutoRefresh=true)
    private AuthenticationKey authenticationKey;

    @DatabaseField
    private ResponseStatus status;

    @DatabaseField
    private String errorMessage;

    @DatabaseField
    private String requestId;


    public Response() {

    }

    public Response(String id, String requestId, AuthenticationKey authenticationKey, ResponseStatus status) {
        this(id, requestId, authenticationKey, status, null);
    }

    public Response(String id, String requestId, AuthenticationKey authenticationKey, ResponseStatus status, String errorMessage) {
        this.id = id;
        this.requestId = requestId;
        this.authenticationKey = authenticationKey;
        this.status = status;
        this.errorMessage = errorMessage;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public void setId(String id) {
        this.id = id;
    }

    @Override
    public AuthenticationKey getRequester() {
        return authenticationKey;
    }

    @Override
    public void setRequester(AuthenticationKey requester) {
        this.authenticationKey = requester;
    }

    @Override
    public ResponseStatus getStatus() {
        return status;
    }

    @Override
    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    @Override
    public String getErrorMessage() { return errorMessage; }

    @Override
    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }
}
