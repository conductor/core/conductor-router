package org.conductor.router.core.types.exceptions;

/**
 * Created by Henrik on 13/03/2016.
 */
public class UnauthorizedException extends Exception {

    public UnauthorizedException(String message) {
        super(message);
    }

}
