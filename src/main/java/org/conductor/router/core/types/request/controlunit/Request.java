package org.conductor.router.core.types.request.controlunit;

import com.j256.ormlite.field.DatabaseField;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.Component;
import org.conductor.router.core.entities.ControlUnit;

import java.util.UUID;

/**
 * Created by Henrik on 13/03/2016.
 */
public abstract class Request implements IRequest {

    @DatabaseField(canBeNull = false, id = true)
    protected String id;

    @DatabaseField(foreign = true, foreignAutoRefresh=true)
    protected AuthenticationKey requester;

    @DatabaseField(foreign = true, foreignAutoRefresh=true)
    protected ControlUnit controlUnit;

    @DatabaseField(foreign = true, foreignAutoRefresh=true)
    protected Component component;

    @Override
    public String getId() {
        return id;
    }

    public AuthenticationKey getRequester() {
        return requester;
    }

    public void setRequester(AuthenticationKey key) {
        this.requester = key;
    }

    @Override
    public ControlUnit getControlUnit() { return controlUnit; }

    @Override
    public Component getComponent() { return component; }

}

