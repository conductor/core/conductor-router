package org.conductor.router.core.types.publications;

import org.conductor.router.core.entities.enums.ApplicationInviteStatus;

/**
 * Created by Henrik on 06/09/2016.
 */
public class ApplicationInvite {

    private String id;
    private String externalId;
    private Application application;
    private String userPublicKey;
    private ApplicationInviteStatus applicationInviteStatus;

    private String inviteUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public String getUserPublicKey() {
        return userPublicKey;
    }

    public void setUserPublicKey(String userPublicKey) {
        this.userPublicKey = userPublicKey;
    }

    public ApplicationInviteStatus getApplicationInviteStatus() {
        return applicationInviteStatus;
    }

    public void setApplicationInviteStatus(ApplicationInviteStatus applicationInviteStatus) {
        this.applicationInviteStatus = applicationInviteStatus;
    }

    public String getInviteUrl() {
        return inviteUrl;
    }

    public void setInviteUrl(String inviteUrl) {
        this.inviteUrl = inviteUrl;
    }

}
