package org.conductor.router.core.publisher.publications;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.CollectionListener;
import org.conductor.router.core.collection.ControlUnitCollection;
import org.conductor.router.core.collection.UserCollection;
import org.conductor.router.core.entities.*;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.publisher.converters.MessageConverter;
import org.conductor.router.core.publisher.event.PublicationMessage;
import org.conductor.router.core.types.exceptions.UnauthorizedException;
import org.conductor.router.service.websocket.model.managingdata.Sub;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Henrik on 09/11/2016.
 */
@Named
public class ControlUnitPublication extends Publication {

    private Logger log = LogManager.getLogger(getClass().getName());

    @Inject
    private MessageConverter messageConverter;

    @Inject
    private ControlUnitCollection controlUnitCollection;

    @Inject
    private UserCollection userCollection;

    @PostConstruct
    public void init() {
        controlUnitCollection.addListener(collectionListener);
    }

    @Override
    public void sub(AuthenticationKey authenticationKey, Sub sub, DDPSession session) throws UnauthorizedException {
        super.sub(authenticationKey, sub, session);

        User userQuery = new User();
        userQuery.setAuthenticationKey(authenticationKey);
        User user = userCollection.findFirst(userQuery);

        ControlUnit query = new ControlUnit();
        query.setOwner(user);
        query.setDevices(null);
        List<ControlUnit> controlUnits = controlUnitCollection.find(query);

        for (ControlUnit controlUnit : controlUnits) {
            send(PublicationType.Add, controlUnit);
        }
    }

    @Override
    public PublicationName getPublicationName() {
        return PublicationName.CONTROL_UNITS;
    }

    @Override
    public List<KeyType> getAuthorizedKeyTypes() {
        return Arrays.asList(KeyType.USER_KEY);
    }

    public String getCollection() {
        return "controlUnits";
    }

    private void send(PublicationType publicationType, ControlUnit controlUnit) {
        PublicationMessage message = createPublicationMessage(publicationType, controlUnit);
        List<DDPSession> sessions = getSubscribers(controlUnit);

        for (DDPSession session : sessions) {
            send(message, session);
        }
    }

    private void send(PublicationMessage message, DDPSession session) {
        try {
            session.sendMsg(message);
        } catch (Exception e) {
            log.warn("An error occurred while sending subscription event to subscriber.", e);
        }
    }

    private PublicationMessage createPublicationMessage(PublicationType publicationType, ControlUnit controlUnit) {
        PublicationMessage message = new PublicationMessage();
        message.setMsg(publicationType.toString());
        message.setCollection(getCollection());
        message.setId(controlUnit.getId().toString());
        try {
            message.setFields(messageConverter.convert(controlUnit, org.conductor.router.core.types.publications.ControlUnit.class));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return message;
    }

    private List<DDPSession> getSubscribers(ControlUnit controlUnit) {
        return super.getSubscribers(controlUnit.getOwner().getAuthenticationKey());
    }

    CollectionListener collectionListener = new CollectionListener<ControlUnit>() {

        @Override
        public void onAdded(ControlUnit controlUnit) {
            send(PublicationType.Add, controlUnit);
        }

        @Override
        public void onUpdated(ControlUnit controlUnit) {
            send(PublicationType.Changed, controlUnit);
        }

        @Override
        public void onRemoved(ControlUnit controlUnit) {
            send(PublicationType.Remove, controlUnit);
        }

    };

}
