package org.conductor.router.core.publisher.converters.request;

/**
 * Created by Henrik on 04/08/2016.
 */
public class InstallControlUnitRequest {

    private String requester;

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

}
