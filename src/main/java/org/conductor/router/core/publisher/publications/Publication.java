package org.conductor.router.core.publisher.publications;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.types.exceptions.UnauthorizedException;
import org.conductor.router.service.websocket.model.managingdata.Sub;
import org.conductor.router.service.websocket.model.managingdata.Unsub;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Henrik on 25/03/2016.
 */
public abstract class Publication implements IPublication {

    private Map<AuthenticationKey, List<DDPSession>> subscribers = new HashMap<>();

    @Override
    public void sub(AuthenticationKey authenticationKey, Sub sub, DDPSession session) throws UnauthorizedException {
        List<DDPSession> sessions = subscribers.get(authenticationKey);

        if (sessions == null) {
            sessions = new ArrayList<>();
        }

        sessions.add(session);
        subscribers.put(authenticationKey, sessions);
    }

    @Override
    public void unsub(Unsub unsub, DDPSession session) {

    }

    protected List<DDPSession> getSubscribers(AuthenticationKey authenticationKey) {
        List<DDPSession> sessions = subscribers.get(authenticationKey);

        if (sessions == null) {
            return new ArrayList<>();
        } else {
            return sessions;
        }
    }
}
