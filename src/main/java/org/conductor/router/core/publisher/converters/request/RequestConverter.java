package org.conductor.router.core.publisher.converters.request;

import org.conductor.router.core.types.request.controlunit.IRequest;
import org.dozer.Mapper;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;

/**
 * Created by Henrik on 15/03/2016.
 */
@Named
public class RequestConverter {

    @Inject
    private Mapper dozerMapper = null;

    public Object convert(IRequest request) throws IOException {
        if (request instanceof org.conductor.router.core.entities.SetPropertyValueRequest) {
            return dozerMapper.map(request, SetPropertyValueRequest.class);
        } else if (request instanceof org.conductor.router.core.entities.InstallDeviceRequest) {
            return dozerMapper.map(request, InstallDeviceRequest.class);
        } else if (request instanceof org.conductor.router.core.entities.InstallControlUnitRequest) {
            return dozerMapper.map(request, InstallControlUnitRequest.class);
        } else if (request instanceof org.conductor.router.core.entities.InstallComponentGroupRequest) {
            return dozerMapper.map(request, InstallComponentGroupRequest.class);
        }

        throw new RuntimeException("Unable to convert request. No converter exist for this request type.");
    }

}
