package org.conductor.router.core.publisher.converters;

import org.dozer.Mapper;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;

/**
 * Created by Henrik on 06/09/2016.
 */
@Named
public class MessageConverter {

    @Inject
    private Mapper dozerMapper;

    public Object convert(Object obj, Class clazz) throws IOException {
        return dozerMapper.map(obj, clazz);
    }

}
