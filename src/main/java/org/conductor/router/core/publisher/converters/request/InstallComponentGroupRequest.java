package org.conductor.router.core.publisher.converters.request;

import org.conductor.router.core.entities.ComponentGroup;

/**
 * Created by Henrik on 04/08/2016.
 */
public class InstallComponentGroupRequest {

    private ComponentGroup componentGroup;
    private String requester;

    public String getRequester() {
        return requester;
    }

    public void setRequester(String requester) {
        this.requester = requester;
    }

    public ComponentGroup getComponentGroup() {
        return componentGroup;
    }

    public void setComponentGroup(ComponentGroup componentGroup) {
        this.componentGroup = componentGroup;
    }
}
