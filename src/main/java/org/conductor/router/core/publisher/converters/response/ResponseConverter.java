package org.conductor.router.core.publisher.converters.response;

import com.google.common.base.Function;
import org.conductor.router.core.types.response.application.Response;
import org.conductor.router.core.types.response.controlunit.IResponse;
import org.conductor.router.core.entities.InstallComponentGroupResponse;
import org.conductor.router.core.entities.SetPropertyValueResponse;
import org.dozer.Mapper;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;

/**
 * Created by Henrik on 22/03/2016.
 */
@Named
public class ResponseConverter implements Function<IResponse, Response> {

    @Inject
    private Mapper dozerMapper;

    public Response convert(org.conductor.router.core.types.response.controlunit.Response response) throws IOException {
        Response resp = dozerMapper.map(response, Response.class);
        return resp;
    }

    @Override
    public Response apply(IResponse response) {
        try {
            if (response instanceof SetPropertyValueResponse) {
                return convert((SetPropertyValueResponse) response);
            } else if (response instanceof InstallComponentGroupResponse) {
                return convert((InstallComponentGroupResponse) response);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        throw new RuntimeException("Unable to convert response. No converter exist for this type.");
    }
}
