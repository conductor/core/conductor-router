package org.conductor.router.core.publisher.publications;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.message.Message;
import org.conductor.router.core.collection.*;
import org.conductor.router.core.entities.*;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.Device;
import org.conductor.router.core.entities.DeviceAuthorization;
import org.conductor.router.core.entities.Property;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.publisher.converters.MessageConverter;
import org.conductor.router.core.publisher.converters.request.RequestConverter;
import org.conductor.router.core.publisher.event.PublicationMessage;
import org.conductor.router.core.types.exceptions.UnauthorizedException;
import org.conductor.router.core.types.publications.*;
import org.conductor.router.service.websocket.model.managingdata.Sub;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Henrik on 08/08/2016.
 */
@Named
public class DevicePublication extends Publication {

    @Inject
    private DeviceCollection deviceCollection;

    @Inject
    private ComponentGroupCollection componentGroupCollection;

    @Inject
    private PropertyCollection propertyCollection;

    @Inject
    private RequestConverter requestConverter = null;

    @Inject
    private MessageConverter messageConverter;

    @Inject
    private DeviceAuthorizationCollection deviceAuthorizationCollection;

    private Logger log = LogManager.getLogger(getClass().getName());

    @PostConstruct
    public void init() {
        deviceAuthorizationCollection.addListener(deviceAuthorizationListener);
        deviceCollection.addListener(deviceCollectionListener);
        propertyCollection.addListener(propertyCollectionListener);
        componentGroupCollection.addListener(componentGroupCollectionListener);
    }

    @Override
    public void sub(AuthenticationKey authenticationKey, Sub sub, DDPSession session) throws UnauthorizedException {
        DeviceAuthorization deviceAuthorizationQuery = new DeviceAuthorization();
        deviceAuthorizationQuery.setAccessGivenTo(authenticationKey);
        List<DeviceAuthorization> deviceAuthorizations = deviceAuthorizationCollection.find(deviceAuthorizationQuery);

        for (DeviceAuthorization deviceAuthorization : deviceAuthorizations) {
            Device device = deviceCollection.findById(deviceAuthorization.getDevice().getId());
            PublicationMessage message = createPublicationMessage(PublicationType.Add, device);
            send(message, session);
        }

        super.sub(authenticationKey, sub, session);
    }

    @Override
    public PublicationName getPublicationName() {
        return PublicationName.DEVICES;
    }

    @Override
    public List<KeyType> getAuthorizedKeyTypes() {
        return Arrays.asList(KeyType.APPLICATION_KEY, KeyType.USER_KEY);
    }

    public String getCollection() {
        return "devices";
    }

    private void send(PublicationType publicationType, Device device) {
        PublicationMessage message = createPublicationMessage(publicationType, device);

        List<DDPSession> sessions = getSubscribers(device);
        send(message, sessions);
    }

    private void send(PublicationMessage message, List<DDPSession> sessions) {
        for (DDPSession session : sessions) {
            send(message, session);
        }
    }

    private void send(PublicationMessage message, DDPSession session) {
        try {
            session.sendMsg(message);
        } catch (Exception e) {
            log.warn("An error occurred while sending subscription event to subscriber.", e);
        }
    }

    private PublicationMessage createPublicationMessage(PublicationType publicationType, Device device) {
        PublicationMessage message = new PublicationMessage();
        message.setMsg(publicationType.toString());
        message.setCollection(getCollection());
        message.setId(device.getId().toString());

        try {
            message.setFields(messageConverter.convert(device, org.conductor.router.core.types.publications.Device.class));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return message;
    }

    private List<DDPSession> getSubscribers(Device device) {
        List<DDPSession> sessions = new ArrayList<>();

        for (DeviceAuthorization deviceAuthorization : device.getDeviceAuthorizations()) {
            sessions.addAll(super.getSubscribers(deviceAuthorization.getAccessGivenTo()));
        }

        return sessions;
    }

    private CollectionListener<Device> deviceCollectionListener = new CollectionListener<Device>() {

        @Override
        public void onAdded(Device device) {
            send(PublicationType.Add, device);
        }

        @Override
        public void onUpdated(Device device) {
            send(PublicationType.Changed, device);
        }

        @Override
        public void onRemoved(Device device) {
            send(PublicationType.Remove, device);
        }

    };

    private CollectionListener<ComponentGroup> componentGroupCollectionListener = new CollectionListener<ComponentGroup>() {

        @Override
        public void onAdded(ComponentGroup componentGroup) {
            Device device = deviceCollection.findById(componentGroup.getDevice().getId());
            send(PublicationType.Changed, device);
        }

        @Override
        public void onUpdated(ComponentGroup componentGroup) {
            Device device = deviceCollection.findById(componentGroup.getDevice().getId());
            send(PublicationType.Changed, device);
        }

        @Override
        public void onRemoved(ComponentGroup componentGroup) {
            Device device = deviceCollection.findById(componentGroup.getDevice().getId());
            send(PublicationType.Changed, device);
        }

    };

    private CollectionListener<Property> propertyCollectionListener = new CollectionListener<Property>() {

        @Override
        public void onAdded(Property property) {
        }

        @Override
        public void onUpdated(final Property property) {
            Device device = deviceCollection.findById(property.getComponent().getComponentGroup().getDevice().getId());
            send(PublicationType.Changed, device);
        }

        @Override
        public void onRemoved(Property property) {

        }

    };

    private CollectionListener<DeviceAuthorization> deviceAuthorizationListener = new CollectionListener<DeviceAuthorization>() {

        @Override
        public void onAdded(DeviceAuthorization deviceAuthorization) {
            List<DDPSession> subscribers = getSubscribers(deviceAuthorization.getAccessGivenTo());
            PublicationMessage message = createPublicationMessage(PublicationType.Add, deviceAuthorization.getDevice());
            send(message, subscribers);
        }

        @Override
        public void onUpdated(DeviceAuthorization deviceAuthorization) {
            // What should we send when device authorization has been changed? Just ignore it?
        }

        @Override
        public void onRemoved(DeviceAuthorization deviceAuthorization) {
            List<DDPSession> subscribers = getSubscribers(deviceAuthorization.getAccessGivenTo());
            PublicationMessage message = createPublicationMessage(PublicationType.Remove, deviceAuthorization.getDevice());
            send(message, subscribers);
        }

    };

}
