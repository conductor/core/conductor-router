package org.conductor.router.core.publisher.publications;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.CollectionListener;
import org.conductor.router.core.collection.InstallDeviceResponseCollection;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.publisher.converters.response.ResponseConverter;
import org.conductor.router.core.publisher.event.PublicationMessage;
import org.conductor.router.core.types.response.controlunit.IResponse;
import org.conductor.router.core.entities.InstallDeviceResponse;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Henrik on 03/08/2016.
 */
@Named
public class InstallDeviceResponsePublication extends Publication {

    @Inject
    private InstallDeviceResponseCollection collection;

    @Inject
    private ResponseConverter responseConverter = null;

    private Logger log = LogManager.getLogger(getClass().getName());

    @PostConstruct
    public void init() {
        collection.addListener(collectionListener);
    }

    @Override
    public PublicationName getPublicationName() {
        return PublicationName.INSTALL_DEVICE_RESPONSES;
    }

    @Override
    public List<KeyType> getAuthorizedKeyTypes() {
        return Arrays.asList(KeyType.USER_KEY);
    }

    public String getCollection() {
        return "installDeviceResponses";
    }

    private void send(PublicationType publicationType, InstallDeviceResponse response) {
        PublicationMessage message = new PublicationMessage();
        message.setMsg(publicationType.toString());
        message.setCollection(getCollection());
        message.setId(response.getId().toString());

        try {
            message.setFields(responseConverter.convert(response));
        } catch (IOException e) {
            log.error("Failed to convert request. ", e);
            return;
        }

        List<DDPSession> sessions = getSubscribers(response);
        for (DDPSession session : sessions) {
            try {
                session.sendMsg(message);
            } catch (Exception e) {
                log.warn("An error occurred while sending subscription event to subscriber.", e);
            }
        }
    }

    private List<DDPSession> getSubscribers(IResponse response) {
        return super.getSubscribers(response.getRequester());
    }

    private CollectionListener<InstallDeviceResponse> collectionListener = new CollectionListener<InstallDeviceResponse>() {

        @Override
        public void onAdded(InstallDeviceResponse response) {
            send(PublicationType.Add, response);
        }

        @Override
        public void onUpdated(InstallDeviceResponse response) {
            send(PublicationType.Changed, response);
        }

        @Override
        public void onRemoved(InstallDeviceResponse response) {
            send(PublicationType.Remove, response);
        }

    };

}