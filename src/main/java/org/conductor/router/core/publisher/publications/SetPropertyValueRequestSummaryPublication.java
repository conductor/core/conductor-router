package org.conductor.router.core.publisher.publications;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.CollectionListener;
import org.conductor.router.core.collection.SetPropertyValueRequestSummaryCollection;
import org.conductor.router.core.entities.SetPropertyValueRequestSummary;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.publisher.converters.MessageConverter;
import org.conductor.router.core.publisher.event.PublicationMessage;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Henrik on 19/09/2016.
 */
@Named
public class SetPropertyValueRequestSummaryPublication extends Publication {

    private Logger log = LogManager.getLogger(getClass().getName());

    @Inject
    private SetPropertyValueRequestSummaryCollection collection = null;

    @Inject
    private MessageConverter messageConverter = null;

    @PostConstruct
    public void init() {
        collection.addListener(collectionListener);
    }

    @Override
    public PublicationName getPublicationName() {
        return PublicationName.SET_PROPERTY_VALUE_REQUEST_SUMMARY;
    }

    @Override
    public List<KeyType> getAuthorizedKeyTypes() {
        return Arrays.asList(KeyType.APPLICATION_KEY, KeyType.USER_KEY);
    }

    public String getCollection() {
        return "setPropertyValueRequestSummaries";
    }

    private List<DDPSession> getSubscribers(SetPropertyValueRequestSummary request) {
        List<DDPSession> sessions = new ArrayList<>();
        sessions.addAll(super.getSubscribers(request.getRequester()));
        return sessions;
    }

    private void send(PublicationType publicationType, SetPropertyValueRequestSummary request) {
        PublicationMessage message = new PublicationMessage();
        message.setMsg(publicationType.toString());
        message.setCollection(getCollection());
        message.setId(request.getId().toString());

        try {
            message.setFields(messageConverter.convert(request, org.conductor.router.core.types.publications.SetPropertyValueRequestSummary.class));
        } catch (IOException e) {
            log.error("Failed to convert request. ", e);
            return;
        }

        List<DDPSession> sessions = getSubscribers(request);
        for (DDPSession session : sessions) {
            try {
                session.sendMsg(message);
            } catch (Exception e) {
                log.warn("An error occurred while sending subscription event to subscriber.", e);
            }
        }
    }

    private CollectionListener<SetPropertyValueRequestSummary> collectionListener = new CollectionListener<SetPropertyValueRequestSummary>() {

        @Override
        public void onAdded(SetPropertyValueRequestSummary request) {
            send(PublicationType.Add, request);
        }

        @Override
        public void onUpdated(SetPropertyValueRequestSummary request) {
            send(PublicationType.Changed, request);
        }

        @Override
        public void onRemoved(SetPropertyValueRequestSummary request) {
            send(PublicationType.Remove, request);
        }

    };

}
