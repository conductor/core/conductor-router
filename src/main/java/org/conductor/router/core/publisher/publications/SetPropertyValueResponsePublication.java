package org.conductor.router.core.publisher.publications;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.CollectionListener;
import org.conductor.router.core.collection.SetPropertyValueResponseCollection;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.publisher.converters.response.ResponseConverter;
import org.conductor.router.core.publisher.event.PublicationMessage;
import org.conductor.router.core.types.response.controlunit.IResponse;
import org.conductor.router.core.entities.SetPropertyValueResponse;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Henrik on 24/07/2016.
 */
@Named
public class SetPropertyValueResponsePublication extends Publication {

    @Inject
    private SetPropertyValueResponseCollection collection;

    @Inject
    private ResponseConverter responseConverter;

    private Logger log = LogManager.getLogger(getClass().getName());

    @PostConstruct
    public void init() {
        collection.addListener(collectionListener);
    }

    @Override
    public PublicationName getPublicationName() {
        return PublicationName.SET_PROPERTY_VALUE_RESPONSES;
    }

    @Override
    public List<KeyType> getAuthorizedKeyTypes() {
        return Arrays.asList(KeyType.APPLICATION_KEY);
    }

    public String getCollection() {
        return "setPropertyValueResponses";
    }

    private List<DDPSession> getSubscribers(IResponse response) {
        return super.getSubscribers(response.getRequester());
    }

    private void send(PublicationType publicationType, SetPropertyValueResponse response) {
        PublicationMessage message = new PublicationMessage();
        message.setMsg(publicationType.toString());
        message.setCollection(getCollection());
        message.setId(response.getId().toString());

        try {
            message.setFields(responseConverter.convert(response));
        } catch (IOException e) {
            log.error("Failed to convert request. ", e);
            return;
        }

        List<DDPSession> sessions = getSubscribers(response);
        for (DDPSession session : sessions) {
            try {
                session.sendMsg(message);
            } catch (Exception e) {
                log.warn("An error occurred while sending subscription event to subscriber.", e);
            }
        }
    }

    private CollectionListener<SetPropertyValueResponse> collectionListener = new CollectionListener<SetPropertyValueResponse>() {

        @Override
        public void onAdded(SetPropertyValueResponse response) {
            send(PublicationType.Add, response);
        }

        @Override
        public void onUpdated(SetPropertyValueResponse response) {
            send(PublicationType.Changed, response);
        }

        @Override
        public void onRemoved(SetPropertyValueResponse response) {
            send(PublicationType.Remove, response);
        }

    };
}
