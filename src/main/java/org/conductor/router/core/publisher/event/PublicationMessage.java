package org.conductor.router.core.publisher.event;

import org.conductor.router.service.websocket.model.HasMsg;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henrik on 23/03/2016.
 */
public class PublicationMessage implements HasMsg {

    private String msg;
    private String id;
    private String collection;
    private Object fields;

    public PublicationMessage() {

    }

    public PublicationMessage(String msg, String id, String collection, List<Object> fields) {
        this.msg = msg;
        this.id = id;
        this.collection = collection;
        this.fields = fields;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCollection() {
        return collection;
    }

    public void setCollection(String collection) {
        this.collection = collection;
    }

    public Object getFields() {
        return fields;
    }

    public void setFields(Object fields) {
        this.fields = fields;
    }

}
