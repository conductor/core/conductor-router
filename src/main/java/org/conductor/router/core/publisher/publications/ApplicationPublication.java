package org.conductor.router.core.publisher.publications;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.ApplicationInviteCollection;
import org.conductor.router.core.collection.CollectionListener;
import org.conductor.router.core.collection.UserCollection;
import org.conductor.router.core.entities.Application;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.User;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.publisher.converters.MessageConverter;
import org.conductor.router.core.publisher.event.PublicationMessage;
import org.conductor.router.core.entities.ApplicationInvite;
import org.conductor.router.core.entities.enums.ApplicationInviteStatus;
import org.conductor.router.core.types.exceptions.UnauthorizedException;
import org.conductor.router.service.websocket.model.managingdata.Sub;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Henrik on 04/09/2016.
 */
@Named
public class ApplicationPublication extends Publication {

    private Logger log = LogManager.getLogger(getClass().getName());

    @Inject
    public ApplicationInviteCollection applicationInviteCollection;

    @Inject
    public UserCollection userCollection;

    @Inject
    private MessageConverter messageConverter;

    @PostConstruct
    public void init() {
        applicationInviteCollection.addListener(applicationInviteListener);
    }

    @Override
    public void sub(AuthenticationKey authenticationKey, Sub sub, DDPSession session) throws UnauthorizedException {
        ApplicationInvite applicationInvite = new ApplicationInvite();
        User userQuery = new User();
        userQuery.setAuthenticationKey(authenticationKey);
        User user = userCollection.findFirst(userQuery);
        applicationInvite.setUser(user);
        List<ApplicationInvite> applicationInvites = applicationInviteCollection.find(applicationInvite);

        List<Application> applications = new ArrayList();
        applicationInvites.forEach(invite -> applications.add(invite.getApplication()));
        List<Application> distinctApplications = applications.stream().distinct().collect(Collectors.toList());

        for (Application application : distinctApplications) {
            PublicationMessage message = createPublicationMessage(PublicationType.Add, application);
            send(message, session);
        }

        super.sub(authenticationKey, sub, session);
    }

    @Override
    public PublicationName getPublicationName() {
        return PublicationName.APPLICATIONS;
    }

    @Override
    public List<KeyType> getAuthorizedKeyTypes() {
        return Arrays.asList(KeyType.USER_KEY);
    }

    public String getCollection() {
        return "applications";
    }

    private void send(PublicationType publicationType, ApplicationInvite applicationInvite) {
        PublicationMessage message = createPublicationMessage(publicationType, applicationInvite);
        List<DDPSession> sessions = getSubscribers(applicationInvite);

        for (DDPSession session : sessions) {
            send(message, session);
        }
    }

    private void send(PublicationMessage message, DDPSession session) {
        try {
            session.sendMsg(message);
        } catch (Exception e) {
            log.warn("An error occurred while sending subscription event to subscriber.", e);
        }
    }

    private PublicationMessage createPublicationMessage(PublicationType publicationType, ApplicationInvite applicationInvite) {
        return createPublicationMessage(publicationType, applicationInvite.getApplication());
    }

    private PublicationMessage createPublicationMessage(PublicationType publicationType, Application application) {
        PublicationMessage message = new PublicationMessage();
        message.setMsg(publicationType.toString());
        message.setCollection(getCollection());
        message.setId(application.getId().toString());
        try {
            message.setFields(messageConverter.convert(application, org.conductor.router.core.types.publications.Application.class));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return message;
    }

    private List<DDPSession> getSubscribers(ApplicationInvite applicationInvite) {
        return super.getSubscribers(applicationInvite.getUser().getAuthenticationKey());
    }

    private CollectionListener<ApplicationInvite> applicationInviteListener = new CollectionListener<ApplicationInvite>() {

        @Override
        public void onAdded(ApplicationInvite applicationInvite) {
            if (applicationInvite.getUser() != null && applicationInvite.getApplicationInviteStatus().equals(ApplicationInviteStatus.ACCEPTED)) {
                send(PublicationType.Add, applicationInvite);
            }
        }

        @Override
        public void onUpdated(ApplicationInvite applicationInvite) {
            if (applicationInvite.getUser() != null) {
                send(PublicationType.Changed, applicationInvite);
            }
        }

        @Override
        public void onRemoved(ApplicationInvite applicationInvite) {
            if (applicationInvite.getUser() != null) {
                send(PublicationType.Remove, applicationInvite);
            }
        }

    };

}
