package org.conductor.router.core.publisher.event;

/**
 * Created by Henrik on 23/03/2016.
 */
public enum EventType {
    Add("added"),
    Update("updated"),
    Remove("removed")
    ;

    private final String name;

    EventType(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
