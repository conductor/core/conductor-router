package org.conductor.router.core.publisher.publications;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.CollectionListener;
import org.conductor.router.core.collection.InstallDeviceRequestCollection;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.publisher.converters.request.RequestConverter;
import org.conductor.router.core.publisher.event.PublicationMessage;
import org.conductor.router.core.types.request.controlunit.IRequest;
import org.conductor.router.core.entities.InstallDeviceRequest;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Henrik on 03/08/2016.
 */
@Named
public class InstallDeviceRequestPublication extends Publication {

    @Inject
    private InstallDeviceRequestCollection collection;

    @Inject
    private RequestConverter requestConverter = null;

    private Logger log = LogManager.getLogger(getClass().getName());

    @PostConstruct
    public void init() {
        collection.addListener(collectionListener);
    }

    @Override
    public PublicationName getPublicationName() {
        return PublicationName.INSTALL_DEVICE_REQUESTS;
    }

    @Override
    public List<KeyType> getAuthorizedKeyTypes() {
        return Arrays.asList(KeyType.USER_KEY);
    }

    public String getCollection() {
        return "installDeviceRequests";
    }

    private void send(PublicationType publicationType, InstallDeviceRequest request) {
        PublicationMessage message = new PublicationMessage();
        message.setMsg(publicationType.toString());
        message.setCollection(getCollection());
        message.setId(request.getId().toString());

        try {
            message.setFields(requestConverter.convert(request));
        } catch (IOException e) {
            log.error("Failed to convert request. ", e);
            return;
        }

        List<DDPSession> sessions = getSubscribers(request);
        for (DDPSession session : sessions) {
            try {
                session.sendMsg(message);
            } catch (Exception e) {
                log.warn("An error occurred while sending subscription event to subscriber.", e);
            }
        }
    }

    private List<DDPSession> getSubscribers(IRequest request) {
        return super.getSubscribers(request.getRequester());
    }

    private CollectionListener<InstallDeviceRequest> collectionListener = new CollectionListener<InstallDeviceRequest>() {

        @Override
        public void onAdded(InstallDeviceRequest request) {
            send(PublicationType.Add, request);
        }

        @Override
        public void onUpdated(InstallDeviceRequest request) {
            send(PublicationType.Changed, request);
        }

        @Override
        public void onRemoved(InstallDeviceRequest request) {
            send(PublicationType.Remove, request);
        }

    };

}