package org.conductor.router.core.publisher.publications;

import com.fasterxml.jackson.annotation.JsonCreator;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Henrik on 17/03/2016.
 */
public enum PublicationName {
    SET_PROPERTY_VALUE_REQUESTS,
    SET_PROPERTY_VALUE_RESPONSES,
    INSTALL_COMPONENT_GROUP_REQUESTS,
    INSTALL_COMPONENT_GROUP_RESPONSES,
    INSTALL_DEVICE_REQUESTS,
    INSTALL_DEVICE_RESPONSES,
    DEVICES,
    APPLICATION_INVITES,
    APPLICATIONS,
    CONTROL_UNITS,
    SET_PROPERTY_VALUE_REQUEST_SUMMARY,
    METEOR_AUTOUPDATE_CLIENTVERSIONS,
    PROPERTIES_CHANGED
    ;

    private static Map<String, PublicationName> namesMap = new HashMap<String, PublicationName>();

    static {
        namesMap.put("meteor_autoupdate_clientversions", METEOR_AUTOUPDATE_CLIENTVERSIONS);
        namesMap.put("command.set_property_value_requests", SET_PROPERTY_VALUE_REQUESTS);
        namesMap.put("command.set_property_value_responses", SET_PROPERTY_VALUE_RESPONSES);
        namesMap.put("command.install_component_group_requests", INSTALL_COMPONENT_GROUP_REQUESTS);
        namesMap.put("command.install_component_group_responses", INSTALL_COMPONENT_GROUP_RESPONSES);
        namesMap.put("command.install_device_requests", INSTALL_DEVICE_REQUESTS);
        namesMap.put("command.install_device_responses", INSTALL_DEVICE_RESPONSES);
        namesMap.put("devices", DEVICES);
        namesMap.put("application.invites", APPLICATION_INVITES);
        namesMap.put("command.set_property_value_request_summaries", SET_PROPERTY_VALUE_REQUEST_SUMMARY);
        namesMap.put("user.applications", APPLICATIONS);
        namesMap.put("control_units", CONTROL_UNITS);
        namesMap.put("properties_changed", PROPERTIES_CHANGED);
    }

    @JsonCreator
    public static PublicationName forValue(String value) {
        return namesMap.get(StringUtils.lowerCase(value));
    }
}
