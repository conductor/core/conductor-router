package org.conductor.router.core.publisher.event;

import org.conductor.router.core.publisher.publications.PublicationName;

/**
 * Created by Henrik on 23/03/2016.
 */
public class Event {

    private EventType eventType;
    private Object item;
    private PublicationName publication;

    public Event(EventType eventType, PublicationName publication, Object item) {
        this.eventType = eventType;
        this.publication = publication;
        this.item = item;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public Object getItem() {
        return item;
    }

    public void setItem(Object item) {
        this.item = item;
    }

    public PublicationName getPublication() {
        return publication;
    }

    public void setPublication(PublicationName publication) {
        this.publication = publication;
    }
}
