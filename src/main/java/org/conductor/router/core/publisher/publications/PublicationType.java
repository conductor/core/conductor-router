package org.conductor.router.core.publisher.publications;

/**
 * Created by Henrik on 24/07/2016.
 */
public enum PublicationType {
    Add("added"),
    Changed("changed"),
    Remove("removed")
            ;

    private final String name;

    PublicationType(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        return (otherName == null) ? false : name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
