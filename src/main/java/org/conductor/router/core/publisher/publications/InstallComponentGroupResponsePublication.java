package org.conductor.router.core.publisher.publications;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.CollectionListener;
import org.conductor.router.core.collection.InstallComponentGroupResponseCollection;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.publisher.converters.response.ResponseConverter;
import org.conductor.router.core.publisher.event.PublicationMessage;
import org.conductor.router.core.types.response.controlunit.IResponse;
import org.conductor.router.core.entities.InstallComponentGroupResponse;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Henrik on 25/07/2016.
 */
@Named
public class InstallComponentGroupResponsePublication extends Publication {


    @Inject
    private InstallComponentGroupResponseCollection collection;

    @Inject
    private ResponseConverter responseConverter;

    private Logger log = LogManager.getLogger(getClass().getName());

    @PostConstruct
    public void init() {
        collection.addListener(collectionListener);
    }

    @Override
    public PublicationName getPublicationName() {
        return PublicationName.INSTALL_COMPONENT_GROUP_RESPONSES;
    }

    @Override
    public List<KeyType> getAuthorizedKeyTypes() {
        return Arrays.asList(KeyType.CONTROL_UNIT_KEY, KeyType.USER_KEY);
    }

    public String getCollection() {
        return "InstallComponentGroupResponses";
    }

    private void send(PublicationType publicationType, InstallComponentGroupResponse response) {
        PublicationMessage message = new PublicationMessage();
        message.setMsg(publicationType.toString());
        message.setCollection(getCollection());
        message.setId(response.getId().toString());

        try {
            message.setFields(responseConverter.convert(response));
        } catch (IOException e) {
            log.error("Failed to convert request. ", e);
            return;
        }

        List<DDPSession> sessions = getSubscribers(response);
        for (DDPSession session : sessions) {
            try {
                session.sendMsg(message);
            } catch (Exception e) {
                log.warn("An error occurred while sending subscription event to subscriber.", e);
            }
        }
    }

    private List<DDPSession> getSubscribers(IResponse response) {
        return super.getSubscribers(response.getRequester());
    }

    private CollectionListener<InstallComponentGroupResponse> collectionListener = new CollectionListener<InstallComponentGroupResponse>() {

        @Override
        public void onAdded(InstallComponentGroupResponse request) {
            send(PublicationType.Add, request);
        }

        @Override
        public void onUpdated(InstallComponentGroupResponse request) {
            send(PublicationType.Changed, request);
        }

        @Override
        public void onRemoved(InstallComponentGroupResponse request) {
            send(PublicationType.Remove, request);
        }

    };
}
