package org.conductor.router.core.publisher.publications;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.CollectionListener;
import org.conductor.router.core.collection.SetPropertyValueRequestCollection;
import org.conductor.router.core.collection.SetPropertyValueRequestSummaryCollection;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.SetPropertyValueRequest;
import org.conductor.router.core.entities.SetPropertyValueRequestSummary;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.publisher.converters.request.RequestConverter;
import org.conductor.router.core.publisher.event.PublicationMessage;
import org.conductor.router.core.types.exceptions.UnauthorizedException;
import org.conductor.router.core.types.request.controlunit.IRequest;
import org.conductor.router.service.websocket.model.managingdata.Sub;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Henrik on 17/03/2016.
 */
@Named
public class SetPropertyValueRequestPublication extends Publication {

    @Inject
    private RequestConverter requestConverter = null;

    @Inject
    private SetPropertyValueRequestSummaryCollection collection = null;

    private Logger log = LogManager.getLogger(getClass().getName());

    @PostConstruct
    public void init() {
        collection.addListener(collectionListener);
    }

    @Override
    public void sub(AuthenticationKey authenticationKey, Sub sub, DDPSession session) throws UnauthorizedException {
        super.sub(authenticationKey, sub, session);
        // TODO: Send all "set property value requests" when user subscribes
    }

    @Override
    public PublicationName getPublicationName() {
        return PublicationName.SET_PROPERTY_VALUE_REQUESTS;
    }

    @Override
    public List<KeyType> getAuthorizedKeyTypes() {
        return Arrays.asList(KeyType.APPLICATION_KEY, KeyType.CONTROL_UNIT_KEY);
    }

    public String getCollection() {
        return "setPropertyValueRequests";
    }

    private List<DDPSession> getSubscribers(IRequest request) {
        List<DDPSession> sessions = new ArrayList<>();
        sessions.addAll(super.getSubscribers(request.getControlUnit().getAuthenticationKey()));
        sessions.addAll(super.getSubscribers(request.getRequester()));
        return sessions;
    }

    private void send(PublicationType publicationType, SetPropertyValueRequest request) {
        PublicationMessage message = new PublicationMessage();
        message.setMsg(publicationType.toString());
        message.setCollection(getCollection());
        message.setId(request.getId().toString());

        try {
            message.setFields(requestConverter.convert(request));
        } catch (IOException e) {
            log.error("Failed to convert request. ", e);
            return;
        }

        List<DDPSession> sessions = getSubscribers(request);
        for (DDPSession session : sessions) {
            try {
                session.sendMsg(message);
            } catch (Exception e) {
                log.warn("An error occurred while sending subscription event to subscriber.", e);
            }
        }
    }

    private CollectionListener<SetPropertyValueRequestSummary> collectionListener = new CollectionListener<SetPropertyValueRequestSummary>() {

        @Override
        public void onAdded(SetPropertyValueRequestSummary requestSummary) {
            requestSummary.getRequests().forEach(request -> send(PublicationType.Add, request));
        }

        @Override
        public void onUpdated(SetPropertyValueRequestSummary requestSummary) {
            requestSummary.getRequests().forEach(request -> send(PublicationType.Changed, request));
        }

        @Override
        public void onRemoved(SetPropertyValueRequestSummary requestSummary) {
            requestSummary.getRequests().forEach(request -> send(PublicationType.Remove, request));
        }

    };

}
