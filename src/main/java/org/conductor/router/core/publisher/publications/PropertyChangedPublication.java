package org.conductor.router.core.publisher.publications;

import com.sun.jna.platform.win32.Guid;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.*;
import org.conductor.router.core.entities.Device;
import org.conductor.router.core.entities.DeviceAuthorization;
import org.conductor.router.core.entities.Property;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.publisher.converters.MessageConverter;
import org.conductor.router.core.publisher.converters.request.RequestConverter;
import org.conductor.router.core.publisher.event.PublicationMessage;
import org.conductor.router.core.types.publications.PropertyChanged;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Henrik on 19/12/2016.
 */
@Named
public class PropertyChangedPublication extends Publication {

    @Inject
    private DeviceCollection deviceCollection;

    @Inject
    private PropertyCollection propertyCollection;

    @Inject
    private RequestConverter requestConverter = null;

    @Inject
    private MessageConverter messageConverter;

    @Inject
    private DeviceAuthorizationCollection deviceAuthorizationCollection;

    private Logger log = LogManager.getLogger(getClass().getName());

    @PostConstruct
    public void init() {
        propertyCollection.addListener(propertyCollectionListener);
    }

    @Override
    public PublicationName getPublicationName() {
        return PublicationName.PROPERTIES_CHANGED;
    }

    @Override
    public List<KeyType> getAuthorizedKeyTypes() {
        return Arrays.asList(KeyType.APPLICATION_KEY);
    }

    public String getCollection() {
        return "properties_changed";
    }

    private void send(PublicationType publicationType, Device device, Property property) {
        PublicationMessage message = createPublicationMessage(publicationType, property);
        message.setId(Guid.GUID.newGuid().toString());

        List<DDPSession> sessions = getSubscribers(device);
        send(message, sessions);
    }

    private void send(PublicationMessage message, List<DDPSession> sessions) {
        for (DDPSession session : sessions) {
            send(message, session);
        }
    }

    private void send(PublicationMessage message, DDPSession session) {
        try {
            session.sendMsg(message);
        } catch (Exception e) {
            log.warn("An error occurred while sending subscription event to subscriber.", e);
        }
    }

    private PublicationMessage createPublicationMessage(PublicationType publicationType, Property property) {
        PublicationMessage message = new PublicationMessage();
        message.setMsg(publicationType.toString());
        message.setCollection(getCollection());
        message.setId(property.getId().toString());

        try {
            message.setFields(messageConverter.convert(property, PropertyChanged.class));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return message;
    }

    private List<DDPSession> getSubscribers(Device device) {
        List<DDPSession> sessions = new ArrayList<>();

        for (DeviceAuthorization deviceAuthorization : device.getDeviceAuthorizations()) {
            sessions.addAll(super.getSubscribers(deviceAuthorization.getAccessGivenTo()));
        }

        return sessions;
    }

    private CollectionListener<Property> propertyCollectionListener = new CollectionListener<Property>() {

        @Override
        public void onAdded(Property property) {
        }

        @Override
        public void onUpdated(final Property property) {
            Device device = deviceCollection.findById(property.getComponent().getComponentGroup().getDevice().getId());
            send(PublicationType.Changed, device, property);
        }

        @Override
        public void onRemoved(Property property) {

        }

    };

}
