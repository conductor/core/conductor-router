package org.conductor.router.core.publisher.publications;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.types.exceptions.UnauthorizedException;
import org.conductor.router.service.websocket.model.managingdata.Sub;
import org.conductor.router.service.websocket.model.managingdata.Unsub;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import java.util.List;

/**
 * Created by Henrik on 17/03/2016.
 */
public interface IPublication {

    PublicationName getPublicationName();
    List<KeyType> getAuthorizedKeyTypes();
    void sub(AuthenticationKey authenticationKey, Sub sub, DDPSession session) throws UnauthorizedException;
    void unsub(Unsub unsub, DDPSession session);
}
