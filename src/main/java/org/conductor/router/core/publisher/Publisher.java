package org.conductor.router.core.publisher;

import org.conductor.router.core.collection.AuthenticationKeyCollection;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.publisher.publications.*;
import org.conductor.router.core.types.exceptions.UnauthorizedException;
import org.conductor.router.service.websocket.model.managingdata.Nosub;
import org.conductor.router.service.websocket.model.managingdata.Sub;
import org.conductor.router.service.websocket.model.managingdata.Unsub;
import org.conductor.router.service.websocket.server.msg.pubsub.PubSub;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.ldap.Control;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Henrik on 16/03/2016.
 */
@Named
public class Publisher implements PubSub {

    @Inject
    private SetPropertyValueRequestPublication setPropertyValueRequestPublication;

    @Inject
    private SetPropertyValueResponsePublication setPropertyValueResponsePublication;

    @Inject
    private InstallComponentGroupRequestPublication installComponentGroupRequestPublication;

    @Inject
    private InstallComponentGroupResponsePublication installComponentGroupResponsePublication;

    @Inject
    private InstallDeviceRequestPublication installDeviceRequestPublication;

    @Inject
    private InstallDeviceResponsePublication installDeviceResponsePublication;

    @Inject
    private SubscriberAuthorization subscriberAuthorization;

    @Inject
    private AuthenticationKeyCollection authenticationKeyCollection;

    @Inject
    private DevicePublication devicePublication;

    @Inject
    private ApplicationInvitePublication applicationInvitePublication;

    @Inject
    private ApplicationPublication applicationPublication;

    @Inject
    private SetPropertyValueRequestSummaryPublication setPropertyValueRequestSummaryPublication;

    @Inject
    private ControlUnitPublication controlUnitPublication;

    @Inject
    private MeteorAutoUpdateClientVersions meteorAutoUpdateClientVersions;

    @Inject
    private PropertyChangedPublication propertyChangedPublication;

    private Map<PublicationName, IPublication> publications = new HashMap<>();

    @PostConstruct
    public void init() {
        addPublication(meteorAutoUpdateClientVersions);
        addPublication(setPropertyValueRequestPublication);
        addPublication(setPropertyValueResponsePublication);
        addPublication(installComponentGroupRequestPublication);
        addPublication(installComponentGroupResponsePublication);
        addPublication(installDeviceRequestPublication);
        addPublication(installDeviceResponsePublication);
        addPublication(devicePublication);
        addPublication(applicationInvitePublication);
        addPublication(applicationPublication);
        addPublication(setPropertyValueRequestSummaryPublication);
        addPublication(controlUnitPublication);
        addPublication(propertyChangedPublication);
    }

    private void addPublication(IPublication publication) {
        publications.put(publication.getPublicationName(), publication);
        subscriberAuthorization.addAuthorization(publication.getPublicationName(), publication.getAuthorizedKeyTypes());
    }

    @Override
    public void sub(Sub sub, DDPSession session) {
        try {
            if (sub.getName().equals(PublicationName.METEOR_AUTOUPDATE_CLIENTVERSIONS)) {
                return;
            }
            
            AuthenticationKey authenticationKey = getApiKey(sub);

            if (authenticationKey == null) {
                throw new UnauthorizedException("No private api key provided or api key is invalid.");
            }

            if (!subscriberAuthorization.isAuthorized(authenticationKey, sub)) {
                throw new UnauthorizedException("You must provide a valid private api key when subscribing to a publication.");
            }

            IPublication publication = publications.get(sub.getName());

            if (publication == null) {
                throw new IllegalArgumentException("Invalid subscription name, no subscription exist with the name '" + sub.getName() + "'");
            }

            publication.sub(authenticationKey, sub, session);
        } catch (Exception e) {
            Nosub nosub = new Nosub();
            nosub.setId(sub.getId());
            nosub.setError(new Error("Failed to subscribe to '" + sub.getName() + "'. " + e.getMessage()));
            session.sendMsg(nosub);
        }
    }

    @Override
    public void unsub(Unsub unsub, DDPSession session) {

    }

    private AuthenticationKey getApiKey(Sub sub) {
        if (sub.getParams() == null || sub.getParams().length == 0 || !(sub.getParams()[0].get("apiKey") instanceof String)) {
            return null;
        } else {
            String privateApiKey = (String) sub.getParams()[0].get("apiKey");
            return authenticationKeyCollection.findByPrivateKey(privateApiKey);
        }
    }

}
