package org.conductor.router.core.publisher.publications;

import org.conductor.router.core.entities.enums.KeyType;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henrik on 14/10/2016.
 */
@Named
public class MeteorAutoUpdateClientVersions extends Publication {

    @Override
    public PublicationName getPublicationName() {
        return PublicationName.METEOR_AUTOUPDATE_CLIENTVERSIONS;
    }

    @Override
    public List<KeyType> getAuthorizedKeyTypes() {
        return new ArrayList();
    }

}
