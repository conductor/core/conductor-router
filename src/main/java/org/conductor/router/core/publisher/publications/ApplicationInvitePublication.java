package org.conductor.router.core.publisher.publications;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.ApplicationInviteCollection;
import org.conductor.router.core.collection.CollectionListener;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.publisher.converters.MessageConverter;
import org.conductor.router.core.publisher.event.PublicationMessage;
import org.conductor.router.core.entities.ApplicationInvite;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * Created by Henrik on 31/08/2016.
 */
@Named
public class ApplicationInvitePublication extends Publication {

    @Inject
    public ApplicationInviteCollection applicationInviteCollection;

    @Inject
    private MessageConverter messageConverter;

    private Logger log = LogManager.getLogger(getClass().getName());

    @PostConstruct
    public void init() {
        applicationInviteCollection.addListener(applicationInviteListener);
    }

    @Override
    public PublicationName getPublicationName() {
        return PublicationName.APPLICATION_INVITES;
    }

    @Override
    public List<KeyType> getAuthorizedKeyTypes() {
        return Arrays.asList(KeyType.APPLICATION_KEY);
    }

    public String getCollection() {
        return "applicationInvites";
    }

    private void send(PublicationType publicationType, ApplicationInvite applicationInvite) {
        PublicationMessage message = null;
        try {
            message = createPublicationMessage(publicationType, applicationInvite);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        List<DDPSession> sessions = getSubscribers(applicationInvite);
        for (DDPSession session : sessions) {
            send(message, session);
        }
    }

    private void send(PublicationMessage message, DDPSession session) {
        try {
            session.sendMsg(message);
        } catch (Exception e) {
            log.warn("An error occurred while sending subscription event to subscriber.", e);
        }
    }

    private PublicationMessage createPublicationMessage(PublicationType publicationType, ApplicationInvite applicationInvite) throws IOException {
        PublicationMessage message = new PublicationMessage();
        message.setMsg(publicationType.toString());
        message.setCollection(getCollection());
        message.setId(applicationInvite.getId().toString());
        message.setFields(messageConverter.convert(applicationInvite, org.conductor.router.core.types.publications.ApplicationInvite.class));
        return message;
    }

    private List<DDPSession> getSubscribers(ApplicationInvite applicationInvite) {
        return super.getSubscribers(applicationInvite.getApplication().getAuthenticationKey());
    }

    private CollectionListener<ApplicationInvite> applicationInviteListener = new CollectionListener<ApplicationInvite>() {

        @Override
        public void onAdded(ApplicationInvite applicationInvite) {
            send(PublicationType.Add, applicationInvite);
        }

        @Override
        public void onUpdated(ApplicationInvite applicationInvite) {
            send(PublicationType.Changed, applicationInvite);
        }

        @Override
        public void onRemoved(ApplicationInvite applicationInvite) {
            send(PublicationType.Remove, applicationInvite);
        }

    };
}
