package org.conductor.router.core.publisher;

import org.conductor.router.core.exceptions.InvalidRequestException;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.publisher.publications.PublicationName;
import org.conductor.router.core.types.exceptions.UnauthorizedException;
import org.conductor.router.service.websocket.model.managingdata.Sub;

import javax.inject.Named;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by Henrik on 22/03/2016.
 */
@Named
public class SubscriberAuthorization {

    private Map<PublicationName, List<KeyType>> validation = new HashMap<>();

    public void addAuthorization(PublicationName publicationName, List<KeyType> keyTypes) {
        validation.put(publicationName, keyTypes);
    }

    public boolean isAuthorized(AuthenticationKey authenticationKey, Sub sub) throws UnauthorizedException, InvalidRequestException {
        List<KeyType> keyTypes = validation.get(sub.getName());

        if (!validation.containsKey(sub.getName())) {
            throw new InvalidRequestException("Invalid subscription name, no subscription exist with name '" + sub.getName() + "'.");
        }

        if (keyTypes == null) {
            return false;
        }

        return keyTypes.contains(authenticationKey.getKeyType());
    }

}
