package org.conductor.router.core.mockups;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.App;
import org.conductor.router.core.collection.ApplicationCollection;
import org.conductor.router.core.entities.Application;
import org.conductor.router.core.entities.ControlUnit;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henrik on 03/08/2016.
 */
@Named
public class ApplicationMockup {

    private Logger log = LogManager.getLogger(this.getClass().getName());

    @Inject
    private ApplicationCollection applicationCollection;

    public void addApplications() throws IOException {
        String content = FileUtils.readFileToString(new File("./mock/applications.txt"));
        log.info(content);

        Type listType = new TypeToken<ArrayList<Application>>() {
        }.getType();

        Gson gson = new Gson();
        List<Application> applications = gson.fromJson(content, listType);

        for (Application application : applications) {
            if (applicationCollection.findById(application.getId()) == null) {
                applicationCollection.add(application);
            }
        }
    }

}
