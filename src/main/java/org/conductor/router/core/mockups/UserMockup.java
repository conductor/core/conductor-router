package org.conductor.router.core.mockups;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.UserCollection;
import org.conductor.router.core.entities.User;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

@Named
public class UserMockup {

    private Logger log = LogManager.getLogger(this.getClass().getName());

    @Inject
    private UserCollection userCollection;

    public void addUsers() throws IOException {
        String content = FileUtils.readFileToString(new File("./mock/users.txt"));
        log.info(content);

        Type listType = new TypeToken<ArrayList<User>>() {
        }.getType();

        Gson gson = new Gson();
        List<User> users = gson.fromJson(content, listType);

        for (User user : users) {
            user.getAuthenticationKey().setUser(user);

            if (userCollection.findById(user.getId()) == null) {
                userCollection.add(user);
            }
        }
    }
}
