package org.conductor.router.core.mockups;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.ControlUnitCollection;
import org.conductor.router.core.collection.UserCollection;
import org.conductor.router.core.entities.*;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

@Named
public class ControlUnitMockup {

    private Logger log = LogManager.getLogger(this.getClass().getName());

    @Inject
    private ControlUnitCollection controlUnitCollection;

    @Inject
    private UserCollection userCollection;

    public void addControlUnits() throws IOException {
        String content = FileUtils.readFileToString(new File("./mock/control_units.txt"));
        log.info(content);
        Type listType = new TypeToken<ArrayList<ControlUnit>>() {
        }.getType();

        Gson gson = new Gson();
        List<ControlUnit> controlUnits = gson.fromJson(content, listType);

        User queryOwner = new User();
        queryOwner.setEmail("testing@stuff.com");
        final User owner = userCollection.findFirst(queryOwner);

        for (ControlUnit controlUnit : controlUnits) {
            for (Device device : controlUnit.getDevices()) {
                device.setControlUnit(controlUnit);

                DeviceAuthorization deviceAuthorization = new DeviceAuthorization();
                deviceAuthorization.setId(UUID.randomUUID().toString());
                deviceAuthorization.setAccessGivenTo(owner.getAuthenticationKey());
                deviceAuthorization.setDevice(device);
                device.getDeviceAuthorizations().add(deviceAuthorization);

                for (ComponentGroup componentGroup : device.getComponentGroups()) {
                    componentGroup.setDevice(device);
                    for (Component component : componentGroup.getComponents()) {
                        component.setComponentGroup(componentGroup);

                        for (Property property : component.getProperties()) {
                            property.setComponent(component);
                        }
                    }
                }
            }

            controlUnit.setOwner(owner);

            if (controlUnitCollection.findById(controlUnit.getId()) == null) {
                controlUnitCollection.add(controlUnit);
            }
        }
    }
}
