package org.conductor.router.core.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.annotations.Authorize;
import org.conductor.router.core.collection.*;
import org.conductor.router.core.commands.*;
import org.conductor.router.core.dispatcher.RequestDispatcher;
import org.conductor.router.core.dispatcher.ResponseDispatcher;
import org.conductor.router.core.entities.*;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.exceptions.InvalidRequestException;
import org.conductor.router.core.factories.AuthenticationKeyFactory;
import org.conductor.router.core.publisher.converters.MessageConverter;
import org.conductor.router.core.publisher.converters.response.ResponseConverter;
import org.conductor.router.core.factories.RequestFactory;
import org.conductor.router.core.types.exceptions.BadRequestException;
import org.conductor.router.core.types.exceptions.UnauthorizedException;
import org.conductor.router.core.types.request.controlunit.IRequest;
import org.conductor.router.core.types.response.controlunit.IResponse;
import org.conductor.router.core.types.response.controlunit.Response;
import org.conductor.router.core.types.response.controlunit.ResponseStatus;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.UUID;

@Named
public class InstallerController {

    @Inject
    private RequestDispatcher requestDispatcher;

    @Inject
    private RequestFactory requestFactory;

    @Inject
    private ResponseConverter responseConverter;

    @Inject
    private ResponseDispatcher responseDispatcher;

    @Inject
    private DeviceCollection deviceCollection;

    @Inject
    private ControlUnitCollection controlUnitCollection;

    @Inject
    private AuthenticationKeyCollection authenticationKeyCollection;

    @Inject
    private UserCollection userCollection;

    @Inject
    private ComponentGroupCollection componentGroupCollection;

    @Inject
    private ComponentCollection componentCollection;

    @Inject
    private PropertyCollection propertyCollection;

    @Inject
    private MessageConverter messageConverter;

    @Inject
    private DeviceAuthorizationCollection deviceAuthorizationCollection;

    private Logger log = LogManager.getLogger(getClass().getName());

    @Authorize(requiredKey = KeyType.USER_KEY)
    public Object installControlUnit(String privateKey, InstallControlUnitCommand command) throws UnauthorizedException, IOException {
        log.info("Install control unit request, private key: {}, installation {}.", privateKey, command);
        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);
        command.setRequester(authenticationKey);

        User userQuery = new User();
        userQuery.setAuthenticationKey(authenticationKey);
        User user = userCollection.findFirst(userQuery);

        if (user == null) {
            throw new UnauthorizedException("No user was found that was associated with the specified private key (" + privateKey + ").");
        }

        /*
        IRequest request = requestFactory.createRequest(command);
        IResponse response = requestDispatcher.send(request);

        if (response.getStatus() == ResponseStatus.WAITING) {
            try {
                ControlUnit controlUnit = new ControlUnit();
                controlUnit.setId(UUID.randomUUID().toString());
                controlUnit.setOwner(user);
                controlUnit.setName(command.getControlUnitName());
                controlUnit.setAuthenticationKey(AuthenticationKeyFactory.create(KeyType.CONTROL_UNIT_KEY));
                controlUnitCollection.add(controlUnit);
                response.setStatus(ResponseStatus.SUCCESS);
            } catch (Throwable e) {
                response.setStatus(ResponseStatus.ERROR);
                response.setErrorMessage(e.getMessage());
                log.error("Failed to install control unit. ", e);
            }
        }

        responseDispatcher.send(response);
        */

        ControlUnit controlUnit = new ControlUnit();
        controlUnit.setId(UUID.randomUUID().toString());
        controlUnit.setOwner(user);
        controlUnit.setName(command.getControlUnitName());
        controlUnit.setAuthenticationKey(AuthenticationKeyFactory.create(KeyType.CONTROL_UNIT_KEY));
        controlUnitCollection.add(controlUnit);

        return messageConverter.convert(controlUnit, org.conductor.router.core.types.publications.ControlUnit.class);
    }

    @Authorize(requiredKey = KeyType.USER_KEY)
    public void uninstallControlUnit(String privateKey, UninstallControlUnitCommand command) throws UnauthorizedException, BadRequestException {
        log.info("Uninstall control unit request, private key: {}, control unit key: {}.", privateKey, command.getControlUnitId());
        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);

        User userQuery = new User();
        userQuery.setAuthenticationKey(authenticationKey);
        User user = userCollection.findFirst(userQuery);

        if (user == null) {
            throw new UnauthorizedException("No user was found that was associated with the specified private key (" + privateKey + ").");
        }

        ControlUnit controlUnit = controlUnitCollection.findById(command.getControlUnitId());

        if (controlUnit == null) {
            throw new BadRequestException("No control unit found with the id '" + command.getControlUnitId() + "'.");
        }

        controlUnit.getDevices().forEach(device -> {
            device.getComponentGroups().forEach(componentGroup -> {

                componentGroup.getComponents().forEach(component -> {
                    component.getProperties().forEach(property -> {
                        propertyCollection.remove(property);
                    });
                    componentCollection.remove(component);
                });
                componentGroupCollection.remove(componentGroup);
            });

            device.getDeviceAuthorizations().forEach(deviceAuthorizationCollection::remove);

            deviceCollection.remove(device);
        });

        controlUnitCollection.remove(controlUnit);

    }

    @Authorize(requiredKey = KeyType.USER_KEY)
    public void uninstallDevice(String privateKey, UninstallDeviceCommand command) throws UnauthorizedException, BadRequestException {
        log.info("Uninstall device request, private key: {}, device key: {}.", privateKey, command.getDeviceId());
        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);

        User userQuery = new User();
        userQuery.setAuthenticationKey(authenticationKey);
        User user = userCollection.findFirst(userQuery);

        if (user == null) {
            throw new UnauthorizedException("No user was found that was associated with the specified private key (" + privateKey + ").");
        }

        Device device = deviceCollection.findById(command.getDeviceId());

        if (device == null) {
            throw new BadRequestException("No device found with the id '" + command.getDeviceId() + "'.");
        }

        device.getComponentGroups().forEach(componentGroup -> {
            componentGroup.getComponents().forEach(component -> {
                component.getProperties().forEach(propertyCollection::remove);
                componentCollection.remove(component);
            });
            componentGroupCollection.remove(componentGroup);
        });

        device.getDeviceAuthorizations().forEach(deviceAuthorizationCollection::remove);

        deviceCollection.remove(device);
    }

    @Authorize(requiredKey = KeyType.USER_KEY)
    public void uninstallComponent(String privateKey, UninstallComponentCommand command) throws UnauthorizedException, BadRequestException {
        log.info("Uninstall component request, private key: {}, component key: {}.", privateKey, command.getComponentId());
        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);

        User userQuery = new User();
        userQuery.setAuthenticationKey(authenticationKey);
        User user = userCollection.findFirst(userQuery);

        if (user == null) {
            throw new UnauthorizedException("No user was found that was associated with the specified private key (" + privateKey + ").");
        }

        Component component = componentCollection.findById(command.getComponentId());

        if (component == null) {
            throw new BadRequestException("No component found with the id '" + command.getComponentId() + "'.");
        }

        ComponentGroup componentGroup = component.getComponentGroup();
        componentGroup.getComponents().forEach(component1 -> {
            component1.getProperties().forEach(propertyCollection::remove);
            componentCollection.remove(component1);
        });
        componentGroupCollection.remove(componentGroup);
    }

    @Authorize(requiredKey = KeyType.USER_KEY)
    public org.conductor.router.core.types.response.application.Response installDevice(String privateKey, InstallDeviceCommand command) throws UnauthorizedException, InvalidRequestException, IOException {
        log.info("Install device request, private key: {}, installation {}.", privateKey, command);
        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);
        command.setRequester(authenticationKey);

        ControlUnit controlUnit = controlUnitCollection.findById(command.getControlUnitId());
        command.setControlUnit(controlUnit);

        if (controlUnit == null) {
            throw new InvalidRequestException("No control unit exist with the id '" + command.getControlUnitId() + "'");
        }

        if (!controlUnit.getOwner().getAuthenticationKey().equals(authenticationKey)) {
            throw new UnauthorizedException("Only the owner of the control unit can install component groups.");
        }

        IRequest request = requestFactory.createRequest(command);
        IResponse response = requestDispatcher.send(request);

        if (response.getStatus() == ResponseStatus.WAITING) {
            try {
                Device device = new Device();
                device.setId(UUID.randomUUID().toString());
                device.setName(command.getDeviceName());
                device.setType(command.getDeviceType());
                device.setControlUnit(command.getControlUnit());

                DeviceAuthorization deviceAuthorization = new DeviceAuthorization();
                deviceAuthorization.setId(UUID.randomUUID().toString());
                deviceAuthorization.setDevice(device);
                deviceAuthorization.setAccessGivenTo(authenticationKey);
                device.getDeviceAuthorizations().add(deviceAuthorization);

                deviceCollection.add(device);
                response.setStatus(ResponseStatus.SUCCESS);
            } catch (Throwable e) {
                response.setStatus(ResponseStatus.ERROR);
                response.setErrorMessage(e.getMessage());
                log.error("Failed to install device. ", e);
            }
        }

        responseDispatcher.send(response);
        return responseConverter.convert((Response) response);
    }

    @Authorize(requiredKey = KeyType.USER_KEY)
    public org.conductor.router.core.types.response.application.Response installComponentGroup(String privateKey, InstallComponentGroupCommand command) throws UnauthorizedException, IOException, InvalidRequestException {
        log.info("Install component group request, private key: {}, installation {}.", privateKey, command);
        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);
        command.setRequester(authenticationKey);

        Device device = deviceCollection.findById(command.getDeviceId());

        if (device == null) {
            throw new InvalidRequestException("No device exist with the id '" + command.getDeviceId() + "'");
        }

        User userQuery = new User();
        userQuery.setAuthenticationKey(authenticationKey);
        User user = userCollection.findFirst(userQuery);

        if (!device.getControlUnit().getOwner().equals(user)) {
            throw new UnauthorizedException("Only the owner of the control unit can install component groups.");
        }

        ComponentGroup componentGroup = command.getComponentGroup();
        componentGroup.setId(UUID.randomUUID().toString());
        componentGroup.setDevice(device);
        for (Component component : componentGroup.getComponents()) {
            component.setId(UUID.randomUUID().toString());
            for (Property property : component.getProperties()) {
                property.setId(UUID.randomUUID().toString());
                property.setComponent(component);
            }
            for (Option option : component.getOptions()) {
                option.setId(UUID.randomUUID().toString());
                option.setComponent(component);
            }

            component.setComponentGroup(componentGroup);
        }
        device.getComponentGroups().add(componentGroup);

        componentGroupCollection.add(componentGroup);
        deviceCollection.update(device);

        IRequest request = requestFactory.createRequest(command);

        IResponse response = requestDispatcher.send(request);
        responseDispatcher.send(response);

        return responseConverter.convert((Response) response);
    }

}
