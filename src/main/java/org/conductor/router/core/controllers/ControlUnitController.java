package org.conductor.router.core.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.annotations.Authorize;
import org.conductor.router.core.collection.*;
import org.conductor.router.core.entities.*;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.exceptions.InvalidRequestException;
import org.conductor.router.core.types.exceptions.BadRequestException;
import org.conductor.router.core.types.exceptions.UnauthorizedException;
import org.conductor.router.core.types.request.controlunit.Request;
import org.conductor.router.core.types.response.controlunit.Response;
import org.conductor.router.core.types.response.controlunit.ResponseStatus;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class ControlUnitController {

    @Inject
    private AuthenticationKeyCollection authenticationKeyCollection;

    @Inject
    private SetPropertyValueRequestCollection setPropertyValueRequestCollection;

    @Inject
    private SetPropertyValueRequestSummaryCollection setPropertyValueRequestSummaryCollection;

    @Inject
    private SetPropertyValueResponseCollection setPropertyValueResponseCollection;

    @Inject
    private InstallComponentGroupRequestCollection installComponentGroupRequestCollection;

    @Inject
    private InstallComponentGroupResponseCollection installComponentGroupResponseCollection;

    @Inject
    private ControlUnitCollection controlUnitCollection;

    @Inject
    private PropertyCollection propertyCollection;

    private Logger log = LogManager.getLogger(getClass().getName());

    public void okResponse(String privateKey, String requestId) throws UnauthorizedException, InvalidRequestException {
        log.info("Received ok response from control unit with private key '{}' for request with id '{}'.", privateKey, requestId);
        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);

        if (authenticationKey == null) {
            throw new UnauthorizedException("Invalid private key.");
        }

        // Validate private key, so only the controlunit that received the request has permission to update the response status
        Request request = findRequest(requestId);

        if (request == null) {
            throw new InvalidRequestException("No request exists with the supplied request id.");
        }

        if (!request.getControlUnit().getAuthenticationKey().equals(authenticationKey)) {
            throw new UnauthorizedException("Only the control unit that received the request can update its status.");
        }

        if (request instanceof SetPropertyValueRequest) {
            SetPropertyValueRequest req = (SetPropertyValueRequest) request;
            req.setStatus(ResponseStatus.SUCCESS);
            setPropertyValueRequestCollection.update(req);

            SetPropertyValueRequestSummary summary = setPropertyValueRequestSummaryCollection.findById(req.getRequestSummary().getId());

            if (summary.getStatus().equals(ResponseStatus.WAITING)) {
                boolean allSuccess = summary.getSetPropertyValueRequests().stream().allMatch(setPropertyValueRequest -> setPropertyValueRequest.getStatus().equals(ResponseStatus.SUCCESS));
                if (allSuccess) {
                    summary.setStatus(ResponseStatus.SUCCESS);
                }
            }

            setPropertyValueRequestSummaryCollection.update(summary);

            return;
        }

        Response response = findResponse(requestId);

        if (response == null) {
            throw new InvalidRequestException("Couldn't find a response object for request id '" + response + "'.");
        }

        response.setStatus(ResponseStatus.SUCCESS);

        if (response instanceof SetPropertyValueResponse) {
            setPropertyValueResponseCollection.update((SetPropertyValueResponse) response);
        } else if (response instanceof InstallComponentGroupResponse) {
            installComponentGroupResponseCollection.update((InstallComponentGroupResponse) response);
        } else {
            throw new InvalidRequestException("Can't update response object because of unknown response object.");
        }
    }

    public void errorResponse(String privateKey, String requestId, String errorMessage, String stackTrace) throws UnauthorizedException, InvalidRequestException {
        log.info("Received error response from control unit with private key '{}' for request with id '{}', error message was '{}' and stack trace '{}'.", privateKey, requestId, errorMessage, stackTrace);
        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);

        if (authenticationKey == null) {
            throw new UnauthorizedException("Invalid private key.");
        }

        // Validate private key, so only the controlunit that received the request has permission to update the response status
        Request request = findRequest(requestId);

        if (request == null) {
            throw new InvalidRequestException("No request exists with the supplied request id.");
        }

        if (!request.getControlUnit().getAuthenticationKey().equals(authenticationKey)) {
            throw new UnauthorizedException("Only the control unit that received the request can update its status.");
        }

        Response response = findResponse(requestId);

        if (response == null) {
            throw new InvalidRequestException("Couldn't find a response object for request id '" + response + "'.");
        }

        response.setStatus(ResponseStatus.ERROR);
        response.setErrorMessage(errorMessage);

        if (response instanceof SetPropertyValueResponse) {
            setPropertyValueResponseCollection.update((SetPropertyValueResponse) response);
        } else if (response instanceof InstallComponentGroupResponse) {
            installComponentGroupResponseCollection.update((InstallComponentGroupResponse) response);
        } else {
            throw new InvalidRequestException("Can't update response object because of unknown response object.");
        }
    }

    private Response findResponse(String requestId) {
        Response response;

        response = setPropertyValueResponseCollection.findByRequestId(requestId);

        if (response != null) {
            return response;
        }

        response = installComponentGroupResponseCollection.findByRequestId(requestId);

        if (response != null) {
            return response;
        }

        return null;
    }

    private Request findRequest(String requestId) {
        Request request;

        request = setPropertyValueRequestCollection.findById(requestId);

        if (request != null) {
            return request;
        }

        request = installComponentGroupRequestCollection.findById(requestId);

        if (request != null) {
            return request;
        }

        return null;
    }

    public void errorReport(String privateKey, String componentGroupId, String componentId, String errorMessage, String stackTrace) {
        log.info("Received error report from control unit with private key '{}', component group id '{}', component id '{}' with error message '{}' and stack trace '{}'.", privateKey, componentGroupId, componentId, errorMessage, stackTrace);
    }

    public void updatePropertyValue(String privateKey, String componentId, String propertyName, Object propertyValue) throws UnauthorizedException, BadRequestException {
        log.info("Updated property value received from control unit with private key '{}', componentId '{}', property name '{}', proeprty value '{]'.", privateKey, componentId, propertyName, propertyValue);
        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);

        if (authenticationKey == null) {
            throw new UnauthorizedException("Invalid private key.");
        }

        Component componentQuery = new Component();
        componentQuery.setId(componentId);

        Property propertyQuery = new Property();
        propertyQuery.setName(propertyName);
        propertyQuery.setComponent(componentQuery);

        Property property = propertyCollection.findFirst(propertyQuery);

        if (property == null) {
            throw new BadRequestException("Couldn't find a property with the name '" + propertyName + "' in component '" + componentId + "'.");
        }

        property.setValue(propertyValue.toString());

        propertyCollection.update(property);
    }

    @Authorize(requiredKey = KeyType.CONTROL_UNIT_KEY)
    public void heartbeat(String privateKey) throws UnauthorizedException, BadRequestException {
        log.info("Received heartbeat from control unit with private key '{}'.", privateKey);
        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);

        ControlUnit query = new ControlUnit();
        query.setAuthenticationKey(authenticationKey);
        query.setDevices(null);
        ControlUnit controlUnit = controlUnitCollection.findFirst(query);

        if (controlUnit == null) {
            throw new BadRequestException("Found no control unit for the private key: " + privateKey);
        }

        if (!controlUnit.isOnline()) {
            controlUnit.setOnline(true);
            controlUnitCollection.update(controlUnit);
        }
    }

}
