package org.conductor.router.core.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.annotations.Authorize;
import org.conductor.router.core.collection.AuthenticationKeyCollection;
import org.conductor.router.core.collection.SetPropertyValueRequestSummaryCollection;
import org.conductor.router.core.commands.SetPropertyValueCommand;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.SetPropertyValueRequestSummary;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.factories.RequestFactory;
import org.conductor.router.core.publisher.converters.MessageConverter;
import org.conductor.router.core.types.exceptions.UnauthorizedException;

import javax.inject.Inject;
import javax.inject.Named;

@Named
public class CommandController {

    @Inject
    private AuthenticationKeyCollection authenticationKeyCollection;

    @Inject
    private RequestFactory requestFactory;

    @Inject
    private MessageConverter messageConverter = null;

    @Inject
    private SetPropertyValueRequestSummaryCollection setPropertyValueRequestSummaryCollection;

    private Logger log = LogManager.getLogger(getClass().getName());

    @Authorize(requiredKey = KeyType.APPLICATION_KEY)
    public Object setPropertyValue(String privateKey, SetPropertyValueCommand command) throws Exception {
        log.info("Set property value command received from private key '{}' with command '{}'", privateKey, command);
        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);

        command.setRequester(authenticationKey);
        command.getFilter().getPublicKeys().add(authenticationKey.getPublicKey());

        SetPropertyValueRequestSummary request = (SetPropertyValueRequestSummary) requestFactory.createRequest(command);
        setPropertyValueRequestSummaryCollection.add(request);

        return messageConverter.convert(request, org.conductor.router.core.types.publications.SetPropertyValueRequestSummary.class);
    }

//    public Object callMethod(String privateKey, CallMethodCommand command) throws UnauthorizedException {
//        log.info("Call method command received from private key '{}' with command '{}'", privateKey, command);
//        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);
//
//        if (authenticationKey == null) {
//            log.info("Invalid authentication key, user must supply an authentication key in the request.");
//            throw new UnauthorizedException("Invalid authentication key.");
//        }
//
//        if (!authenticationKey.getKeyType().equals(KeyType.APPLICATION_KEY)) {
//            log.info("Invalid authentication key. Authentication key must be an application key");
//            throw new UnauthorizedException("Invalid authentication key. Authentication key must be an application key.");
//        }
//
//    }

}
