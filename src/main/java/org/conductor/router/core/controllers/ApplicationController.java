package org.conductor.router.core.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.annotations.Authorize;
import org.conductor.router.core.collection.ApplicationCollection;
import org.conductor.router.core.collection.ApplicationInviteCollection;
import org.conductor.router.core.collection.AuthenticationKeyCollection;
import org.conductor.router.core.collection.UserCollection;
import org.conductor.router.core.config.Config;
import org.conductor.router.core.entities.Application;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.User;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.entities.ApplicationInvite;
import org.conductor.router.core.entities.enums.ApplicationInviteStatus;
import org.conductor.router.core.publisher.converters.MessageConverter;
import org.conductor.router.core.types.exceptions.UnauthorizedException;
import org.conductor.router.core.types.publications.CreatedApplication;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by Henrik on 15/03/2016.
 */
@Named
public class ApplicationController {

    private Logger log = LogManager.getLogger(getClass().getName());

    @Inject
    private AuthenticationKeyCollection authenticationKeyCollection;

    @Inject
    private ApplicationInviteCollection applicationInviteCollection;

    @Inject
    private ApplicationCollection applicationCollection;

    @Inject
    private UserCollection userCollection;

    @Inject
    private MessageConverter messageConverter;

    @Inject
    private Config config;

    @Authorize(requiredKey = KeyType.USER_KEY)
    public Object createApplication(String privateKey, String applicationName) throws UnauthorizedException, IOException {
        log.info("Received request to create a new application with name '{}' for user with private key '{}'.", applicationName, privateKey);

        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);

        User userQuery = new User();
        userQuery.setAuthenticationKey(authenticationKey);
        User user = userCollection.findFirst(userQuery);

        if (user == null) {
            throw new UnauthorizedException("No user was found with the specified private key.");
        }

        Application application = new Application();
        application.setName(applicationName);
        application.setId(UUID.randomUUID().toString());
        application.setOwner(user);

        AuthenticationKey applicationKey = new AuthenticationKey();
        applicationKey.setKeyType(KeyType.APPLICATION_KEY);
        applicationKey.setId(UUID.randomUUID().toString());
        applicationKey.setPublicKey(UUID.randomUUID().toString());
        applicationKey.setPrivateKey(UUID.randomUUID().toString());
        applicationKey.setApplication(application);

        application.setAuthenticationKey(applicationKey);

        applicationCollection.add(application);

        return messageConverter.convert(application, CreatedApplication.class);
    }

    @Authorize(requiredKey = KeyType.APPLICATION_KEY)
    public Object createApplicationInvite(String privateKey, String externalInviteId) throws UnauthorizedException, IOException {
        log.info("Received request to create an application invite for application with private key '{}' and external invite id '{}'.", privateKey, externalInviteId);
        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);

        Application applicationQuery = new Application();
        applicationQuery.setAuthenticationKey(authenticationKey);
        Application application = applicationCollection.findFirst(applicationQuery);

        ApplicationInvite invite = new ApplicationInvite();
        String id = UUID.randomUUID().toString();
        invite.setId(id);
        invite.setApplication(application);
        invite.setExternalId(externalInviteId);
        invite.setApplicationInviteStatus(ApplicationInviteStatus.WAITING);
        invite.setInviteUrl(config.getProperty(Config.Properties.ADMINISTRATION_BASE_URL) + "/invite/" + id);

        applicationInviteCollection.add(invite);
        return messageConverter.convert(invite, org.conductor.router.core.types.publications.ApplicationInvite.class);
    }

}
