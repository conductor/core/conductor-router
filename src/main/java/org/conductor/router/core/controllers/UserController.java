package org.conductor.router.core.controllers;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.annotations.Authorize;
import org.conductor.router.core.collection.*;
import org.conductor.router.core.commands.CreateUserCommand;
import org.conductor.router.core.entities.*;
import org.conductor.router.core.entities.Device;
import org.conductor.router.core.entities.DeviceAuthorization;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.factories.UserFactory;
import org.conductor.router.core.entities.ApplicationInvite;
import org.conductor.router.core.entities.enums.ApplicationInviteStatus;
import org.conductor.router.core.publisher.converters.MessageConverter;
import org.conductor.router.core.types.exceptions.BadRequestException;
import org.conductor.router.core.types.exceptions.UnauthorizedException;

import javax.inject.Inject;
import javax.inject.Named;
import java.io.IOException;
import java.util.UUID;

/**
 * Created by Henrik on 31/08/2016.
 */
@Named
public class UserController {

    private Logger log = LogManager.getLogger(getClass().getName());

    @Inject
    private ApplicationInviteCollection applicationInviteCollection;

    @Inject
    private AuthenticationKeyCollection authenticationKeyCollection;

    @Inject
    private DeviceCollection deviceCollection;

    @Inject
    private DeviceAuthorizationCollection deviceAuthorizationCollection;

    @Inject
    private UserCollection userCollection;

    @Inject
    private MessageConverter messageConverter;

    public User register(CreateUserCommand createUserCommand) throws BadRequestException {
        log.info("Received request to create a new user '{}'.", createUserCommand);

        if (createUserCommand.getEmail() == null || createUserCommand.getPassword() == null) {
            throw new BadRequestException("You must provide an email and passoword to register.");
        }

        User user = UserFactory.createUser(createUserCommand.getEmail(), createUserCommand.getPassword());
        userCollection.add(user);

        return user;
    }

    public User login(String email, String password) throws UnauthorizedException {
        log.info("Received a login request from user with email '{}'.", email);

        if (email == null || email.isEmpty() || password == null || password.isEmpty()) {
            throw new UnauthorizedException("You must supply an email and password.");
        }

        User userQuery = new User();
        userQuery.setEmail(email);
        userQuery.setPassword(password);
        User user = userCollection.findFirst(userQuery);

        if (user == null) {
            throw new UnauthorizedException("Invalid email and/or password.");
        }

        return user;
    }

    @Authorize(requiredKey = KeyType.USER_KEY)
    public Object getApplicationInvite(String privateKey, String applicationInviteId) throws BadRequestException, UnauthorizedException, IOException {
        log.info("Received request to get application invite '{}' for user with private key '{}'.", applicationInviteId, privateKey);

        ApplicationInvite applicationInvite = applicationInviteCollection.findById(applicationInviteId);

        if (applicationInvite == null) {
            log.info("Couldn't find application invite with the id {}.", applicationInviteId);
            throw new BadRequestException("Couldn't find application invite with the id " + applicationInviteId + ".");
        }

        return messageConverter.convert(applicationInvite, org.conductor.router.core.types.publications.ApplicationInvite.class);
    }

    @Authorize(requiredKey = KeyType.USER_KEY)
    public Object acceptApplicationInvite(String privateKey, String applicationInviteId) throws UnauthorizedException, BadRequestException, IOException {
        log.info("Received request to accept application invite '{}' for user with private key '{}'.", applicationInviteId, privateKey);

        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);
        ApplicationInvite applicationInvite = applicationInviteCollection.findById(applicationInviteId);

        if (applicationInvite == null) {
            log.info("Couldn't find application invite with the id {}.", applicationInviteId);
            throw new BadRequestException("Couldn't find application invite with the id " + applicationInviteId + ".");
        }

        User user = userCollection.findById(authenticationKey.getUser().getId());
        applicationInvite.setUser(user);
        applicationInvite.setApplicationInviteStatus(ApplicationInviteStatus.ACCEPTED);
        applicationInviteCollection.update(applicationInvite);

        return messageConverter.convert(applicationInvite, org.conductor.router.core.types.publications.ApplicationInvite.class);
    }

    @Authorize(requiredKey = KeyType.USER_KEY)
    public Object applicationInviteCompleted(String privateKey, String applicationInviteId) throws UnauthorizedException, BadRequestException, IOException {
        log.info("Received request to complete application invite '{}' for user with private key '{}'.", applicationInviteId, privateKey);

        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);
        ApplicationInvite applicationInvite = applicationInviteCollection.findById(applicationInviteId);

        if (applicationInvite == null) {
            log.info("Couldn't find application invite with the id {}.", applicationInviteId);
            throw new BadRequestException("Couldn't find application invite with the id " + applicationInviteId + ".");
        }

        User user = userCollection.findById(authenticationKey.getUser().getId());
        applicationInvite.setUser(user);
        applicationInvite.setApplicationInviteStatus(ApplicationInviteStatus.COMPLETED);
        applicationInviteCollection.update(applicationInvite);

        return messageConverter.convert(applicationInvite, org.conductor.router.core.types.publications.ApplicationInvite.class);
    }

    @Authorize(requiredKey = KeyType.USER_KEY)
    public Object giveDeviceAccess(String privateKey, String giveAccessToPublicKey, String deviceId) throws UnauthorizedException, BadRequestException, IOException {
        log.info("Received request from private key '{}' to give public key '{}' access to device '{}'.", privateKey, giveAccessToPublicKey, deviceId);

        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);
        AuthenticationKey authenticationKeyOther = authenticationKeyCollection.findByPublicKey(giveAccessToPublicKey);

        if (authenticationKeyOther == null) {
            throw new BadRequestException("Unable to give device access to public key '" + giveAccessToPublicKey + "', no key found.");
        }

        Device device = deviceCollection.findById(deviceId);

        if (device == null) {
            throw new BadRequestException("No device found with id '" + deviceId + "'.");
        }

        DeviceAuthorization deviceAuthorization = new DeviceAuthorization();
        deviceAuthorization.setId(UUID.randomUUID().toString());
        deviceAuthorization.setAccessGivenTo(authenticationKeyOther);
        deviceAuthorization.setAccessGivenBy(authenticationKey);
        deviceAuthorization.setDevice(device);
        deviceAuthorizationCollection.add(deviceAuthorization);

        device.getDeviceAuthorizations().add(deviceAuthorization);
        deviceCollection.update(device);

        authenticationKeyOther.getDeviceAuthorizations().add(deviceAuthorization);
        authenticationKeyCollection.update(authenticationKeyOther);

        return messageConverter.convert(deviceAuthorization, org.conductor.router.core.types.publications.DeviceAuthorization.class);
    }

    @Authorize(requiredKey = KeyType.USER_KEY)
    public void revokeDeviceAccess(String privateKey, String revokeAccessToPublicKey, String deviceId) throws UnauthorizedException, BadRequestException {
        log.info("Received request from private key '{}' to revoke public key '{}' access to device '{}'.", privateKey, revokeAccessToPublicKey, deviceId);

        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);
        AuthenticationKey authenticationKeyOther = authenticationKeyCollection.findByPublicKey(revokeAccessToPublicKey);

        if (authenticationKeyOther == null) {
            throw new BadRequestException("Unable to revoke device access to public key '" + revokeAccessToPublicKey + "', no key found.");
        }

        Device device = deviceCollection.findById(deviceId);

        if (device == null) {
            throw new BadRequestException("No device found with id '" + deviceId + "'.");
        }

        DeviceAuthorization deviceAuthorizationQuery = new DeviceAuthorization();
        deviceAuthorizationQuery.setAccessGivenBy(authenticationKey);
        deviceAuthorizationQuery.setAccessGivenTo(authenticationKeyOther);
        deviceAuthorizationQuery.setDevice(device);
        DeviceAuthorization deviceAuthorization = deviceAuthorizationCollection.findFirst(deviceAuthorizationQuery);

        if (deviceAuthorization == null) {
            throw new BadRequestException("No device access found.");
        }

        deviceAuthorizationCollection.remove(deviceAuthorization);

        device.getDeviceAuthorizations().remove(deviceAuthorization);
        deviceCollection.update(device);
    }
}
