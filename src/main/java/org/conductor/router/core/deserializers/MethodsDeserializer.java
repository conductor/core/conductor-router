package org.conductor.router.core.deserializers;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.Converter;
import org.conductor.router.core.entities.Method;
import org.conductor.router.core.entities.Property;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by Henrik on 27/11/2016.
 */
public class MethodsDeserializer implements Converter<LinkedHashMap<String, LinkedHashMap<String, Object>>, Collection<Method>> {

    final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Collection<Method> convert(LinkedHashMap<String, LinkedHashMap<String, Object>> map) {
        List<Method> methods = new ArrayList();

        map.forEach((key, value) -> {
            Method method = mapper.convertValue(value, Method.class);
            method.setName(key);
            methods.add(method);
        });

        return methods;
    }

    @Override
    public JavaType getInputType(TypeFactory typeFactory) {
        return typeFactory.constructMapType(LinkedHashMap.class, String.class, LinkedHashMap.class);
    }

    @Override
    public JavaType getOutputType(TypeFactory typeFactory) {
        return typeFactory.constructCollectionType(Collection.class, Method.class);
    }
}
