package org.conductor.router.core.deserializers;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.fasterxml.jackson.databind.util.Converter;
import org.conductor.router.core.entities.Property;

import java.util.*;

/**
 * Created by Henrik on 26/11/2016.
 */
public class PropertiesDeserializer implements Converter<LinkedHashMap<String, LinkedHashMap<String, Object>>, Collection<Property>> {

    final ObjectMapper mapper = new ObjectMapper();

    @Override
    public Collection<Property> convert(LinkedHashMap<String, LinkedHashMap<String, Object>> map) {
        List<Property> properties = new ArrayList();

        map.forEach((key, value) -> {
            Property property = mapper.convertValue(value, Property.class);
            property.setName(key);
            properties.add(property);
        });

        return properties;
    }

    @Override
    public JavaType getInputType(TypeFactory typeFactory) {
        return typeFactory.constructMapType(LinkedHashMap.class, String.class, LinkedHashMap.class);
    }

    @Override
    public JavaType getOutputType(TypeFactory typeFactory) {
        return typeFactory.constructCollectionType(Collection.class, Property.class);
    }
}