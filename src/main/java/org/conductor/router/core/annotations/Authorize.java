package org.conductor.router.core.annotations;

import org.conductor.router.core.entities.enums.KeyType;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

/**
 * Created by Henrik on 01/07/2017.
 */
@Retention(RetentionPolicy.RUNTIME)
public @interface Authorize {
    KeyType requiredKey();
}
