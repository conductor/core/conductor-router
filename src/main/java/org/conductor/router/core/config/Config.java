package org.conductor.router.core.config;

import javax.inject.Named;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * Created by Henrik on 10/09/2017.
 */
@Named
public class Config extends java.util.Properties {

    public enum Properties {
        ADMINISTRATION_BASE_URL,
        DATABASE_PATH
    }

    public Config() {
        load();
    }

    public String getProperty(Properties property) {
        return getProperty(property.name());
    }

    public String getProperty(Properties property, String defaultValue) {
        return getProperty(property.name(), defaultValue);
    }

    private void load() {
        try {
            this.load(new FileInputStream("router.config"));
        } catch (IOException e) {
            throw new RuntimeException("Failed to load router.config file.", e);
        }
    }
}
