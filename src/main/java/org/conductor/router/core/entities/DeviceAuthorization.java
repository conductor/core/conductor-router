package org.conductor.router.core.entities;

import com.j256.ormlite.field.DatabaseField;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "device_authorization", catalog = "conductor_router")
public class DeviceAuthorization {

    @DatabaseField(canBeNull = false, id = true)
    private String id;

    @DatabaseField(foreign = true)
    private Device device;

    @DatabaseField(foreign = true, foreignAutoRefresh=true)
    private AuthenticationKey accessGivenTo;

    @DatabaseField(foreign = true, foreignAutoRefresh=true)
    private AuthenticationKey accessGivenBy;

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "device_authorization_id", unique = true, nullable = false)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "device_id", nullable = false)
    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public AuthenticationKey getAccessGivenTo() {
        return accessGivenTo;
    }

    public void setAccessGivenTo(AuthenticationKey accessGivenTo) {
        this.accessGivenTo = accessGivenTo;
    }

    public AuthenticationKey getAccessGivenBy() {
        return accessGivenBy;
    }

    public void setAccessGivenBy(AuthenticationKey accessGivenBy) {
        this.accessGivenBy = accessGivenBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DeviceAuthorization that = (DeviceAuthorization) o;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
