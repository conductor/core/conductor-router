package org.conductor.router.core.entities;

import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;


@Entity(name = "device")
@Table(name = "device", catalog = "conductor_router")
public class Device {

    @DatabaseField(canBeNull = false, id = true)
    private String id;

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField(canBeNull = false)
    private String type;

    @DatabaseField(foreign = true, foreignAutoRefresh=true)
    @JsonIgnore
    private ControlUnit controlUnit;

    @ForeignCollectionField(eager = true)
    private Collection<ComponentGroup> componentGroups = new ArrayList<>();

    @ForeignCollectionField(eager = true)
    @JsonIgnore
    private Collection<DeviceAuthorization> deviceAuthorizations = new ArrayList<>(0);

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    //@Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    //@Column(name = "type", nullable = false, length = 100)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "control_unit_id", nullable = false)
    public ControlUnit getControlUnit() {
        return controlUnit;
    }

    public void setControlUnit(ControlUnit controlUnit) {
        this.controlUnit = controlUnit;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "device")
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    public Collection<ComponentGroup> getComponentGroups() {
        return componentGroups;
    }

    public void setComponentGroups(Collection<ComponentGroup> componentGroups) {
        this.componentGroups = componentGroups;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "device")
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    public Collection<DeviceAuthorization> getDeviceAuthorizations() {
        return deviceAuthorizations;
    }

    public void setDeviceAuthorizations(Collection<DeviceAuthorization> deviceAuthorizations) {
        this.deviceAuthorizations = deviceAuthorizations;
    }

    @Override
    public String toString() {
        return "Device [id=" + id + ", name=" + name + ", type=" + type + ", componentGroups=" + componentGroups + ", deviceAuthorizations=" + deviceAuthorizations + "]";
    }

}
