package org.conductor.router.core.entities;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.types.response.controlunit.*;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Henrik on 24/07/2016.
 */
@Entity(name = "setPropertyValueResponse")
@Table(name = "setPropertyValueResponse", catalog = "conductor_router")
public class SetPropertyValueResponse extends org.conductor.router.core.types.response.controlunit.Response {

    public SetPropertyValueResponse() {

    }

    public SetPropertyValueResponse(String id, String requestId, AuthenticationKey authenticationKey, ResponseStatus status) {
        super(id, requestId, authenticationKey, status);
    }

}
