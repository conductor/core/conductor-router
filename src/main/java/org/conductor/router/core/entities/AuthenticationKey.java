package org.conductor.router.core.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import org.conductor.router.core.entities.enums.KeyType;

@Entity
@Table(name = "authentication_key", catalog = "conductor_router")
public class AuthenticationKey {

    @DatabaseField(canBeNull = false, id = true)
    private String id;

    @DatabaseField(foreign = true)
    @JsonIgnore
    private User user;

    @DatabaseField(foreign = true)
    @JsonIgnore
    private ControlUnit controlUnit;

    @DatabaseField(foreign = true)
    @JsonIgnore
    private Application application;

    @DatabaseField(canBeNull = false)
    private String privateKey;

    @DatabaseField(canBeNull = false)
    private String publicKey;

    @DatabaseField(canBeNull = false, dataType = DataType.ENUM_STRING)
    private KeyType keyType;

    @JsonIgnore
    private Set<DeviceAuthorization> deviceAuthorizations = new HashSet<DeviceAuthorization>(0);

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "authentication_key_id", unique = true, nullable = false)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", nullable = false)
    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ControlUnit getControlUnit() {
        return controlUnit;
    }

    public void setControlUnit(ControlUnit controlUnit) {
        this.controlUnit = controlUnit;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    @Column(name = "key_type", nullable = false, length = 20)
    @Enumerated(EnumType.STRING)
    public KeyType getKeyType() {
        return keyType;
    }

    public void setKeyType(KeyType keyType) {
        this.keyType = keyType;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "authenticationKey")
    public Set<DeviceAuthorization> getDeviceAuthorizations() {
        return deviceAuthorizations;
    }

    public void setDeviceAuthorizations(Set<DeviceAuthorization> deviceAuthorizations) {
        this.deviceAuthorizations = deviceAuthorizations;
    }

    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    @Override
    public String toString() {
        return "AuthenticationKey [id=" + id + ", user=" + user + ", controlUnit=" + controlUnit + ", keyType=" + keyType + ", deviceAuthorizations=" + deviceAuthorizations + "]";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AuthenticationKey that = (AuthenticationKey) o;

        return privateKey != null ? privateKey.equals(that.privateKey) : that.privateKey == null;

    }

    @Override
    public int hashCode() {
        return privateKey != null ? privateKey.hashCode() : 0;
    }

}
