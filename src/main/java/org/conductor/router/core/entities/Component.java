package org.conductor.router.core.entities;

import java.util.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import org.conductor.router.core.deserializers.MethodsDeserializer;
import org.conductor.router.core.deserializers.PropertiesDeserializer;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "component", catalog = "conductor_router")
public class Component {

    @DatabaseField(canBeNull = false, id = true)
    private String id;

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField(canBeNull = false)
    private String type;

    @DatabaseField(foreign = true, foreignAutoRefresh=true)
    @JsonIgnore
    private ComponentGroup componentGroup;

    @ForeignCollectionField(eager = true)
    @JsonDeserialize(converter = PropertiesDeserializer.class)
    private Collection<Property> properties = new ArrayList<>();

    @ForeignCollectionField(eager = true)
    @JsonDeserialize(converter = MethodsDeserializer.class)
    private Collection<Method> methods = new ArrayList<>();

    @ForeignCollectionField(eager = true)
    private Collection<Option> options = new ArrayList<>();

    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @Column(name = "component_id", unique = true, nullable = false, columnDefinition = "BINARY(16)")
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "type", nullable = false, length = 100)
    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "component")
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    public Collection<Property> getProperties() {
        return properties;
    }

    public void setProperties(Collection<Property> properties) {
        this.properties = properties;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "component")
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    public Collection<Method> getMethods() {
        return methods;
    }

    public void setMethods(Collection<Method> method) {
        this.methods = method;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "component_group_id", nullable = false)
    public ComponentGroup getComponentGroup() {
        return componentGroup;
    }

    public void setComponentGroup(ComponentGroup componentGroup) {
        this.componentGroup = componentGroup;
    }

    public Collection<Option> getOptions() {
        return options;
    }

    public void setOptions(Collection<Option> options) {
        this.options = options;
    }
}
