package org.conductor.router.core.entities;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.types.response.controlunit.*;

/**
 * Created by Henrik on 03/08/2016.
 */
public class InstallDeviceResponse extends org.conductor.router.core.types.response.controlunit.Response {

    public InstallDeviceResponse() {

    }

    public InstallDeviceResponse(String id, String requestId, AuthenticationKey authenticationKey, ResponseStatus status) {
        super(id, requestId, authenticationKey, status);
    }

}
