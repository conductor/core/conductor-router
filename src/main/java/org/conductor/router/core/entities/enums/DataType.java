package org.conductor.router.core.entities.enums;

import com.fasterxml.jackson.annotation.JsonValue;
import com.google.gson.annotations.SerializedName;

public enum DataType {
    @SerializedName("boolean")
    BOOLEAN("boolean"),
    @SerializedName("string")
    STRING("string"),
    @SerializedName("integer")
    INTEGER("integer"),
    @SerializedName("double")
    DOUBLE("double");

    private String dataType;

    private DataType(String dataType) {
        this.dataType = dataType;
    }

    @JsonValue
    public String getValue() {
        return this.dataType;
    }

    @Override
    public String toString() {
        return this.dataType;
    }
}
