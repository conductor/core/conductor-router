package org.conductor.router.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.j256.ormlite.field.DatabaseField;
import org.conductor.router.core.entities.enums.DataType;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Henrik on 04/08/2016.
 */
@Entity
@Table(name = "option", catalog = "conductor_router")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Option {

    @DatabaseField(canBeNull = false, id = true)
    private String id;

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField(canBeNull = false)
    private DataType dataType;

    @DatabaseField(canBeNull = false)
    private String value;

    @JsonIgnore
    @DatabaseField(foreign = true)
    private ComponentGroup componentGroup;

    @JsonIgnore
    @DatabaseField(foreign = true)
    private Component component;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public ComponentGroup getComponentGroup() {
        return componentGroup;
    }

    public void setComponentGroup(ComponentGroup componentGroup) {
        this.componentGroup = componentGroup;
    }

    public Component getComponent() {
        return component;
    }

    public void setComponent(Component component) {
        this.component = component;
    }

}
