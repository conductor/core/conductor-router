package org.conductor.router.core.entities;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.ComponentGroup;
import org.conductor.router.core.entities.ControlUnit;
import org.conductor.router.core.types.request.controlunit.*;

/**
 * Created by Henrik on 24/07/2016.
 */
public class InstallComponentGroupRequest extends org.conductor.router.core.types.request.controlunit.Request {

    private ComponentGroup componentGroup;

    public InstallComponentGroupRequest() {

    }

    public InstallComponentGroupRequest(String id, AuthenticationKey requester, ControlUnit controlUnit, ComponentGroup componentGroup) {
        this.id = id;
        this.requester = requester;
        this.controlUnit = controlUnit;
        this.componentGroup = componentGroup;
    }

    public ComponentGroup getComponentGroup() {
        return componentGroup;
    }

    public void setComponentGroup(ComponentGroup componentGroup) {
        this.componentGroup = componentGroup;
    }

}
