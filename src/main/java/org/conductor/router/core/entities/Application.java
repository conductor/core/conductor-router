package org.conductor.router.core.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collection;
import java.util.UUID;

/**
 * Created by Henrik on 15/03/2016.
 */
@Entity
@Table(name = "application", catalog = "conductor_router")
public class Application {

    @DatabaseField(canBeNull = false, id = true)
    private String id;

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField(foreign = true)
    private User owner;

    @DatabaseField(foreign = true, foreignAutoRefresh=true)
    private AuthenticationKey authenticationKey;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public AuthenticationKey getAuthenticationKey() {
        return authenticationKey;
    }

    public void setAuthenticationKey(AuthenticationKey authenticationKey) {
        this.authenticationKey = authenticationKey;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Application that = (Application) o;

        return id != null ? id.equals(that.id) : that.id == null;

    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }

}
