package org.conductor.router.core.entities;

import com.j256.ormlite.field.DatabaseField;
import org.conductor.router.core.entities.enums.ApplicationInviteStatus;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Henrik on 31/08/2016.
 */
@Entity(name = "applicationInvite")
@Table(name = "applicationInvite", catalog = "conductor_router")
public class ApplicationInvite {

    @DatabaseField(canBeNull = false, id = true)
    private String id;

    @DatabaseField
    private String externalId;

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Application application;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 3)
    private User user;

    @DatabaseField
    private ApplicationInviteStatus applicationInviteStatus;

    private String inviteUrl;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ApplicationInviteStatus getApplicationInviteStatus() {
        return applicationInviteStatus;
    }

    public void setApplicationInviteStatus(ApplicationInviteStatus applicationInviteStatus) {
        this.applicationInviteStatus = applicationInviteStatus;
    }

    public String getInviteUrl() {
        return inviteUrl;
    }

    public void setInviteUrl(String inviteUrl) {
        this.inviteUrl = inviteUrl;
    }

}
