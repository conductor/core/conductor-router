package org.conductor.router.core.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import org.conductor.router.core.entities.enums.DataType;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "method_parameter_definition", catalog = "conductor_router")
public class MethodParameterDefinition {

    @DatabaseField(canBeNull = false, id = true)
    private String id;

    @DatabaseField(foreign = true)
    @JsonIgnore
    private Method method;

    @DatabaseField(canBeNull = false)
    private String name;

    @DatabaseField(canBeNull = false)
    private DataType dataType;

    private Collection<MethodParameter> methodParameters = new ArrayList<>();

    @Id
    @GeneratedValue(strategy = IDENTITY)
    @Column(name = "method_parameter_definition_id", unique = true, nullable = false)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "method_id", nullable = false)
    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "data_type", nullable = false, length = 100)
    @Enumerated(EnumType.STRING)
    public DataType getDataType() {
        return dataType;
    }

    public void setDataType(DataType dataType) {
        this.dataType = dataType;
    }

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "methodParameterDefinition")
    @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
    public Collection<MethodParameter> getMethodParameters() {
        return methodParameters;
    }

    public void setMethodParameters(Collection<MethodParameter> methodParameters) {
        this.methodParameters = methodParameters;
    }
}
