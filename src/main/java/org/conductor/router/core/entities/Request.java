package org.conductor.router.core.entities;

import java.util.Date;
import java.util.UUID;

public class Request {

  private UUID id;
  private Date requestDate;

  public UUID getId() {
    return id;
  }

  public void setId(UUID id) {
    this.id = id;
  }

  public Date getRequestDate() {
    return requestDate;
  }

  public void setRequestDate(Date requestDate) {
    this.requestDate = requestDate;
  }

}
