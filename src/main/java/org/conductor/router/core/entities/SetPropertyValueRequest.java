package org.conductor.router.core.entities;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.Property;
import org.conductor.router.core.types.request.controlunit.*;
import org.conductor.router.core.types.response.controlunit.ResponseStatus;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Created by Henrik on 13/03/2016.
 */
@Entity(name = "setPropertyValueRequest")
@Table(name = "setPropertyValueRequest", catalog = "conductor_router")
public class SetPropertyValueRequest extends org.conductor.router.core.types.request.controlunit.Request {

    @DatabaseField(foreign = true, foreignAutoRefresh = true)
    private Property property;

    @DatabaseField(dataType = DataType.SERIALIZABLE)
    private String propertyValue;

    @DatabaseField(canBeNull = false)
    private ResponseStatus status;

    @DatabaseField(foreign = true, foreignAutoRefresh = true, maxForeignAutoRefreshLevel = 3)
    private SetPropertyValueRequestSummary setPropertyValueRequestSummary;

    public SetPropertyValueRequest() {

    }

    public SetPropertyValueRequest(String id, Property property, Object propertyValue, ResponseStatus status) {
        super.id = id;
        super.controlUnit = property.getComponent().getComponentGroup().getDevice().getControlUnit();
        this.component = property.getComponent();
        this.property = property;
        this.propertyValue = propertyValue.toString();
        this.status = status;
    }

    public SetPropertyValueRequestSummary getRequestSummary() {
        return setPropertyValueRequestSummary;
    }

    public void setSetPropertyValueRequestSummary(SetPropertyValueRequestSummary setPropertyValueRequestSummary) {
        this.setPropertyValueRequestSummary = setPropertyValueRequestSummary;
    }

    public void setPropertyValue(String propertyValue) {
        this.propertyValue = propertyValue;
    }

    public Property getProperty() {
        return property;
    }

    public void setProperty(Property property) {
        this.property = property;
        this.component = property.getComponent();
        this.controlUnit = property.getComponent().getComponentGroup().getDevice().getControlUnit();
    }

    public Object getPropertyValue() {
        if (property == null) {
            throw new RuntimeException("Can't deserialize property value because property isn't set.");
        }

        switch (property.getDataType()) {
            case STRING:
                return propertyValue;
            case DOUBLE:
                return Double.parseDouble(propertyValue);
            case INTEGER:
                return Integer.parseInt(propertyValue);
            case BOOLEAN:
                return Boolean.parseBoolean(propertyValue);
            default:
                throw new RuntimeException("Can't deserialize property value because unexpected data type '" + property.getDataType() + "'");
        }
    }

    public void setPropertyValue(Object propertyValue) {
        this.propertyValue = propertyValue.toString();
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }
}
