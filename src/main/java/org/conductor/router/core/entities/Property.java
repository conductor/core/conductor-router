package org.conductor.router.core.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j256.ormlite.field.DatabaseField;
import org.conductor.router.core.entities.enums.DataType;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Table(name = "property", catalog = "conductor_router")
public class Property {

  @DatabaseField(canBeNull = false, id = true)
  private String id;

  @DatabaseField(foreign = true, foreignAutoRefresh=true, maxForeignAutoRefreshLevel = 5)
  @JsonIgnore
  private Component component;

  @DatabaseField(canBeNull = false)
  private String name;

  @DatabaseField(canBeNull = false)
  private DataType dataType;

  @DatabaseField(canBeNull = false)
  private String value;

  @DatabaseField
  private Boolean readOnly;

  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "property_id", unique = true, nullable = false)
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "component_id", nullable = false)
  public Component getComponent() {
    return component;
  }

  public void setComponent(Component component) {
    this.component = component;
  }

  @Column(name = "name", nullable = false, length = 100)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Column(name = "data_type", nullable = false, length = 100)
  @Enumerated(EnumType.STRING)
  public DataType getDataType() {
    return dataType;
  }

  public void setDataType(DataType dataType) {
    this.dataType = dataType;
  }

  @Column(name = "value", nullable = false, length = 300)
  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  public Boolean isReadOnly() {
    return readOnly;
  }

  public void setReadOnly(Boolean readOnly) {
    this.readOnly = readOnly;
  }

  @Override
  public String toString() {
    return "Property [id=" + id + ", name=" + name + ", dataType=" + dataType + ", value=" + value + "]";
  }

}
