package org.conductor.router.core.entities;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.Component;
import org.conductor.router.core.entities.ControlUnit;
import org.conductor.router.core.types.request.controlunit.*;

/**
 * Created by Henrik on 04/08/2016.
 */
public class InstallControlUnitRequest extends org.conductor.router.core.types.request.controlunit.Request {

    public InstallControlUnitRequest(String id, AuthenticationKey requester) {
        this.id = id;
        this.requester = requester;
    }

    @Override
    public String getId() {
        return id;
    }

    @Override
    public AuthenticationKey getRequester() {
        return requester;
    }

    @Override
    public ControlUnit getControlUnit() {
        return null;
    }

    @Override
    public Component getComponent() {
        return null;
    }
}
