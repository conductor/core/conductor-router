package org.conductor.router.core.entities;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.types.response.controlunit.Response;
import org.conductor.router.core.types.response.controlunit.ResponseStatus;

/**
 * Created by Henrik on 04/08/2016.
 */
public class InstallControlUnitResponse extends Response {

    public InstallControlUnitResponse() {

    }

    public InstallControlUnitResponse(String id, String requestId, AuthenticationKey requester, ResponseStatus responseStatus) {
        super(id, requestId, requester, responseStatus);
    }

}
