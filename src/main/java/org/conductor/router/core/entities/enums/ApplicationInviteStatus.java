package org.conductor.router.core.entities.enums;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Henrik on 31/08/2016.
 */
public enum ApplicationInviteStatus {
    @SerializedName("waiting")
    WAITING("waiting"),
    @SerializedName("accepted")
    ACCEPTED("accepted"),
    @SerializedName("completed")
    COMPLETED("completed");

    private String status;

    ApplicationInviteStatus(String status) {
        this.status = status;
    }

    public String getValue() {
        return this.status;
    }

    @Override
    public String toString() {
        return this.status;
    }
}
