package org.conductor.router.core.entities.enums;

public enum KeyType {
    USER_KEY,
    APPLICATION_KEY,
    CONTROL_UNIT_KEY
}
