package org.conductor.router.core.entities;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import org.conductor.router.core.types.request.controlunit.IRequest;
import org.conductor.router.core.types.response.controlunit.ResponseStatus;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by Henrik on 19/09/2016.
 */
@Entity(name = "setPropertyValueRequestSummary")
@Table(name = "setPropertyValueRequestSummary", catalog = "conductor_router")
public class SetPropertyValueRequestSummary extends org.conductor.router.core.types.request.controlunit.Request {

    @ForeignCollectionField(eager = true, maxEagerForeignCollectionLevel = 5, maxEagerLevel = 5)
    private Collection<SetPropertyValueRequest> setPropertyValueRequests = new ArrayList<>();

    @DatabaseField(canBeNull = false)
    private ResponseStatus status;

    @DatabaseField(canBeNull = true)
    private String errorMessage;

    public SetPropertyValueRequestSummary() {

    }

    public SetPropertyValueRequestSummary(String id, AuthenticationKey requester, ResponseStatus status, Collection<SetPropertyValueRequest> setPropertyValueRequests) {
        this.id = id;
        this.requester = requester;
        this.status = status;
        this.setPropertyValueRequests = setPropertyValueRequests;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Collection<SetPropertyValueRequest> getRequests() {
        return setPropertyValueRequests;
    }

    public void setRequests(Collection<SetPropertyValueRequest> setPropertyValueRequests) {
        this.setPropertyValueRequests = setPropertyValueRequests;
    }

    public AuthenticationKey getRequester() {
        return requester;
    }

    public void setRequester(AuthenticationKey requester) {
        this.requester = requester;
    }

    public Collection<SetPropertyValueRequest> getSetPropertyValueRequests() {
        return setPropertyValueRequests;
    }

    public void setSetPropertyValueRequests(Collection<SetPropertyValueRequest> setPropertyValueRequests) {
        this.setPropertyValueRequests = setPropertyValueRequests;
    }

    public ResponseStatus getStatus() {
        return status;
    }

    public void setStatus(ResponseStatus status) {
        this.status = status;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }


}
