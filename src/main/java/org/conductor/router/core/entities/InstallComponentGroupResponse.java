package org.conductor.router.core.entities;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.types.response.controlunit.*;

import java.util.UUID;

/**
 * Created by Henrik on 24/07/2016.
 */
public class InstallComponentGroupResponse extends org.conductor.router.core.types.response.controlunit.Response {

    public InstallComponentGroupResponse() {

    }

    public InstallComponentGroupResponse(String id, String requestId, AuthenticationKey authenticationKey, ResponseStatus status) {
        super(id, requestId, authenticationKey, status);
    }

}
