package org.conductor.router.core.entities;

import static javax.persistence.GenerationType.IDENTITY;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.field.ForeignCollectionField;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
@Table(name = "method", catalog = "conductor_router")
public class Method {

  @DatabaseField(canBeNull = false, id = true)
  private String id;

  @DatabaseField(foreign = true)
  @JsonIgnore
  private Component component;

  @DatabaseField(canBeNull = false)
  private String name;

  @ForeignCollectionField(eager = true)
  private Collection<MethodParameterDefinition> methodParameterDefinitions = new ArrayList<>();

  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "method_id", unique = true, nullable = false)
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @OneToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "component_id", nullable = false)
  public Component getComponent() {
    return component;
  }

  public void setComponent(Component component) {
    this.component = component;
  }

  @Column(name = "name", nullable = false, length = 100)
  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "method")
  @Cascade({CascadeType.SAVE_UPDATE, CascadeType.DELETE})
  public Collection<MethodParameterDefinition> getMethodParameterDefinitions() {
    return methodParameterDefinitions;
  }

  public void setMethodParameterDefinitions(Collection<MethodParameterDefinition> methodParameterDefinitions) {
    this.methodParameterDefinitions = methodParameterDefinitions;
  }

}
