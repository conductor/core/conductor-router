package org.conductor.router.core.entities;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.ControlUnit;
import org.conductor.router.core.types.request.controlunit.*;

/**
 * Created by Henrik on 03/08/2016.
 */
public class InstallDeviceRequest extends org.conductor.router.core.types.request.controlunit.Request {

    public InstallDeviceRequest() {

    }

    public InstallDeviceRequest(String id, AuthenticationKey requester, ControlUnit controlUnit) {
        this.id = id;
        this.requester = requester;
        this.controlUnit = controlUnit;
    }

}
