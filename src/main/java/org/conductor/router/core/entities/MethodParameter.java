package org.conductor.router.core.entities;

import static javax.persistence.GenerationType.IDENTITY;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "method_parameter", catalog = "conductor_router")
public class MethodParameter {

  private String id;
  private String value;
  private MethodParameterDefinition methodParameterDefinition;

  @Id
  @GeneratedValue(strategy = IDENTITY)
  @Column(name = "method_parameter_id", unique = true, nullable = false)
  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  @Column(name = "value", nullable = false, length = 100)
  public String getValue() {
    return value;
  }

  public void setValue(String value) {
    this.value = value;
  }

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "method_parameter_definition_id", nullable = false)
  public MethodParameterDefinition getMethodParameterDefinition() {
    return methodParameterDefinition;
  }

  public void setMethodParameterDefinition(MethodParameterDefinition methodParameterDefinition) {
    this.methodParameterDefinition = methodParameterDefinition;
  }
}
