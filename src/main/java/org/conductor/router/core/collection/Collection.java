package org.conductor.router.core.collection;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Henrik on 23/07/2016.
 */
public abstract class Collection<T> {

    private List<CollectionListener> listeners = new ArrayList<>();

    public void add(T element) {
        for (CollectionListener listener : listeners) {
            listener.onAdded(element);
        }
    }

    public void remove(T element) {
        for (CollectionListener listener : listeners) {
            listener.onRemoved(element);
        }
    }

    public void update(T element) {
        for (CollectionListener listener : listeners) {
            listener.onUpdated(element);
        }
    }

    public void addListener(CollectionListener<T> listener) {
        listeners.add(listener);
    }

    public void removeListener(CollectionListener<T> listener) {
        listeners.remove(listener);
    }

    public abstract T findById(String id);

    public abstract List<T> find(T object);

    public abstract T findFirst(T object);

}
