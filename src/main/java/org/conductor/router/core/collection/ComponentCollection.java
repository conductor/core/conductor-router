package org.conductor.router.core.collection;

import org.conductor.router.core.entities.Component;
import org.conductor.router.core.repositories.DBRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 14/01/2017.
 */
@Named
public class ComponentCollection extends Collection<Component> {

    @Autowired
    private DBRepository repository;

    @Override
    public void add(Component component) {
        try {
            repository.add(component);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(component);
    }

    @Override
    public void update(Component component) {
        try {
            repository.update(component);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(component);
    }

    @Override
    public void remove(Component component) {
        try {
            repository.remove(component);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(component);
    }

    public Component findById(String id) {
        try {
            return repository.queryForId(Component.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Component> find(Component component) {
        try {
            return repository.queryForMatching(component);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Component findFirst(Component component) {
        try {
            return repository.queryForFirstMatching(component);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
