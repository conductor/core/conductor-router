package org.conductor.router.core.collection;

import org.conductor.router.core.repositories.DBRepository;
import org.conductor.router.core.entities.InstallComponentGroupResponse;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 24/07/2016.
 */
@Named
public class InstallComponentGroupResponseCollection extends Collection<InstallComponentGroupResponse> {

    @Autowired
    private DBRepository repository;

    @Override
    public void add(InstallComponentGroupResponse installComponentGroupResponse) {
        try {
            repository.add(installComponentGroupResponse);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(installComponentGroupResponse);
    }

    @Override
    public void update(InstallComponentGroupResponse installComponentGroupResponse) {
        try {
            repository.update(installComponentGroupResponse);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(installComponentGroupResponse);
    }

    @Override
    public void remove(InstallComponentGroupResponse installComponentGroupResponse) {
        try {
            repository.remove(installComponentGroupResponse);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(installComponentGroupResponse);
    }

    @Override
    public InstallComponentGroupResponse findById(String id) {
        try {
            return repository.queryForId(InstallComponentGroupResponse.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<InstallComponentGroupResponse> find(InstallComponentGroupResponse object) {
        try {
            return repository.queryForMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public InstallComponentGroupResponse findByRequestId(String requestId) {
        InstallComponentGroupResponse query = new InstallComponentGroupResponse();
        query.setRequestId(requestId);
        return findFirst(query);
    }

    @Override
    public InstallComponentGroupResponse findFirst(InstallComponentGroupResponse object) {
        try {
            return repository.queryForFirstMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
