package org.conductor.router.core.collection;

import org.conductor.router.core.entities.InstallControlUnitResponse;

import javax.inject.Named;
import java.util.List;

/**
 * Created by Henrik on 04/08/2016.
 */
@Named
public class InstallControlUnitResponseCollection extends Collection<InstallControlUnitResponse> {

    @Override
    public InstallControlUnitResponse findById(String id) {
        return null;
    }

    @Override
    public List<InstallControlUnitResponse> find(InstallControlUnitResponse object) {
        return null;
    }

    @Override
    public InstallControlUnitResponse findFirst(InstallControlUnitResponse object) {
        return null;
    }
}
