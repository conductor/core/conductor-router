package org.conductor.router.core.collection;

import org.conductor.router.core.entities.SetPropertyValueRequest;
import org.conductor.router.core.entities.SetPropertyValueRequestSummary;
import org.conductor.router.core.repositories.DBRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 19/09/2016.
 */
@Named
public class SetPropertyValueRequestSummaryCollection extends Collection<SetPropertyValueRequestSummary> {

    @Autowired
    private DBRepository repository;

    @Override
    public void add(SetPropertyValueRequestSummary setPropertyValueRequestSummary) {
        try {
            repository.add(setPropertyValueRequestSummary);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(setPropertyValueRequestSummary);
    }

    @Override
    public void update(SetPropertyValueRequestSummary setPropertyValueRequestSummary) {
        try {
            repository.update(setPropertyValueRequestSummary);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(setPropertyValueRequestSummary);
    }

    @Override
    public void remove(SetPropertyValueRequestSummary setPropertyValueRequestSummary) {
        try {
            repository.remove(setPropertyValueRequestSummary);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(setPropertyValueRequestSummary);
    }

    @Override
    public SetPropertyValueRequestSummary findById(String id) {
        try {
            return repository.queryForId(SetPropertyValueRequestSummary.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<SetPropertyValueRequestSummary> find(SetPropertyValueRequestSummary object) {
        try {
            return repository.queryForMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public SetPropertyValueRequestSummary findFirst(SetPropertyValueRequestSummary object) {
        try {
            return repository.queryForFirstMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
