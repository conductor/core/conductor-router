package org.conductor.router.core.collection;

import org.conductor.router.core.entities.ControlUnit;
import org.conductor.router.core.repositories.DBRepository;
import org.conductor.router.core.entities.Device;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 25/07/2016.
 */
@Named
public class DeviceCollection extends Collection<Device> {

    @Autowired
    private DBRepository repository;

    @Override
    public void add(Device device) {
        try {
            repository.add(device);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(device);
    }

    @Override
    public void update(Device device) {
        try {
            repository.update(device);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(device);
    }

    @Override
    public void remove(Device device) {
        try {
            repository.remove(device);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(device);
    }

    @Override
    public Device findById(String id) {
        try {
            return repository.queryForId(Device.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Device> find(Device object) {
        try {
            return repository.queryForMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Device findFirst(Device object) {
        try {
            return repository.queryForFirstMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
