package org.conductor.router.core.collection;

import org.conductor.router.core.entities.Application;
import org.conductor.router.core.entities.ControlUnit;
import org.conductor.router.core.repositories.DBRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 26/07/2016.
 */
@Named
public class ControlUnitCollection extends Collection<ControlUnit> {

    @Autowired
    private DBRepository repository;

    @Override
    public void add(ControlUnit controlUnit) {
        try {
            repository.add(controlUnit);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(controlUnit);
    }

    @Override
    public void update(ControlUnit controlUnit) {
        try {
            repository.update(controlUnit);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(controlUnit);
    }

    @Override
    public void remove(ControlUnit controlUnit) {
        try {
            repository.remove(controlUnit);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(controlUnit);
    }

    @Override
    public ControlUnit findById(String id) {
        try {
            return repository.queryForId(ControlUnit.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<ControlUnit> find(ControlUnit object) {
        try {
            return repository.queryForMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ControlUnit findFirst(ControlUnit object) {
        try {
            return repository.queryForFirstMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
