package org.conductor.router.core.collection;

/**
 * Created by Henrik on 23/07/2016.
 */
public interface CollectionListener<T> {

    void onAdded(T element);
    void onUpdated(T element);
    void onRemoved(T element);

}
