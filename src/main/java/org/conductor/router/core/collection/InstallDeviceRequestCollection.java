package org.conductor.router.core.collection;

import org.conductor.router.core.repositories.DBRepository;
import org.conductor.router.core.entities.InstallDeviceRequest;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 03/08/2016.
 */
@Named
public class InstallDeviceRequestCollection extends Collection<InstallDeviceRequest> {

    @Autowired
    private DBRepository repository;

    @Override
    public void add(InstallDeviceRequest installDeviceRequest) {
        try {
            repository.add(installDeviceRequest);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(installDeviceRequest);
    }

    @Override
    public void update(InstallDeviceRequest installDeviceRequest) {
        try {
            repository.update(installDeviceRequest);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(installDeviceRequest);
    }

    @Override
    public void remove(InstallDeviceRequest installDeviceRequest) {
        try {
            repository.remove(installDeviceRequest);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(installDeviceRequest);
    }

    @Override
    public InstallDeviceRequest findById(String id) {
        return null;
    }

    @Override
    public List<InstallDeviceRequest> find(InstallDeviceRequest object) {
        return null;
    }

    @Override
    public InstallDeviceRequest findFirst(InstallDeviceRequest object) {
        return null;
    }

}
