package org.conductor.router.core.collection;

import org.conductor.router.core.entities.Device;
import org.conductor.router.core.entities.Property;
import org.conductor.router.core.repositories.DBRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 04/08/2016.
 */
@Named
public class PropertyCollection extends Collection<Property> {

    @Autowired
    private DBRepository repository;

    @Override
    public void add(Property property) {
        try {
            repository.add(property);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(property);
    }

    @Override
    public void update(Property property) {
        try {
            repository.update(property);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(property);
    }

    @Override
    public void remove(Property property) {
        try {
            repository.remove(property);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(property);
    }

    @Override
    public Property findById(String id) {
        try {
            return repository.queryForId(Device.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Property> find(Property object) {
        try {
            return repository.queryForMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Property findFirst(Property object) {
        try {
            return repository.queryForFirstMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
