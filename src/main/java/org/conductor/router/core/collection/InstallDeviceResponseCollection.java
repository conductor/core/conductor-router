package org.conductor.router.core.collection;

import org.conductor.router.core.repositories.DBRepository;
import org.conductor.router.core.entities.InstallDeviceResponse;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 03/08/2016.
 */
@Named
public class InstallDeviceResponseCollection extends Collection<InstallDeviceResponse> {

    @Autowired
    private DBRepository repository;

    @Override
    public void add(InstallDeviceResponse installDeviceResponse) {
        try {
            repository.add(installDeviceResponse);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(installDeviceResponse);
    }

    @Override
    public void update(InstallDeviceResponse installDeviceResponse) {
        try {
            repository.update(installDeviceResponse);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(installDeviceResponse);
    }

    @Override
    public void remove(InstallDeviceResponse installDeviceResponse) {
        try {
            repository.remove(installDeviceResponse);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(installDeviceResponse);
    }

    @Override
    public InstallDeviceResponse findById(String id) {
        return null;
    }

    @Override
    public List<InstallDeviceResponse> find(InstallDeviceResponse object) {
        return null;
    }

    @Override
    public InstallDeviceResponse findFirst(InstallDeviceResponse object) {
        return null;
    }

}
