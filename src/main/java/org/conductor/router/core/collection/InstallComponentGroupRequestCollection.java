package org.conductor.router.core.collection;

import org.conductor.router.core.repositories.DBRepository;
import org.conductor.router.core.entities.InstallComponentGroupRequest;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 24/07/2016.
 */
@Named
public class InstallComponentGroupRequestCollection extends Collection<InstallComponentGroupRequest> {

    @Autowired
    private DBRepository repository;

    @Override
    public void add(InstallComponentGroupRequest installComponentGroupRequest) {
        try {
            repository.add(installComponentGroupRequest);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(installComponentGroupRequest);
    }

    @Override
    public void update(InstallComponentGroupRequest installComponentGroupRequest) {
        try {
            repository.update(installComponentGroupRequest);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(installComponentGroupRequest);
    }

    @Override
    public void remove(InstallComponentGroupRequest installComponentGroupRequest) {
        try {
            repository.remove(installComponentGroupRequest);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(installComponentGroupRequest);
    }

    @Override
    public InstallComponentGroupRequest findById(String id) {
        try {
            return repository.queryForId(InstallComponentGroupRequest.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<InstallComponentGroupRequest> find(InstallComponentGroupRequest object) {
        try {
            return repository.queryForMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public InstallComponentGroupRequest findFirst(InstallComponentGroupRequest object) {
        try {
            return repository.queryForFirstMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
