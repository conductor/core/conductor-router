package org.conductor.router.core.collection;

import org.conductor.router.core.entities.Application;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.repositories.DBRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 04/08/2016.
 */
@Named
public class AuthenticationKeyCollection extends Collection<AuthenticationKey> {

    @Autowired
    private DBRepository repository;

    public AuthenticationKey findByPrivateKey(String privateKey) {
        AuthenticationKey query = new AuthenticationKey();
        query.setPrivateKey(privateKey);
        return findFirst(query);
    }

    public AuthenticationKey findByPublicKey(String publicKey) {
        AuthenticationKey query = new AuthenticationKey();
        query.setPublicKey(publicKey);
        return findFirst(query);
    }

    @Override
    public AuthenticationKey findById(String id) {
        try {
            return repository.queryForId(Application.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<AuthenticationKey> find(AuthenticationKey object) {
        try {
            return repository.queryForMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public AuthenticationKey findFirst(AuthenticationKey object) {
        try {
            return repository.queryForFirstMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
