package org.conductor.router.core.collection;

import org.conductor.router.core.entities.InstallControlUnitRequest;

import javax.inject.Named;
import java.util.List;

/**
 * Created by Henrik on 04/08/2016.
 */
@Named
public class InstallControlUnitRequestCollection extends Collection<InstallControlUnitRequest> {

    @Override
    public InstallControlUnitRequest findById(String id) {
        return null;
    }

    @Override
    public List<InstallControlUnitRequest> find(InstallControlUnitRequest object) {
        return null;
    }

    @Override
    public InstallControlUnitRequest findFirst(InstallControlUnitRequest object) {
        return null;
    }

}
