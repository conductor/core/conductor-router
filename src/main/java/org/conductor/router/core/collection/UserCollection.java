package org.conductor.router.core.collection;

import org.conductor.router.core.entities.User;
import org.conductor.router.core.repositories.DBRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 03/08/2016.
 */
@Named
public class UserCollection extends Collection<User> {

    @Inject
    private DBRepository repository;

    @Override
    public void add(User user) {
        try {
            repository.add(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(user);
    }

    @Override
    public void update(User user) {
        try {
            repository.update(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(user);
    }

    @Override
    public void remove(User user) {
        try {
            repository.remove(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(user);
    }

    @Override
    public User findById(String id) {
        try {
            return repository.queryForId(User.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<User> find(User user) {
        try {
            return repository.queryForMatching(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public User findFirst(User user) {
        try {
            return repository.queryForFirstMatching(user);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
