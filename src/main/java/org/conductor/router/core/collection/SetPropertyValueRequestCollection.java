package org.conductor.router.core.collection;

import org.conductor.router.core.repositories.DBRepository;
import org.conductor.router.core.entities.SetPropertyValueRequest;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 23/07/2016.
 */
@Named
public class SetPropertyValueRequestCollection extends Collection<SetPropertyValueRequest> {

    @Autowired
    private DBRepository repository;

    @Override
    public void add(SetPropertyValueRequest setPropertyValueRequest) {
        try {
            repository.add(setPropertyValueRequest);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(setPropertyValueRequest);
    }

    @Override
    public void update(SetPropertyValueRequest setPropertyValueRequest) {
        try {
            repository.update(setPropertyValueRequest);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(setPropertyValueRequest);
    }

    @Override
    public void remove(SetPropertyValueRequest setPropertyValueRequest) {
        try {
            repository.remove(setPropertyValueRequest);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(setPropertyValueRequest);
    }

    @Override
    public SetPropertyValueRequest findById(String id) {
        try {
            return repository.queryForId(SetPropertyValueRequest.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<SetPropertyValueRequest> find(SetPropertyValueRequest object) {
        try {
            return repository.queryForMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public SetPropertyValueRequest findFirst(SetPropertyValueRequest object) {
        try {
            return repository.queryForFirstMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
