package org.conductor.router.core.collection;

import org.conductor.router.core.entities.Application;
import org.conductor.router.core.repositories.DBRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Inject;
import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 03/08/2016.
 */
@Named
public class ApplicationCollection extends Collection<Application> {

    @Inject
    private DBRepository repository;

    @Override
    public void add(Application application) {
        try {
            repository.add(application);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(application);
    }

    @Override
    public void update(Application application) {
        try {
            repository.update(application);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(application);
    }

    @Override
    public void remove(Application application) {
        try {
            repository.remove(application);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(application);
    }

    @Override
    public Application findById(String id) {
        try {
            return repository.queryForId(Application.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<Application> find(Application object) {
        try {
            return repository.queryForMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Application findFirst(Application object) {
        try {
            return repository.queryForFirstMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
