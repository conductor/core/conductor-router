package org.conductor.router.core.collection;

import org.conductor.router.core.entities.ComponentGroup;
import org.conductor.router.core.entities.Device;
import org.conductor.router.core.repositories.DBRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 08/08/2016.
 */
@Named
public class ComponentGroupCollection extends Collection<ComponentGroup> {

    @Autowired
    private DBRepository repository;

    @Override
    public void add(ComponentGroup componentGroup) {
        try {
            repository.add(componentGroup);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(componentGroup);
    }

    @Override
    public void update(ComponentGroup componentGroup) {
        try {
            repository.update(componentGroup);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(componentGroup);
    }

    @Override
    public void remove(ComponentGroup componentGroup) {
        try {
            repository.remove(componentGroup);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(componentGroup);
    }

    public ComponentGroup findById(String id) {
        try {
            return repository.queryForId(ComponentGroup.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<ComponentGroup> find(ComponentGroup object) {
        try {
            return repository.queryForMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ComponentGroup findFirst(ComponentGroup object) {
        try {
            return repository.queryForFirstMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
