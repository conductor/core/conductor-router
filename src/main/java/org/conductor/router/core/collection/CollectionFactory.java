package org.conductor.router.core.collection;

import org.conductor.router.core.entities.InstallComponentGroupRequest;
import org.conductor.router.core.entities.InstallControlUnitRequest;
import org.conductor.router.core.entities.InstallDeviceRequest;
import org.conductor.router.core.entities.SetPropertyValueRequest;
import org.conductor.router.core.entities.InstallComponentGroupResponse;
import org.conductor.router.core.entities.InstallControlUnitResponse;
import org.conductor.router.core.entities.InstallDeviceResponse;
import org.conductor.router.core.entities.SetPropertyValueResponse;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Henrik on 25/07/2016.
 */
@Named
public class CollectionFactory {

    @Inject
    private InstallComponentGroupRequestCollection installComponentGroupRequestCollection;

    @Inject
    private InstallComponentGroupResponseCollection installComponentGroupResponseCollection;

    @Inject
    private SetPropertyValueRequestCollection setPropertyValueRequestCollection;

    @Inject
    private SetPropertyValueResponseCollection setPropertyValueResponseCollection;

    @Inject
    private InstallDeviceRequestCollection installDeviceRequestCollection;

    @Inject
    private InstallDeviceResponseCollection installDeviceResponseCollection;

    @Inject
    private InstallControlUnitRequestCollection installControlUnitRequestCollection;

    @Inject
    private InstallControlUnitResponseCollection installControlUnitResponseCollection;

    private Map<Class, Collection> collectionMap = new HashMap<>();

    @PostConstruct
    public void init() {
        collectionMap.put(InstallComponentGroupRequest.class, installComponentGroupRequestCollection);
        collectionMap.put(InstallComponentGroupResponse.class, installComponentGroupResponseCollection);
        collectionMap.put(SetPropertyValueRequest.class, setPropertyValueRequestCollection);
        collectionMap.put(SetPropertyValueResponse.class, setPropertyValueResponseCollection);
        collectionMap.put(InstallDeviceRequest.class, installDeviceRequestCollection);
        collectionMap.put(InstallDeviceResponse.class, installDeviceResponseCollection);
        collectionMap.put(InstallControlUnitRequest.class, installControlUnitRequestCollection);
        collectionMap.put(InstallControlUnitResponse.class, installControlUnitResponseCollection);
    }

    public <T> Collection<T> getCollectionFor(T clazz) {
        return collectionMap.get(clazz);
    }

}
