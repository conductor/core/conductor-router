package org.conductor.router.core.collection;

import org.conductor.router.core.repositories.DBRepository;
import org.conductor.router.core.entities.ApplicationInvite;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 31/08/2016.
 */
@Named
public class ApplicationInviteCollection extends Collection<ApplicationInvite> {

    @Autowired
    private DBRepository repository;

    @Override
    public void add(ApplicationInvite applicationInvite) {
        try {
            repository.add(applicationInvite);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(applicationInvite);
    }

    @Override
    public void update(ApplicationInvite applicationInvite) {
        try {
            repository.update(applicationInvite);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(applicationInvite);
    }

    @Override
    public void remove(ApplicationInvite applicationInvite) {
        try {
            repository.remove(applicationInvite);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(applicationInvite);
    }

    @Override
    public ApplicationInvite findById(String id) {
        try {
            return repository.queryForId(ApplicationInvite.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<ApplicationInvite> find(ApplicationInvite object) {
        try {
            return repository.queryForMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public ApplicationInvite findFirst(ApplicationInvite object) {
        try {
            return repository.queryForFirstMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
