package org.conductor.router.core.collection;

import org.conductor.router.core.entities.Device;
import org.conductor.router.core.entities.DeviceAuthorization;
import org.conductor.router.core.repositories.DBRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 09/08/2016.
 */
@Named
public class DeviceAuthorizationCollection extends Collection<DeviceAuthorization> {

    @Autowired
    private DBRepository repository;

    @Override
    public DeviceAuthorization findById(String id) {
        try {
            return repository.queryForId(Device.class, id);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public List<DeviceAuthorization> find(DeviceAuthorization object) {
        try {
            return repository.queryForMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void remove(DeviceAuthorization deviceAuthorization) {
        try {
            repository.remove(deviceAuthorization);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(deviceAuthorization);
    }

    @Override
    public DeviceAuthorization findFirst(DeviceAuthorization object) {
        try {
            return repository.queryForFirstMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
