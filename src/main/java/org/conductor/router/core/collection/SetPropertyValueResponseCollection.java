package org.conductor.router.core.collection;

import org.conductor.router.core.repositories.DBRepository;
import org.conductor.router.core.entities.SetPropertyValueResponse;
import org.springframework.beans.factory.annotation.Autowired;

import javax.inject.Named;
import java.sql.SQLException;
import java.util.List;

/**
 * Created by Henrik on 24/07/2016.
 */
@Named
public class SetPropertyValueResponseCollection extends Collection<SetPropertyValueResponse> {

    @Autowired
    private DBRepository repository;

    @Override
    public void add(SetPropertyValueResponse setPropertyValueResponse) {
        try {
            repository.add(setPropertyValueResponse);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.add(setPropertyValueResponse);
    }

    @Override
    public void update(SetPropertyValueResponse setPropertyValueResponse) {
        try {
            repository.update(setPropertyValueResponse);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.update(setPropertyValueResponse);
    }

    @Override
    public void remove(SetPropertyValueResponse setPropertyValueResponse) {
        try {
            repository.remove(setPropertyValueResponse);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        super.remove(setPropertyValueResponse);
    }

    public SetPropertyValueResponse findByRequestId(String requestId) {
        return null;
    }

    @Override
    public SetPropertyValueResponse findById(String id) {
        return null;
    }

    @Override
    public List<SetPropertyValueResponse> find(SetPropertyValueResponse object) {
        return null;
    }

    @Override
    public SetPropertyValueResponse findFirst(SetPropertyValueResponse object) {
        try {
            return repository.queryForFirstMatching(object);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
