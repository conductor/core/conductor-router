package org.conductor.router.core.factories;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.User;
import org.conductor.router.core.entities.enums.KeyType;

import java.util.UUID;

/**
 * Created by Henrik on 03/09/2016.
 */
public class UserFactory {

    public static User createUser(String email, String password) {
        User user = new User();
        user.setId(UUID.randomUUID().toString());
        user.setEmail(email);
        user.setPassword(password);

        AuthenticationKey applicationKey = new AuthenticationKey();
        applicationKey.setKeyType(KeyType.USER_KEY);
        applicationKey.setId(UUID.randomUUID().toString());
        applicationKey.setPublicKey(UUID.randomUUID().toString());
        applicationKey.setPrivateKey(UUID.randomUUID().toString());
        applicationKey.setUser(user);

        user.setAuthenticationKey(applicationKey);
        return user;
    }

}
