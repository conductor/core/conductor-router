package org.conductor.router.core.factories;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.enums.KeyType;

import java.util.UUID;

/**
 * Created by Henrik on 04/08/2016.
 */
public class AuthenticationKeyFactory {

    public static AuthenticationKey create(KeyType keyType) {
        AuthenticationKey authenticationKey = new AuthenticationKey();
        authenticationKey.setId(UUID.randomUUID().toString());
        authenticationKey.setPrivateKey(UUID.randomUUID().toString());
        authenticationKey.setPublicKey(UUID.randomUUID().toString());
        authenticationKey.setKeyType(keyType);
        return authenticationKey;
    }
}
