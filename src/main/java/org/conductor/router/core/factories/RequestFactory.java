package org.conductor.router.core.factories;

import org.conductor.router.core.collection.DeviceAuthorizationCollection;
import org.conductor.router.core.collection.PropertyCollection;
import org.conductor.router.core.commands.*;
import org.conductor.router.core.entities.*;
import org.conductor.router.core.types.PropertyFilter;
import org.conductor.router.core.types.request.controlunit.*;
import org.conductor.router.core.entities.SetPropertyValueRequest;
import org.conductor.router.core.types.response.controlunit.ResponseStatus;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by Henrik on 13/03/2016.
 */
@Named
public class RequestFactory {

    @Inject
    private PropertyCollection propertyCollection;

    @Inject
    private DeviceAuthorizationCollection deviceAuthorizationCollection;

    public IRequest createRequest(ICommand command) {
        if (command instanceof InstallComponentGroupCommand) {
            return createInstallComponentGroupRequest((InstallComponentGroupCommand) command);
        } else if (command instanceof InstallDeviceCommand) {
            return createInstallDeviceRequest((InstallDeviceCommand) command);
        } else if (command instanceof InstallControlUnitCommand) {
            return createInstallControlUnitRequest((InstallControlUnitCommand) command);
        } else if (command instanceof SetPropertyValueCommand) {
            return createSetPropertyValueRequest((SetPropertyValueCommand) command);
        }

        throw new IllegalArgumentException("Invalid command received '" + command.getClass() + "'.");
    }

    private IRequest createInstallControlUnitRequest(InstallControlUnitCommand command) {
        return new InstallControlUnitRequest(UUID.randomUUID().toString(), command.getRequester());
    }

    private IRequest createInstallDeviceRequest(InstallDeviceCommand command) {
        return new InstallDeviceRequest(UUID.randomUUID().toString(), command.getRequester(), command.getControlUnit());
    }

    private IRequest createSetPropertyValueRequest(SetPropertyValueCommand command) {
        PropertyFilter filter = (PropertyFilter) command.getFilter();

        Device deviceQuery = new Device();
        deviceQuery.setId(filter.getDeviceId());
        deviceQuery.setName(filter.getDeviceName());
        deviceQuery.setType(filter.getDeviceType());

        if (filter.getPublicKeys() != null && filter.getPublicKeys().size() > 0) {
            for (String publicKey : filter.getPublicKeys()) {
                AuthenticationKey authenticationKey = new AuthenticationKey();
                authenticationKey.setPublicKey(publicKey);
                DeviceAuthorization deviceAuthorization = new DeviceAuthorization();
                deviceAuthorization.setAccessGivenTo(authenticationKey);
                deviceQuery.getDeviceAuthorizations().add(deviceAuthorization);
            }
        }

        ComponentGroup componentGroupQuery = new ComponentGroup();
        componentGroupQuery.setDevice(deviceQuery);

        Component componentQuery = new Component();
        componentQuery.setId(filter.getComponentId());
        componentQuery.setName(filter.getComponentName());
        componentQuery.setType(filter.getComponentType());
        componentQuery.setComponentGroup(componentGroupQuery);

        Property propertyQuery = new Property();
        propertyQuery.setId(filter.getPropertyId());
        propertyQuery.setName(filter.getPropertyName());
        propertyQuery.setValue(filter.getPropertyValue());
        propertyQuery.setComponent(componentQuery);

        List<Property> properties = propertyCollection.find(propertyQuery);
        List<SetPropertyValueRequest> requests = new ArrayList();

        SetPropertyValueRequestSummary setPropertyValueRequestSummary = new SetPropertyValueRequestSummary(UUID.randomUUID().toString(), command.getRequester(), ResponseStatus.WAITING, requests);

        for (Property property : properties) {
            SetPropertyValueRequest request = new SetPropertyValueRequest(UUID.randomUUID().toString(), property, command.getPropertyValue(), ResponseStatus.WAITING);
            request.setSetPropertyValueRequestSummary(setPropertyValueRequestSummary);
            requests.add(request);
        }

        setPropertyValueRequestSummary.setRequests(requests);
        return setPropertyValueRequestSummary;
    }

    private IRequest createInstallComponentGroupRequest(InstallComponentGroupCommand command) {
        return new InstallComponentGroupRequest(UUID.randomUUID().toString(), command.getRequester(), command.getComponentGroup().getDevice().getControlUnit(), command.getComponentGroup());
    }

}
