package org.conductor.router.core.dispatcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.InstallComponentGroupResponseCollection;
import org.conductor.router.core.collection.InstallControlUnitResponseCollection;
import org.conductor.router.core.collection.InstallDeviceResponseCollection;
import org.conductor.router.core.collection.SetPropertyValueResponseCollection;
import org.conductor.router.core.entities.InstallComponentGroupResponse;
import org.conductor.router.core.entities.InstallControlUnitResponse;
import org.conductor.router.core.entities.InstallDeviceResponse;
import org.conductor.router.core.entities.SetPropertyValueResponse;
import org.conductor.router.core.types.response.controlunit.*;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.List;

/**
 * Created by Henrik on 23/03/2016.
 */
@Named
public class ResponseDispatcher {

    private Logger log = LogManager.getLogger(getClass().getName());

    @Inject
    private SetPropertyValueResponseCollection setPropertyValueResponseCollection = null;

    @Inject
    private InstallComponentGroupResponseCollection installComponentGroupResponseCollection;

    @Inject
    private InstallDeviceResponseCollection installDeviceResponseCollection;

    @Inject
    private InstallControlUnitResponseCollection installControlUnitResponseCollection;

    public void send(List<IResponse> responses) {
        for (IResponse response : responses) {
           send(response);
        }
    }

    public void send(IResponse response) {
        if (response instanceof SetPropertyValueResponse) {
            setPropertyValueResponseCollection.add((SetPropertyValueResponse) response);
        } else if (response instanceof InstallComponentGroupResponse) {
            installComponentGroupResponseCollection.add((InstallComponentGroupResponse) response);
        }else if (response instanceof InstallDeviceResponse) {
            installDeviceResponseCollection.add((InstallDeviceResponse) response);
        }else if (response instanceof InstallControlUnitResponse) {
            installControlUnitResponseCollection.add((InstallControlUnitResponse) response);
        } else {
            log.error("Unknown response type '" + response.getClass().getSimpleName() + "'");
            throw new Error("Unknown response type '" + response.getClass().getSimpleName() + "'");
        }
    }
}
