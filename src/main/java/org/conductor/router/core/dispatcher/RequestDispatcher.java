package org.conductor.router.core.dispatcher;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.Collection;
import org.conductor.router.core.collection.CollectionFactory;
import org.conductor.router.core.entities.*;
import org.conductor.router.core.types.request.controlunit.*;
import org.conductor.router.core.types.response.controlunit.*;
import org.conductor.router.core.types.response.controlunit.Response;

import javax.inject.Inject;
import javax.inject.Named;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@Named
public class RequestDispatcher {

    private Logger log = LogManager.getLogger(getClass().getName());

    @Inject
    private CollectionFactory collectionFactory;

    public List<IResponse> send(List<IRequest> requests) {
        List<IResponse> responses = new ArrayList<IResponse>();

        for (IRequest request : requests) {
            responses.add(send(request));
        }

        return responses;
    }

    public IResponse send(final IRequest request) {
        try {
            Collection collection = collectionFactory.getCollectionFor(request.getClass());

            if (collection == null) {
                throw new Exception("Unknown request type '" + request.getClass().getSimpleName() + "', can't find a collection for the specified request type.");
            }

            collection.add(request);

            if (request instanceof SetPropertyValueRequest) {
                return new SetPropertyValueResponse(UUID.randomUUID().toString(), request.getId(), request.getRequester(), ResponseStatus.WAITING);
            } else if (request instanceof InstallComponentGroupRequest) {
                return new InstallComponentGroupResponse(UUID.randomUUID().toString(), request.getId(), request.getRequester(), ResponseStatus.WAITING);
            } else if (request instanceof InstallDeviceRequest) {
                return new InstallDeviceResponse(UUID.randomUUID().toString(), request.getId(), request.getRequester(), ResponseStatus.WAITING);
            } else if (request instanceof InstallControlUnitRequest) {
                return new InstallControlUnitResponse(UUID.randomUUID().toString(), request.getId(), request.getRequester(), ResponseStatus.WAITING);
            } else {
                throw new Exception("Unknown request type '" + request.getClass().getSimpleName() + "'");
            }
        } catch (Throwable e) {
            log.error("Failed to send request ({}). ", request, e);
            return new Response(UUID.randomUUID().toString(), request.getId(), request.getRequester(), ResponseStatus.ERROR, e.getMessage());
        }
    }

}
