package org.conductor.router.core.commands;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.types.IFilter;

public class SetPropertyValueCommand implements ICommand {

    private AuthenticationKey requester;
    private IFilter filter;
    private Object propertyValue;

    public AuthenticationKey getRequester() {
        return requester;
    }

    public void setRequester(AuthenticationKey requester) {
        this.requester = requester;
    }

    @Override
    @JsonDeserialize(as = org.conductor.router.core.types.PropertyFilter.class)
    public IFilter getFilter() {
        return filter;
    }

    @Override
    @JsonDeserialize(as = org.conductor.router.core.types.PropertyFilter.class)
    public void setFilter(IFilter filter) {
        this.filter = filter;
    }

    public Object getPropertyValue() {
        return propertyValue;
    }

    public void setPropertyValue(Object propertyValue) {
        this.propertyValue = propertyValue;
    }

}
