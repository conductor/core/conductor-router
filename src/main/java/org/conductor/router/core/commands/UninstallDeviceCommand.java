package org.conductor.router.core.commands;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.types.IFilter;

/**
 * Created by Henrik on 14/01/2017.
 */
public class UninstallDeviceCommand implements ICommand {

    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    @Override
    public AuthenticationKey getRequester() {
        return null;
    }

    @Override
    public void setRequester(AuthenticationKey requester) {

    }

    @Override
    public IFilter getFilter() {
        return null;
    }

    @Override
    public void setFilter(IFilter filter) {

    }
}
