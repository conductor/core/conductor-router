package org.conductor.router.core.commands;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.ControlUnit;
import org.conductor.router.core.types.IFilter;

/**
 * Created by Henrik on 26/07/2016.
 */
public class InstallDeviceCommand implements ICommand {

    private String controlUnitId;
    private ControlUnit controlUnit;
    private String deviceName;
    private String deviceType;
    private AuthenticationKey authenticationKey;

    public String getControlUnitId() {
        return controlUnitId;
    }

    public void setControlUnitId(String controlUnitId) {
        this.controlUnitId = controlUnitId;
    }

    public ControlUnit getControlUnit() {
        return controlUnit;
    }

    public void setControlUnit(ControlUnit controlUnit) {
        this.controlUnit = controlUnit;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceType() {
        return deviceType;
    }

    public void setDeviceType(String deviceType) {
        this.deviceType = deviceType;
    }

    @Override
    public AuthenticationKey getRequester() {
        return authenticationKey;
    }

    @Override
    public void setRequester(AuthenticationKey requester) {
        this.authenticationKey = requester;
    }

    @Override
    public IFilter getFilter() {
        return null;
    }

    @Override
    public void setFilter(IFilter filter) {

    }
}
