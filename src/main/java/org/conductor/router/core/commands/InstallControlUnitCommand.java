package org.conductor.router.core.commands;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.types.IFilter;

/**
 * Created by Henrik on 26/07/2016.
 */
public class InstallControlUnitCommand implements ICommand {

    private String controlUnitName;

    @Override
    public AuthenticationKey getRequester() {
        return null;
    }

    @Override
    public void setRequester(AuthenticationKey requester) {

    }

    @Override
    public IFilter getFilter() {
        return null;
    }

    @Override
    public void setFilter(IFilter filter) {

    }

    public String getControlUnitName() {
        return controlUnitName;
    }

    public void setControlUnitName(String controlUnitName) {
        this.controlUnitName = controlUnitName;
    }

}
