package org.conductor.router.core.commands;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.types.IFilter;

/**
 * Created by Henrik on 14/01/2017.
 */
public class UninstallComponentCommand implements ICommand {

    private String componentId;

    public String getComponentId() {
        return componentId;
    }

    public void setComponentId(String componentId) {
        this.componentId = componentId;
    }

    @Override
    public AuthenticationKey getRequester() {
        return null;
    }

    @Override
    public void setRequester(AuthenticationKey requester) {

    }

    @Override
    public IFilter getFilter() {
        return null;
    }

    @Override
    public void setFilter(IFilter filter) {

    }
}
