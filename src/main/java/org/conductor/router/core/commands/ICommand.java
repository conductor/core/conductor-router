package org.conductor.router.core.commands;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.types.IFilter;

/**
 * Created by Henrik on 13/03/2016.
 */
public interface ICommand {

    AuthenticationKey getRequester();
    void setRequester(AuthenticationKey requester);

    IFilter getFilter();
    void setFilter(IFilter filter);

}
