package org.conductor.router.core.commands;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.types.IFilter;

/**
 * Created by Henrik on 12/01/2017.
 */
public class UninstallControlUnitCommand implements ICommand {

    private String controlUnitId;

    public String getControlUnitId() {
        return controlUnitId;
    }

    public void setControlUnitId(String controlUnitId) {
        this.controlUnitId = controlUnitId;
    }

    @Override
    public AuthenticationKey getRequester() {
        return null;
    }

    @Override
    public void setRequester(AuthenticationKey requester) {

    }

    @Override
    public IFilter getFilter() {
        return null;
    }

    @Override
    public void setFilter(IFilter filter) {

    }

}
