package org.conductor.router.core.commands;

import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.ComponentGroup;
import org.conductor.router.core.types.IFilter;

/**
 * Created by Henrik on 24/07/2016.
 */
public class InstallComponentGroupCommand implements ICommand {

    private AuthenticationKey requester;
    private ComponentGroup componentGroup;
    private String deviceId;

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public AuthenticationKey getRequester() {
        return requester;
    }

    public void setRequester(AuthenticationKey requester) {
        this.requester = requester;
    }

    @Override
    public IFilter getFilter() {
        return null;
    }

    @Override
    public void setFilter(IFilter filter) {

    }

    public ComponentGroup getComponentGroup() {
        return componentGroup;
    }

    public void setComponentGroup(ComponentGroup componentGroup) {
        this.componentGroup = componentGroup;
    }
}
