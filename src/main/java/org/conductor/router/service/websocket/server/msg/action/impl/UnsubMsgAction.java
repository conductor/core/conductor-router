package org.conductor.router.service.websocket.server.msg.action.impl;

import org.conductor.router.service.websocket.model.managingdata.Unsub;
import org.conductor.router.service.websocket.server.msg.action.MsgAction;
import org.conductor.router.service.websocket.server.msg.pubsub.PubSub;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

public class UnsubMsgAction implements MsgAction<Unsub> {

	private PubSub pubSub;

	public UnsubMsgAction(PubSub pubSub){
		this.pubSub = pubSub;
	}

	public void execute(Unsub unsub, DDPSession session) {
		pubSub.unsub(unsub, session);
	}

}
