package org.conductor.router.service.websocket.server.msg;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.conductor.router.service.websocket.model.HasMsg;
import org.conductor.router.service.websocket.model.connection.Connect;
import org.conductor.router.service.websocket.model.connection.Connected;
import org.conductor.router.service.websocket.model.connection.Failed;
import org.conductor.router.service.websocket.model.hearthbit.Ping;
import org.conductor.router.service.websocket.model.hearthbit.Pong;
import org.conductor.router.service.websocket.model.managingdata.Added;
import org.conductor.router.service.websocket.model.managingdata.AddedBefore;
import org.conductor.router.service.websocket.model.managingdata.Changed;
import org.conductor.router.service.websocket.model.managingdata.MovedBefore;
import org.conductor.router.service.websocket.model.managingdata.Nosub;
import org.conductor.router.service.websocket.model.managingdata.Ready;
import org.conductor.router.service.websocket.model.managingdata.Removed;
import org.conductor.router.service.websocket.model.managingdata.Sub;
import org.conductor.router.service.websocket.model.managingdata.Unsub;
import org.conductor.router.service.websocket.model.rpc.Method;
import org.conductor.router.service.websocket.model.rpc.Result;
import org.conductor.router.service.websocket.model.rpc.Updated;

import java.util.Set;

public class MapMsgClassRepository implements MsgClassRepository {

	private Map<String, Class<? extends HasMsg>> map = new HashMap<String, Class<? extends HasMsg>>();

	public MapMsgClassRepository() {
		map.put(Connect.MSG, Connect.class);
		map.put(Connected.MSG, Connected.class);
		map.put(Failed.MSG, Failed.class);
		map.put(org.conductor.router.service.websocket.model.error.Error.MSG, org.conductor.router.service.websocket.model.error.Error.class);
		map.put(Ping.MSG, Ping.class);
		map.put(Pong.MSG, Pong.class);
		map.put(Added.MSG, Added.class);
		map.put(AddedBefore.MSG, AddedBefore.class);
		map.put(Changed.MSG, Changed.class);
		map.put(MovedBefore.MSG, MovedBefore.class);
		map.put(Nosub.MSG, Nosub.class);
		map.put(Ready.MSG, Ready.class);
		map.put(Removed.MSG, Removed.class);
		map.put(Sub.MSG, Sub.class);
		map.put(Unsub.MSG, Unsub.class);
		map.put(Method.MSG, Method.class);
		map.put(Result.MSG, Result.class);
		map.put(Updated.MSG, Updated.class);
	}

	public Class<? extends HasMsg> classFor(String msg) {
		return map.get(msg);
	}

	public String msgFor(Class<? extends HasMsg> clazz){
		Set<Entry<String,Class<? extends HasMsg>>> entrySet = map.entrySet();
		for (Entry<String, Class<? extends HasMsg>> entry : entrySet) {
			if(entry.getValue().equals(clazz)){
				return entry.getKey();
			}
		}
		return null;
	}

	public Collection<Class<? extends HasMsg>> getAllClasses(){
		return map.values();
	}

}
