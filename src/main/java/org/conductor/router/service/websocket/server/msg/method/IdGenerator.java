package org.conductor.router.service.websocket.server.msg.method;

public interface IdGenerator {

	String generateCollectionID(String randomSeed);

}
