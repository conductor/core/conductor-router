package org.conductor.router.service.websocket.server.msg;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;

public class JacksonOutboundMessagesFormatter<T> implements OutboundMessagesFormatter<T> {

	private ObjectMapper objectMapper;

	public JacksonOutboundMessagesFormatter(ObjectMapper objectMapper) {
		this.objectMapper = objectMapper;
	}

	public String format(T obj) {
		try {
			return objectMapper.writeValueAsString(obj);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

}
