package org.conductor.router.service.websocket.server.msg.method;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.collection.AuthenticationKeyCollection;
import org.conductor.router.core.controllers.*;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.entities.enums.KeyType;
import org.conductor.router.core.types.exceptions.UnauthorizedException;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class ReflectionMethodInvoker implements MethodInvoker {

    @Inject
    private CommandController commandController;

    @Inject
    private InstallerController installerController;

    @Inject
    private ControlUnitController controlUnitController;

    @Inject
    private ApplicationController applicationController;

    @Inject
    private UserController userController;

    @Inject
    private MethodAuthorizer methodAuthorizer;

    private Logger log = LogManager.getLogger(getClass().getName());

    private Map<String, Object> classes = new HashMap<>();

    @PostConstruct
    public void init() {
        classes.put("Install", installerController);
        classes.put("Command", commandController);
        classes.put("ControlUnit", controlUnitController);
        classes.put("Application", applicationController);
        classes.put("User", userController);
    }

    /* (non-Javadoc)
     * @see org.conductor.router.server.server.msg.method.MethodInvoker#invoke(java.lang.String, java.lang.Object[])
     *
     * Currently this invoker doesn't support call to method with overrun parameters. This is the first version :)
     */
    public Object invoke(String methodPath, Object... params) throws NoSuchMethodException {
        try {
            Object clazz = getClass(methodPath);

            if (clazz == null) {
                throw new NoSuchMethodException("Couldn't find class with the specified name " + methodPath + ".");
            }

            String methodName = getMethodName(methodPath);
            Method method = findMethod(clazz, methodName);
            Object[] requestParameters = deserializeParameters(method, params);

            if (methodAuthorizer.requireAuthorization(method)) {
                methodAuthorizer.validateAuthorization(method, requestParameters);
            }

            return method.invoke(clazz, requestParameters);
        } catch (SecurityException | UnauthorizedException | IllegalAccessException | InvocationTargetException | IllegalArgumentException e) {
            throw new RuntimeException(e);
        }
    }

    private Method findMethod(Object clazz, String methodName) throws NoSuchMethodException {
        Method[] methods = clazz.getClass().getMethods();
        for (Method method : methods) {
            if (method.getName().equals(methodName)) {
                return method;
            }
        }
        throw new NoSuchMethodException(methodName);
    }

    private String getMethodName(String str) {
        if (str == null || !str.contains(".")) {
            return null;
        }

        return str.split("\\.")[1];
    }

    private Object getClass(String str) {
        if (str == null || !str.contains(".")) {
            return null;
        }

        String className = str.split("\\.")[0];
        return classes.get(className);
    }

    private Object[] deserializeParameters(Method method, Object... params) {
        if (method.getParameterCount() != params.length) {
            throw new RuntimeException("Invalid number of parameters, method '" + method.getName() + "' expected " + method.getParameterCount() + " parameters and received " + params.length + ".");
        }

        ObjectMapper mapper = new ObjectMapper();
        Object[] parameters = new Object[method.getParameterCount()];

        for (int i = 0; i < method.getParameterCount(); i++) {
            parameters[i] = mapper.convertValue(params[i], (Class) method.getParameterTypes()[i]);
        }

        return parameters;
    }

}
