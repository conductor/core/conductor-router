package org.conductor.router.service.websocket.server.msg;

import com.fasterxml.jackson.databind.ObjectMapper;

public interface InboundMessagesParserObjectMapperFactory {

	ObjectMapper build();

}
