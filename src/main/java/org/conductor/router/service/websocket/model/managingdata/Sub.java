package org.conductor.router.service.websocket.model.managingdata;

import java.util.Map;

import org.conductor.router.core.publisher.publications.PublicationName;
import org.conductor.router.service.websocket.model.HasMsg;


/**
 * @author davidecerbo
 *
 * Messages:
 * sub (client -> server):
 * id: string (an arbitrary client-determined identifier for this subscription)
 * name: string (the name of the subscription)
 * params: optional array of EJSON items (parameters to the subscription)
 *
 */
public class Sub  implements HasMsg {

	public static final String MSG = "sub";

	private String id;
	private PublicationName name;
	private Map<String, Object>[] params;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public PublicationName getName() {
		return name;
	}
	public void setName(PublicationName name) {
		this.name = name;
	}
	public Map<String, Object>[] getParams() {
		return params;
	}
	public void setParams(Map<String, Object>[] params) {
		this.params = params;
	}
	public String getMsg() {
		return MSG;
	}

}
