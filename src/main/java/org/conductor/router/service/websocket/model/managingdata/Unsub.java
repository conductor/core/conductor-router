package org.conductor.router.service.websocket.model.managingdata;

import org.conductor.router.service.websocket.model.HasMsg;


/**
 * @author davidecerbo
 *
 * Messages:
 * unsub (client -> server):
 * id: string (the id passed to 'sub')
 *
 */
public class Unsub  implements HasMsg {

	public static final String MSG = "unsub";

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getMsg() {
		return MSG;
	}

}
