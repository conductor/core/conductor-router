package org.conductor.router.service.websocket.server.msg.action.impl;

import org.conductor.router.service.websocket.model.connection.Connect;
import org.conductor.router.service.websocket.model.connection.Connected;
import org.conductor.router.service.websocket.server.msg.action.MsgAction;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

public class ConnectMsgAction implements MsgAction<Connect> {

	public void execute(Connect object, DDPSession session) {
		Connected connected = new Connected();
		connected.setSession(session.getSession().getId());
		session.sendMsg(connected);
	}

}
