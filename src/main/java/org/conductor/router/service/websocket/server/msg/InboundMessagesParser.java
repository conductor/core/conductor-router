package org.conductor.router.service.websocket.server.msg;

public interface InboundMessagesParser<T> {
	
	T parse(String json);

	void init();

}
