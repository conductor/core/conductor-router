package org.conductor.router.service.websocket.model;

public interface HasMsg {
	
	public String getMsg();

}
