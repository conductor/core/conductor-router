package org.conductor.router.service.websocket.server.msg.objectmapper;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.conductor.router.service.websocket.model.HasMsg;
import org.conductor.router.service.websocket.server.msg.MsgClassRepository;

public class DDPCustomDeserializer<T extends HasMsg> extends StdDeserializer<T> {

	private static final String MSG_KEY = "msg";
	private MsgClassRepository msgClassRepository;
	private ObjectMapper basicObjectMapper;

	public DDPCustomDeserializer(MsgClassRepository msgClassRepository, ObjectMapper basicObjectMapper) {
		super(HasMsg.class);
		this.msgClassRepository = msgClassRepository;
		this.basicObjectMapper = basicObjectMapper;
	}

	@Override
	public T deserialize(JsonParser jp, DeserializationContext ctxt)  throws IOException, JsonProcessingException {
		ObjectCodec codec = jp.getCodec();
		ObjectNode node = (ObjectNode) codec.readTree(jp);
		JsonNode jsonNode = node.get(MSG_KEY);
		if(jsonNode != null){
			String msg = jsonNode.asText();
			node.remove(MSG_KEY);
			Class<T> classFor =  (Class<T>) msgClassRepository.classFor(msg);
			T result = null;
			if(classFor != null){
				result = this.basicObjectMapper.treeToValue(node, classFor);
			}
			return result;
		} else {
			return null;
		}
	}
}
