package org.conductor.router.service.websocket.server.msg.action;

import org.conductor.router.service.websocket.model.HasMsg;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

public interface MsgAction<T extends HasMsg> {

	void execute(T obj, DDPSession session);

}
