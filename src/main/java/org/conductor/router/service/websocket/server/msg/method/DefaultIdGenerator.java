package org.conductor.router.service.websocket.server.msg.method;

import java.util.UUID;

public class DefaultIdGenerator implements IdGenerator {

	public String generateCollectionID(String randomSeed) {
		return UUID.fromString(randomSeed).toString();
	}

}
