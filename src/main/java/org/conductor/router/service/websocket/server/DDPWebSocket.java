package org.conductor.router.service.websocket.server;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import javax.websocket.CloseReason;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.service.websocket.model.HasMsg;
import org.conductor.router.service.websocket.server.msg.InboundMessagesParser;
import org.conductor.router.service.websocket.server.msg.InboundMessagesParserObjectMapperFactoryImpl;
import org.conductor.router.service.websocket.server.msg.JacksonInboundMessagesParser;
import org.conductor.router.service.websocket.server.msg.action.InboundMsgActionSelector;
import org.conductor.router.service.websocket.server.msg.action.MapInboundMsgActionSelector;
import org.conductor.router.service.websocket.server.msg.action.MsgAction;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

@ServerEndpoint(value = "/websocket")
@Named
public class DDPWebSocket {

    private Logger log = LogManager.getLogger(this.getClass().getName());

    private InboundMessagesParser<HasMsg> inboundMessagesParser;
    private InboundMsgActionSelector inboundMsgActionSelector;

    @Inject
    private MapInboundMsgActionSelector mapInboundMsgActionSelector;

    public DDPWebSocket(InboundMessagesParser<HasMsg> inboundMessagesParser) {
        this.inboundMessagesParser = inboundMessagesParser;
    }

    public DDPWebSocket() {

    }

    @PostConstruct
    public void init() {
        this.inboundMessagesParser = new JacksonInboundMessagesParser(new InboundMessagesParserObjectMapperFactoryImpl());
        this.inboundMsgActionSelector = mapInboundMsgActionSelector;
        this.inboundMessagesParser.init();
    }

    @OnOpen
    public void onOpen(Session session) {
        log.info("Connected! " + session.getId());
    }

    @OnMessage
    public void onMessage(String message, Session session) throws Exception {
        log.info("Incoming message: " + session.getId() + ": " + message);
        HasMsg obj = this.inboundMessagesParser.parse(message);
        MsgAction<HasMsg> action = (MsgAction<HasMsg>) this.inboundMsgActionSelector.select((Class<? extends HasMsg>) obj.getClass());
        action.execute(obj, new DDPSession(session));
    }

    @OnClose
    public void onClose(Session session, CloseReason closeReason) {
        System.out.println(String.format("Session %s closed because of %s", session.getId(), closeReason));
    }

}
