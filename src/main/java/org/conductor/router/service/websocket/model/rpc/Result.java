package org.conductor.router.service.websocket.model.rpc;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.conductor.router.service.websocket.model.HasMsg;

/**
 * @author davidecerbo
 * Messages:
 * result (server -> client):
 * id: string (the id passed to 'method')
 * error: optional Error (an error thrown by the method (or method-not-found)
 * result: optional EJSON item (the return value of the method, if any)
 */
public class Result implements HasMsg {

	public static final String MSG = "result";

	private String id;
	@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
	private org.conductor.router.service.websocket.model.error.Error error;
	private Object result;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public org.conductor.router.service.websocket.model.error.Error getError() {
		return error;
	}
	public void setError(org.conductor.router.service.websocket.model.error.Error error) {
		this.error = error;
	}
	public Object getResult() {
		return result;
	}
	public void setResult(Object result) {
		this.result = result;
	}
	public String getMsg() {
		return MSG;
	}


}
