package org.conductor.router.service.websocket.server.msg.pubsub;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.conductor.router.service.websocket.model.managingdata.Sub;
import org.conductor.router.service.websocket.model.managingdata.Unsub;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

import java.util.Set;
/*
public class MapPubSub implements PubSub {

	private static Map<String,List<SubscriptionInfo>> collectionToSubscription = new HashMap<String, List<SubscriptionInfo>>();

	public void sub(Sub sub, DDPSession session) {
		//collectionToSubscription.put(sub.getName(), buildSubscriptionInfo(sub, session));
	}

	public void unsub(Unsub unsub, DDPSession session) {
		Set<Entry<String,List<SubscriptionInfo>>> entrySet = collectionToSubscription.entrySet();
		for (Entry<String, List<SubscriptionInfo>> entry : entrySet) {
			List<SubscriptionInfo> list = entry.getValue();
			Iterator<SubscriptionInfo> iterator = list.iterator();
			while (iterator.hasNext()) {
				SubscriptionInfo subscriptionInfo = (SubscriptionInfo) iterator.next();
				boolean sessionEq = subscriptionInfo.getSession().equals(session);
				boolean idEq = subscriptionInfo.getSub().getId().equals(unsub.getId());
				if(sessionEq && idEq){
					iterator.remove();
				}
			}
		}
	}

	private List<SubscriptionInfo> buildSubscriptionInfo(Sub sub, DDPSession session) {
		List<SubscriptionInfo> list = collectionToSubscription.get(sub.getName());
		if(list == null ){
			list = new LinkedList<SubscriptionInfo>();
		}
		list.add(new SubscriptionInfo(sub, session));
		return list;
	}

	public void fireEvent(Event event) {
		List<SubscriptionInfo> list = collectionToSubscription.get(event.getCollection());
		if(list != null){
			for (SubscriptionInfo subscriptionInfo : list) {
				subscriptionInfo.getSession().sendMsg(event.getMsg());
			}
		}
	}

}
*/