package org.conductor.router.service.websocket.server.msg;

import java.util.Collection;

import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import org.conductor.router.service.websocket.model.HasMsg;
import org.conductor.router.service.websocket.server.msg.objectmapper.DDPCustomDeserializer;

public class InboundMessagesParserObjectMapperFactoryImpl implements InboundMessagesParserObjectMapperFactory {

	public ObjectMapper build() {
		ObjectMapper objectMapper = new ObjectMapper();
		MsgClassRepository msgClassRepository = new MapMsgClassRepository();
		SimpleModule module = new SimpleModule("DDP", new Version(1, 0, 0, null));
		ObjectMapper basicObjectMapper = new ObjectMapper();
		DDPCustomDeserializer ddpCustomDeserializer = new DDPCustomDeserializer(msgClassRepository, basicObjectMapper);

		Collection<Class<? extends HasMsg>> allClasses = msgClassRepository.getAllClasses();
		for (Class<? extends HasMsg> class1 : allClasses) {
			configureModule(class1, module, ddpCustomDeserializer);
		}
		objectMapper.registerModule(module);
		return objectMapper;
	}

	private void configureModule(Class<? extends HasMsg> clazz, SimpleModule module, DDPCustomDeserializer ddpCustomDeserializer) {
		module.addDeserializer(clazz, ddpCustomDeserializer);
		module.addAbstractTypeMapping(HasMsg.class, clazz);
	}

}
