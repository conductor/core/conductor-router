package org.conductor.router.service.websocket.server.msg.session;

import java.io.IOException;

import javax.websocket.Session;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.service.websocket.model.HasMsg;
import org.conductor.router.service.websocket.server.msg.action.GenericException;
import org.conductor.router.service.websocket.server.msg.action.JsonException;

public class DDPSession {

    private Logger log = LogManager.getLogger(this.getClass().getName());

    private Session session;
    private ObjectMapper mapper;
    private String sessionId;

    public DDPSession(Session session) {
        this(session, new ObjectMapper());
    }

    public DDPSession(Session session, ObjectMapper mapper) {
        this.session = session;
        this.mapper = mapper;
        this.sessionId = session.getId();
    }

    public Session getSession() {
        return session;
    }

    public void sendMsg(HasMsg object) {
        if (object != null) {
            String msg;
            try {
                msg = mapper.writeValueAsString(object);
                log.info("Outgoing message: " + msg);
                session.getBasicRemote().sendText(msg);
            } catch (JsonGenerationException e) {
                throw new JsonException(e);
            } catch (JsonMappingException e) {
                throw new JsonException(e);
            } catch (IOException e) {
                throw new GenericException(e);
            }
        } else {
            log.error("Failed to send message. Message was null.");
        }
    }

    public void sendMsg(String message) {
        try {
            session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            throw new GenericException(e);
        }
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result
                + ((sessionId == null) ? 0 : sessionId.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        DDPSession other = (DDPSession) obj;
        if (sessionId == null) {
            if (other.sessionId != null)
                return false;
        } else if (!sessionId.equals(other.sessionId))
            return false;
        return true;
    }


}
