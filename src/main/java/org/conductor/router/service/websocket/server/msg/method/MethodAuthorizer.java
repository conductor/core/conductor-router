package org.conductor.router.service.websocket.server.msg.method;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.conductor.router.core.annotations.Authorize;
import org.conductor.router.core.collection.AuthenticationKeyCollection;
import org.conductor.router.core.entities.AuthenticationKey;
import org.conductor.router.core.types.exceptions.UnauthorizedException;

import javax.inject.Inject;
import javax.inject.Named;
import java.lang.reflect.Method;

/**
 * Created by Henrik on 01/07/2017.
 */
@Named
public class MethodAuthorizer {

    private Logger log = LogManager.getLogger(getClass().getName());

    @Inject
    private AuthenticationKeyCollection authenticationKeyCollection;

    public boolean requireAuthorization(Method method) {
        return method.getAnnotation(Authorize.class) != null;
    }

    public void validateAuthorization(Method method, Object[] requestParameters) throws UnauthorizedException {
        if (!requireAuthorization(method)) {
            return;
        }

        if (requestParameters == null || requestParameters.length == 0) {
            log.info("No private key supplied in the request.");
            throw new UnauthorizedException("No private key supplied in the request.");
        }

        if (!(requestParameters[0] instanceof String)) {
            log.info("First parameter should be the private key of type String.");
            throw new UnauthorizedException("First parameter should be the private key of type String.");
        }

        String privateKey = (String) requestParameters[0];

        AuthenticationKey authenticationKey = authenticationKeyCollection.findByPrivateKey(privateKey);

        if (authenticationKey == null) {
            log.info("No private key found that matches the supplied private key.");
            throw new UnauthorizedException("No private key found that matches the supplied private key.");
        }

        Authorize authorize = method.getAnnotation(Authorize.class);

        if (!authenticationKey.getKeyType().equals(authorize.requiredKey())) {
            log.info("Invalid authentication key. Authentication key must be a '" + authorize.requiredKey() + "' key.");
            throw new UnauthorizedException("Invalid authentication key. Authentication key must be a '" + authorize.requiredKey() + "' key.");
        }
    }

}
