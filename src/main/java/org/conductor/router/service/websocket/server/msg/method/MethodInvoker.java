package org.conductor.router.service.websocket.server.msg.method;


public interface MethodInvoker {

	Object invoke(String key, Object... params) throws NoSuchMethodException; 

}
