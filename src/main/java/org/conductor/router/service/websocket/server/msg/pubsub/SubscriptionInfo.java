package org.conductor.router.service.websocket.server.msg.pubsub;

import org.conductor.router.service.websocket.model.managingdata.Sub;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

public class SubscriptionInfo {

	private final DDPSession session;
	private final Sub sub;

	public SubscriptionInfo(Sub sub, DDPSession session) {
		this.sub = sub;
		this.session = session;
	}

	public Sub getSub() {
		return sub;
	}

	public DDPSession getSession() {
		return session;
	}

}
