package org.conductor.router.service.websocket.server.msg.action.impl;

import org.conductor.router.service.websocket.model.managingdata.Sub;
import org.conductor.router.service.websocket.server.msg.action.MsgAction;
import org.conductor.router.service.websocket.server.msg.pubsub.PubSub;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

public class SubMsgAction implements MsgAction<Sub> {

	private PubSub pubSub;

	public SubMsgAction(PubSub pubSub){
		this.pubSub = pubSub;
	}

	public void execute(Sub sub, DDPSession session) {
		pubSub.sub(sub, session);
	}

}
