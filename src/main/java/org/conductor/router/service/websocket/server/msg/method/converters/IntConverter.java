package org.conductor.router.service.websocket.server.msg.method.converters;

public class IntConverter implements DataTypeConverter {

  public Object convert(Object value) throws Exception {
    if (value.getClass() == int.class) {
      return (int) value;
    } else if (value.getClass() == String.class) {
      return Integer.parseInt((String) value);
    } else {
      throw new Exception("Unexpected data type, unable to convert data type '" + value.getClass().getSimpleName() + "' to int");
    }
  }
}
