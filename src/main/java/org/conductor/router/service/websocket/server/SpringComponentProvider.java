    package org.conductor.router.service.websocket.server;

import org.glassfish.tyrus.spi.ComponentProvider;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.util.ClassUtils;

import javax.inject.Inject;
import javax.inject.Named;

    /**
 * Created by Henrik on 18/03/2016.
 */
    @Named
public class SpringComponentProvider extends ComponentProvider implements ApplicationContextAware {

    @Inject
    public static ApplicationContext ctx;

    @Override
    public boolean isApplicable(Class<?> c) {
        boolean exist = ctx.containsBean(ClassUtils.getShortNameAsProperty(c));
        return exist;
    }

    @Override
    public <T> T provideInstance(Class<T> c) {
        return ctx.getBean(c);
    }

    @Override
    public boolean destroyInstance(Object o) {
        return false;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        ctx = applicationContext;
    }

}