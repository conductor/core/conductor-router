package org.conductor.router.service.websocket.model.connection;

import org.conductor.router.service.websocket.model.HasMsg;


/**
 * @author davidecerbo
 *
 * Messages:
 * connected (server->client)
 * session: string (an identifier for the DDP session)
 *
 */
public class Connected  implements HasMsg {

	public static final String MSG = "connected";
	private String session;

	public String getSession() {
		return session;
	}
	public void setSession(String session) {
		this.session = session;
	}
	public String getMsg() {
		return MSG;
	}

}
