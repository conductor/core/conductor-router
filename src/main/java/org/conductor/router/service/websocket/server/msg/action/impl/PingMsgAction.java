package org.conductor.router.service.websocket.server.msg.action.impl;

import org.conductor.router.service.websocket.model.hearthbit.Ping;
import org.conductor.router.service.websocket.model.hearthbit.Pong;
import org.conductor.router.service.websocket.server.msg.action.MsgAction;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

public class PingMsgAction implements MsgAction<Ping> {

	public void execute(final Ping ping, DDPSession session) {
		Pong pong = new Pong(){{
			setId(ping.getId());
		}};
		session.sendMsg(pong);
	}

}
