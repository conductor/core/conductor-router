package org.conductor.router.service.websocket.server.msg;

public interface OutboundMessagesFormatter<T> {

	String format(T obj);
	
}
