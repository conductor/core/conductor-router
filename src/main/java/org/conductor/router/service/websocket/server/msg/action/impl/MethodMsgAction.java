package org.conductor.router.service.websocket.server.msg.action.impl;

import java.util.LinkedList;
import java.util.List;

import org.conductor.router.service.websocket.model.rpc.Method;
import org.conductor.router.service.websocket.model.rpc.Result;
import org.conductor.router.service.websocket.model.rpc.Updated;
import org.conductor.router.service.websocket.server.msg.action.MsgAction;
import org.conductor.router.service.websocket.server.msg.method.MethodInvoker;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;
import org.conductor.router.service.websocket.model.error.Error;

public class MethodMsgAction implements MsgAction<Method> {

	private List<MethodInvoker> invokers = new LinkedList<>();

	public void execute(final Method object, DDPSession session) {
		Result result = null;

		for (MethodInvoker methodInvoker : invokers) {
			try {
				Object invocationResult = invoke(object, methodInvoker);
				result = new Result();
				result.setResult(invocationResult);
				break;
			} catch (NoSuchMethodException e) {
				// Method not found, try next invoker
			} catch (Exception e) {
				result = createErrorResult(e);
				break;
			}
		}

		if(result == null){
			result = createErrorResult("Method '" + object.getMethod()  + "' not found");
		}

		result.setId(object.getId());

		session.sendMsg(result);
		session.sendMsg(new Updated(){{
			setMethods(new String[]{object.getId()});
		}});
	}

	private Object invoke(Method object, MethodInvoker methodInvoker) throws NoSuchMethodException {
		List params = object.getParams();
		Object[] array = null;

		if(params != null){
			array = params.toArray();
		}

		return methodInvoker.invoke(object.getMethod(), array);
	}

	private Result createErrorResult(Exception e) {
		Error error = new Error();
		error.setError(e.getClass().toString());
		error.setReason(e.getMessage());

		Result result = new Result();
		result.setError(error);
		return result;
	}

	private Result createErrorResult(String message) {
		Error error = new Error();
		error.setError(message);

		Result result = new Result();
		result.setError(error);
		return result;
	}

	public void addInvoker(MethodInvoker invoker){
		this.invokers.add(invoker);
	}

	public void setInvokers(List<MethodInvoker> invokers) {
		this.invokers = invokers;
	}

}
