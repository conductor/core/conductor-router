package org.conductor.router.service.websocket.server.msg.method.converters;

public interface DataTypeConverter {
  public Object convert(Object value) throws Exception;
}
