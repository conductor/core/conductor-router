package org.conductor.router.service.websocket.server.msg;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.conductor.router.service.websocket.model.HasMsg;


public class JacksonInboundMessagesParser implements InboundMessagesParser<HasMsg> {

	private InboundMessagesParserObjectMapperFactory objectMapperFactory;
	private ObjectMapper objectMapper;

	public JacksonInboundMessagesParser(InboundMessagesParserObjectMapperFactory objectMapperFactory) {
		this.objectMapperFactory = objectMapperFactory;
	}

	public void init(){
		this.objectMapper = this.objectMapperFactory.build();
	}

	public HasMsg parse(String json) {
		try {
			return objectMapper.readValue(json, HasMsg.class);
		} catch (IOException e) {
			throw new ParseMessageException(e);
		}
	}

}
