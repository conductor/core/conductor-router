package org.conductor.router.service.websocket.server.msg.pubsub;

public enum Type {
	
	ADDED, ADDEDBEFORE, CHANGED, MOVEDBEFORE, READY, REMOVED

}
