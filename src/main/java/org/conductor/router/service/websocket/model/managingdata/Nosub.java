package org.conductor.router.service.websocket.model.managingdata;

import org.conductor.router.service.websocket.model.HasMsg;


/**
 * @author davidecerbo
 *
 * Messages:
 * nosub (server -> client):
 * id: string (the id passed to 'sub')
 * error: optional Error (an error raised by the subscription as it concludes, or sub-not-found)
 *
 */
public class Nosub  implements HasMsg {

	public static final String MSG = "nosub";

	private String id;
	private Error error;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Error getError() {
		return error;
	}
	public void setError(Error error) {
		this.error = error;
	}
	public String getMsg() {
		return MSG;
	}

}
