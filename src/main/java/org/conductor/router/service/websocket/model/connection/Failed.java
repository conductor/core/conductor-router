package org.conductor.router.service.websocket.model.connection;

import org.conductor.router.service.websocket.model.HasMsg;


/**
 * @author davidecerbo
 *
 * Messages:
 * connected (server->client)
 * session: string (an identifier for the DDP session)
 * failed (server->client)
 * version: string (a suggested protocol version to connect with)
 *
 */
public class Failed  implements HasMsg {

	public static final String MSG = "failed";

	private String version;

	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	public String getMsg() {
		return MSG;
	}
}
