package org.conductor.router.service.websocket.server.msg.action;

import org.conductor.router.service.websocket.model.HasMsg;

public interface InboundMsgActionSelector {

	MsgAction<? extends HasMsg> select(Class<? extends HasMsg> msgClass);

}
