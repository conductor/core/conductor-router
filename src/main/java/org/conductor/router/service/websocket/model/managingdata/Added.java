package org.conductor.router.service.websocket.model.managingdata;

import java.util.List;

import org.conductor.router.service.websocket.model.HasCollection;
import org.conductor.router.service.websocket.model.HasMsg;


/**
 * @author davidecerbo
 *
 * Messages:
 * added (server -> client):
 * collection: string (collection name)
 * id: string (document ID)
 * fields: optional object with EJSON values
 *
 */
public class Added  implements HasMsg, HasCollection {

	public static final String MSG = "added";

	private String collection;
	private String id;
	private Object fields;

	public String getCollection() {
		return collection;
	}
	public void setCollection(String collection) {
		this.collection = collection;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Object getFields() {
		return fields;
	}
	public void setFields(Object fields) {
		this.fields = fields;
	}
	public String getMsg() {
		return MSG;
	}

}
