package org.conductor.router.service.websocket.server.msg;

import java.util.Collection;

import org.conductor.router.service.websocket.model.HasMsg;

public interface MsgClassRepository {

	Class<? extends HasMsg> classFor(String msg);

	String msgFor(Class<? extends HasMsg> clazz);

	Collection<Class<? extends HasMsg>> getAllClasses();

}
