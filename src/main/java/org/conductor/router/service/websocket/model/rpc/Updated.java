package org.conductor.router.service.websocket.model.rpc;

import org.conductor.router.service.websocket.model.HasMsg;

/**
 * @author davidecerbo
 *         Messages:
 *         updated (server -> client):
 *         methods: array of strings (ids passed to 'method', all of whose writes have been reflected in data messages)
 */
public class Updated implements HasMsg {

    public static final String MSG = "updated";

    private String[] methods;

    public String[] getMethods() {
        return methods;
    }

    public void setMethods(String[] methods) {
        this.methods = methods;
    }

    public String getMsg() {
        return MSG;
    }
}
