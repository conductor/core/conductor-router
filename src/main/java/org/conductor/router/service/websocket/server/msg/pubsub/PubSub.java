package org.conductor.router.service.websocket.server.msg.pubsub;

import org.conductor.router.service.websocket.model.managingdata.Sub;
import org.conductor.router.service.websocket.model.managingdata.Unsub;
import org.conductor.router.service.websocket.server.msg.session.DDPSession;

public interface PubSub {

	void sub(Sub sub, DDPSession session);

	void unsub(Unsub unsub, DDPSession session);

}
