package org.conductor.router.service.websocket.model;

public interface HasCollection {
	
	String getCollection();

}
