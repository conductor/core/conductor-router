package org.conductor.router.service.websocket.server.msg.action;

import org.conductor.router.core.publisher.Publisher;
import org.conductor.router.service.websocket.model.connection.Connect;
import org.conductor.router.service.websocket.model.hearthbit.Ping;
import org.conductor.router.service.websocket.model.managingdata.Sub;
import org.conductor.router.service.websocket.model.managingdata.Unsub;
import org.conductor.router.service.websocket.model.rpc.Method;
import org.conductor.router.service.websocket.server.msg.action.impl.*;
import org.conductor.router.service.websocket.server.msg.method.ReflectionMethodInvoker;

import javax.annotation.PostConstruct;
import javax.inject.Inject;
import javax.inject.Named;
import java.util.HashMap;
import java.util.Map;

@Named
public class MapInboundMsgActionSelector implements InboundMsgActionSelector {

    @Inject
    private Publisher publisher;

    @Inject
    private ReflectionMethodInvoker reflectionMethodInvoker;

    private Map<Class, MsgAction> map = new HashMap<Class, MsgAction>();

    @PostConstruct
    public void init() {
        map.put(Connect.class, new ConnectMsgAction());
        map.put(Sub.class, new SubMsgAction(publisher));
        map.put(Unsub.class, new UnsubMsgAction(publisher));
        MethodMsgAction methodMsgAction = new MethodMsgAction();
        methodMsgAction.addInvoker(reflectionMethodInvoker);
        map.put(Method.class, methodMsgAction);
        map.put(Ping.class, new PingMsgAction());
    }

    public MsgAction select(Class msgClass) {
        return map.get(msgClass);
    }

}
