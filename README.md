# Conductor Router
Handles the communication between the apps and the control units and vice versa.

## Build Instructions
- Open a command prompt at the root of the conductor-router folder.
- Run command "gradle clean fatJar".
- Go to the folder conductor-router/build/libs.
- Run command "java -jar conductor-router-all-1.0.jar".